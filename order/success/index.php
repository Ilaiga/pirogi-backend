<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Успешное оформление заказа"); ?>

<div class="container container--helper">
	<section class="success">
		<p class="title title__form">Ваш заказ №<?=$_GET['ORDERID']?> принят в обработку!</p>
		<p class="success__text">Менеджер свяжется с Вами в ближайшее время.</p>
		<p class="success__text">Следить за статусом заказа можно в личном кабинете</p>
		<a class="btn btn--accept" href="/personal/private/">В личный кабинет</a>
	</section>
</div>


<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
