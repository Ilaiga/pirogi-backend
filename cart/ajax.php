<?
define("STOP_STATISTICS", true);
define('NO_AGENT_CHECK', true);

use Bitrix\Main\Loader;

if (isset($_REQUEST['site_id']) && is_string($_REQUEST['site_id']))
{
    $siteID = trim($_REQUEST['site_id']);
    if ($siteID !== '' && preg_match('/^[a-z0-9_]{2}$/i', $siteID) === 1)
    {
        define('SITE_ID', $siteID);
    }
}

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if (!check_bitrix_sessid() || $_SERVER["REQUEST_METHOD"] != "POST")
    return;

if (!Loader::includeModule('sale') || !Loader::includeModule('catalog'))
    return;

global $USER, $APPLICATION;

CUtil::JSPostUnescape();

$arRes = array();
$newProductId = false;
$newBasketId = false;
$arErrors = array();


//echo debug($_POST);

if(isset($_POST['action']) && strlen($_POST['action']) > 0) {
	if ($_POST['action'] == "recalculate")
	{
		// todo: extract duplicated code to function


		//echo '123';

		$arFields = array(
			"QUANTITY" => $_POST['value'],
		);
		CSaleBasket::Update($_POST['basketID'], $arFields);


		if ($ex = $APPLICATION->GetException()) {
			$arRes["CODE"] = "ERROR =" . $ex->GetString();
		}

		$arRes["CODE"] = "SUCCESS";

	}
}

if (isset($_POST['basketAction']) && strlen($_POST['basketAction']) > 0)
{

    $arPropsValues = isset($_POST["props"]) ? $_POST["props"] : array();
    $strColumns = isset($_POST["select_props"]) ? $_POST["select_props"] : "";
    $arColumns = explode(",", $strColumns);
    $strOffersProps = isset($_POST["offers_props"]) ? $_POST["offers_props"] : "";
    $strOffersProps = explode(",", $strOffersProps);


    if($_POST['basketAction'] == "COUPON") {
        if(empty($_POST['coupon'])) return 1;

		CCatalogDiscount::ClearCoupon();
        
        $status = CCatalogDiscount::SetCoupon(trim($_POST['coupon']));
        if ($ex = $APPLICATION->GetException())
        	return $arRes["CODE"] = "ERROR =".$ex->GetString();


        $arRes['COUPONS'] = CCatalogDiscount::GetCoupons();

        if(!$status)
			CCatalogDiscount::ClearCoupon();

        $arRes['CODE'] = $status ? "SUCCESS" : "ERROR";

    }
    
    if($_POST['basketAction'] == "DELETE") {
        if (CSaleBasket::Delete($_POST['basketID']))
            $arRes['CODE'] = "SUCCESS";
    }

    if ($_POST['basketAction'] == "recalculate")
    {
        // todo: extract duplicated code to function


		//echo '123';

        $arFields = array(
            "QUANTITY" => $_POST['value'],
        );
        CSaleBasket::Update($_POST['basketID'], $arFields);


        if ($ex = $APPLICATION->GetException()) {
			$arRes["CODE"] = "ERROR =" . $ex->GetString();
		}

        $arRes["CODE"] = "SUCCESS";

    }
}

$cost = (new MyBasketClass())->BasketInit();
$_SESSION['TOTAL_SUM_FORMATED'] = $cost['TOTAL_SUM_FORMATED'];


//echo '<pre>'.print_r($arRes, true).'</pre>';
///echo '<pre>'.print_r($_POST, true).'</pre>';

header('Content-Type: application/json; charset='.LANG_CHARSET);
echo CUtil::PhpToJSObject($arRes);
die();
