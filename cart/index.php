<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");


$price = $_SESSION['GET_CITY']['CODE_PRICE']['VALUE'];
?>
    <section class="cart">
        <div class="container"><h1 class="cart__title title">Корзина</h1>
            <?

                /*if($_SERVER['REMOTE_ADDR'] == '37.195.203.64') {

					$APPLICATION->IncludeComponent(
						"bitrix:sale.basket.basket",
						"",
						array(

							"PATH_TO_ORDER" => SITE_DIR . "order/",
							"HIDE_COUPON" => "N",
							"PRICE_VAT_SHOW_VALUE" => "Y",
							"COUNT_DISCOUNT_4_ALL_QUANTITY" => "Y",
							"USE_PREPAYMENT" => "N",
							"SET_TITLE" => "N",
							"AJAX_MODE_CUSTOM" => "Y",
							"SHOW_MEASURE" => "Y",
							"PICTURE_WIDTH" => "100",
							"PICTURE_HEIGHT" => "100",
							"SHOW_FULL_ORDER_BUTTON" => "Y",
							"SHOW_FAST_ORDER_BUTTON" => "Y",
							"COMPONENT_TEMPLATE" => "lovely",
							"QUANTITY_FLOAT" => "N",
							"ACTION_VARIABLE" => "action",
							"TEMPLATE_THEME" => "blue",
							"AUTO_CALCULATION" => "N",
							"COMPOSITE_FRAME_MODE" => "A",
							"COMPOSITE_FRAME_TYPE" => "AUTO",
							"USE_GIFTS" => "Y",
							"GIFTS_PLACE" => "BOTTOM",
							"GIFTS_BLOCK_TITLE" => "Выберите один из подарков",
							"GIFTS_HIDE_BLOCK_TITLE" => "N",
							"GIFTS_TEXT_LABEL_GIFT" => "Подарок",
							"GIFTS_PRODUCT_QUANTITY_VARIABLE" => "quantity",
							"GIFTS_PRODUCT_PROPS_VARIABLE" => "prop",
							"GIFTS_SHOW_OLD_PRICE" => "Y",
							"GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",
							"GIFTS_SHOW_NAME" => "Y",
							"GIFTS_SHOW_IMAGE" => "Y",
							"GIFTS_MESS_BTN_BUY" => "Выбрать",
							"GIFTS_MESS_BTN_DETAIL" => "Подробнее",
							"GIFTS_PAGE_ELEMENT_COUNT" => "1000",
							"GIFTS_CONVERT_CURRENCY" => "N",
							"GIFTS_HIDE_NOT_AVAILABLE" => "N",
							"CORRECT_RATIO" => "N",
							"USE_ENHANCED_ECOMMERCE" => "N"
						),
						false
					);
                } else {*/


					$APPLICATION->IncludeComponent(
						"bitrix:sale.basket.basket",
						"basket",
						array(

							"PATH_TO_ORDER" => SITE_DIR . "order/",
							"HIDE_COUPON" => "N",
							"PRICE_VAT_SHOW_VALUE" => "Y",
							"COUNT_DISCOUNT_4_ALL_QUANTITY" => "Y",
							"USE_PREPAYMENT" => "N",
							"SET_TITLE" => "N",
							"AJAX_MODE_CUSTOM" => "Y",
							"SHOW_MEASURE" => "Y",
							"PICTURE_WIDTH" => "100",
							"PICTURE_HEIGHT" => "100",
							"SHOW_FULL_ORDER_BUTTON" => "Y",
							"SHOW_FAST_ORDER_BUTTON" => "Y",
							"COMPONENT_TEMPLATE" => "lovely",
							"QUANTITY_FLOAT" => "N",
							"ACTION_VARIABLE" => "action",
							"TEMPLATE_THEME" => "blue",
							"AUTO_CALCULATION" => "N",
							"COMPOSITE_FRAME_MODE" => "A",
							"COMPOSITE_FRAME_TYPE" => "AUTO",
							"USE_GIFTS" => "Y",
							"GIFTS_PLACE" => "BOTTOM",
							"GIFTS_BLOCK_TITLE" => "Выберите один из подарков",
							"GIFTS_HIDE_BLOCK_TITLE" => "N",
							"GIFTS_TEXT_LABEL_GIFT" => "Подарок",
							"GIFTS_PRODUCT_QUANTITY_VARIABLE" => "quantity",
							"GIFTS_PRODUCT_PROPS_VARIABLE" => "prop",
							"GIFTS_SHOW_OLD_PRICE" => "Y",
							"GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",
							"GIFTS_SHOW_NAME" => "Y",
							"GIFTS_SHOW_IMAGE" => "Y",
							"GIFTS_MESS_BTN_BUY" => "Выбрать",
							"GIFTS_MESS_BTN_DETAIL" => "Подробнее",
							"GIFTS_PAGE_ELEMENT_COUNT" => "1000",
							"GIFTS_CONVERT_CURRENCY" => "N",
							"GIFTS_HIDE_NOT_AVAILABLE" => "N",
							"CORRECT_RATIO" => "N",
							"USE_ENHANCED_ECOMMERCE" => "N"
						),
						false
					);

				//}
			?>
        </div>
    </section>
    <section class="recommend cart">
        <div class="container">
			<?
			$APPLICATION->IncludeComponent(
                "bitrix:catalog.section",
                "recommended",
                array(
                    "IBLOCK_TYPE" => "catalog",
                    "IBLOCK_ID" => "1",
                    "DETAIL_URL" => "/catalog/#SECTION_CODE_PATH#/#ELEMENT_CODE#/",
                    "PRICE_CODE" => array(
                        0 => $price ?: "BASE",
                    ),
                    "COMPONENT_TEMPLATE" => "recommended",
                    "SECTION_CODE" => "",
                    "SECTION_USER_FIELDS" => array(
                        0 => "",
                        1 => "",
                    ),
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "SHOW_ALL_WO_SECTION" => "Y",
                    "CUSTOM_FILTER" => "{\"CLASS_ID\":\"CondGroup\",\"DATA\":{\"All\":\"AND\",\"True\":\"True\"},\"CHILDREN\":[{\"CLASS_ID\":\"CondIBProp:1:24\",\"DATA\":{\"logic\":\"Equal\",\"value\":5}}]}",
                    "HIDE_NOT_AVAILABLE" => "N",
                    "HIDE_NOT_AVAILABLE_OFFERS" => "N",
                    "ELEMENT_SORT_FIELD" => "sort",
                    "ELEMENT_SORT_ORDER" => "asc",
                    "ELEMENT_SORT_FIELD2" => "id",
                    "ELEMENT_SORT_ORDER2" => "desc",
                    "OFFERS_SORT_FIELD" => "sort",
                    "OFFERS_SORT_ORDER" => "asc",
                    "OFFERS_SORT_FIELD2" => "id",
                    "OFFERS_SORT_ORDER2" => "desc",
                    "PAGE_ELEMENT_COUNT" => "18",
                    "LINE_ELEMENT_COUNT" => "3",
                    "OFFERS_FIELD_CODE" => array(
                        0 => "",
                        1 => "",
                    ),
                    "OFFERS_LIMIT" => "5",
                    "BACKGROUND_IMAGE" => "-",
                    "SECTION_URL" => "",
                    "SECTION_ID_VARIABLE" => "SECTION_ID",
                    "SEF_MODE" => "N",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "36000000",
                    "CACHE_GROUPS" => "Y",
                    "SET_TITLE" => "Y",
                    "SET_BROWSER_TITLE" => "Y",
                    "BROWSER_TITLE" => "-",
                    "SET_META_KEYWORDS" => "Y",
                    "META_KEYWORDS" => "-",
                    "SET_META_DESCRIPTION" => "Y",
                    "META_DESCRIPTION" => "-",
                    "SET_LAST_MODIFIED" => "N",
                    "USE_MAIN_ELEMENT_SECTION" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "CACHE_FILTER" => "N",
                    "ACTION_VARIABLE" => "action",
                    "PRODUCT_ID_VARIABLE" => "id",
                    "USE_PRICE_COUNT" => "N",
                    "SHOW_PRICE_COUNT" => "1",
                    "PRICE_VAT_INCLUDE" => "Y",
                    "CONVERT_CURRENCY" => "N",
                    "BASKET_URL" => "/personal/basket.php",
                    "USE_PRODUCT_QUANTITY" => "N",
                    "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                    "ADD_PROPERTIES_TO_BASKET" => "Y",
                    "PRODUCT_PROPS_VARIABLE" => "prop",
                    "PARTIAL_PRODUCT_PROPERTIES" => "N",
                    "DISPLAY_COMPARE" => "N",
                    "PAGER_TEMPLATE" => ".default",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "PAGER_TITLE" => "Товары",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "PAGER_BASE_LINK_ENABLE" => "N",
                    "SET_STATUS_404" => "N",
                    "SHOW_404" => "N",
                    "MESSAGE_404" => "",
                    "COMPATIBLE_MODE" => "Y",
                    "DISABLE_INIT_JS_IN_COMPONENT" => "N"
                ),
                false
            ); ?>
        </div>
    </section>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>