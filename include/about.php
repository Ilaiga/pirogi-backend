<section class="about-block about-block--helper">
<div class="container">
	<div class="row about-block__row about-block__row--text-page">
		<div class="about-block__img col-12 col-xl-6">
 <img alt="Даниил Александрович" src="/upload/medialibrary/48c/48c93438a199b5851b0ac3c4d3a4213f.jpg" title="Даниил Александрович">
		</div>
		<div class="about-block__inner col-12 col-xl-5">
			<h1 class="title">История пекарни</h1>
			<p class="about-block__desc">
				 Федеральная сеть доставок&nbsp;«Вот такие пироги» работает с 2010 года, за это время мы завоевали любовь и заслуженное признание в 8 городах России.&nbsp;
			</p>
			<p class="about-block__desc">
				 У нашей компании два главных принципа: всегда к месту и всегда очень вкусно!&nbsp;Вы можете заказать пироги в офис по случаю обеденного перерыва или на корпоратив, на большую вечеринку или День Рождения. Не сомневайтесь, каждый из ваших коллег, гостей или друзей останется чрезвычайно сытым и довольным.&nbsp;
			</p>
			<p class="about-block__desc">
				 Попробуйте раз, и вы влюбитесь в них навсегда — тонкое тесто и большое количество начинки никого не оставят равнодушным.&nbsp;<br>
			</p>
		</div>
	</div>
</div>
 </section> <section class="about-block about-block--helper">
<div class="container">
	<div class="row about-block__row about-block__row--text-page">
		<div class="about-block__img about-block__img--helper col-12 col-xl-6">
 <img width="1024" alt="nIIfYVC2ID4.jpg" src="/upload/medialibrary/8df/8df0fa75580c7adbf7972fd170de7aa0.jpg" height="707" title="nIIfYVC2ID4.jpg">&nbsp; &nbsp;
		</div>
		<div class="about-block__inner about-block__inner--helper col-12 col-xl-5">
			<p class="sub-title">
				 Путешествие по миру вкусов
			</p>
			<p class="about-block__desc">
				 Главная задача — это возрождение кулинарных традиций разных стран мира. По нашим пирогам можно изучать кулинарную историю народов. Мы используем лучшие рецепты из Германии, Франции, Англии, Греции, Скандинавии, Сербии, Киргизии и других стран.&nbsp;
			</p>
			<p class="about-block__desc">
				 Компания принципиально не использует заготовки, пироги пекутся исключительно из свежих и натуральных продуктов. Поэтому заказ клиент получает не ранее чем через два часа, зато прямо из печи.
			</p>
			<p class="about-block__desc">
 <span style="letter-spacing: 0.64px;">&nbsp;«Вот такие пироги» — это не изделия из обычного теста, это кулинарное произведение искусства, по которому можно изучать историю различных стран, эпох и стилей.</span><br>
			</p>
		</div>
	</div>
</div>
 </section> <section class="about-block about-block--helper">
<div class="container">
	<div class="row about-block__row about-block__row--text-page">
		<div class="about-block__img col-12 col-xl-6">
			<img width="1000" alt="jZ-Wa6RJyiY.jpg" src="/upload/medialibrary/1d5/1d562d6033aea9e54d37ff8f966d9e7b.jpg" height="617" title="jZ-Wa6RJyiY.jpg">&nbsp; &nbsp;
		</div>
		<div class="about-block__inner col-12 col-xl-5">
			<p class="sub-title">
				 Все пироги готовятся исключительно на заказ!
			</p>
			<p class="about-block__desc">
				 Выпекаются тщательно, сродни домашней выпечке, и доставляются&nbsp;курьерами свежими, горячими, «с пылу с жару»&nbsp;прямиком из печи.
			</p>
			<p class="about-block__desc">
				 Увидеть всё разнообразие пирогов и заказать их онлайн можно на сайте: <a href="http://www.vot-takie-pirogi.ru">www.vot-takie-pirogi.ru</a>, или вы можете позвонить оператору по телефону, указанному в шапке сайта.
			</p>
			<p class="about-block__desc">
				 Приятного аппетита!
			</p>
		</div>
	</div>
</div>
 </section>