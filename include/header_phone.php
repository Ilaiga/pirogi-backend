<a class="main-header__info-phone" href="tel:+73833830888">
    <svg class="icon-svg">
        <use xlink:href="#icon-phone-circle"></use>
    </svg>
    <span>383-0-888</span>
</a>