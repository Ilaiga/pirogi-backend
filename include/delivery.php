<div class="container container--helper">
	<h1 class="title">Оплата и доставка</h1>
 <section class="delivery">
	<div class="delivery-info">
 <section class="delivery-info__inner">
		<p class="delivery-info__term">
			 Заказы принимаются
		</p>
		<p class="delivery-info__text">
			 Ежедневно с 9:00 до 20:00
		</p>
		<p class="delivery-info__term">
			 Доставка производится
		</p>
		<p class="delivery-info__text">
			 Ежедневно до 20:00
		</p>
		<p class="delivery-info__term">
			 Самовывоз
		</p>
		<p class="delivery-info__text">
			 Правый берег ул. Тополевая, дом 26. Левый берег ул. Вертковская 5/1
		</p>
		<br>
		</section><section class="delivery-info__inner">
		<ul class="delivery-info__list">
			<li class="delivery-info__item">
			<p class="delivery-info__text">
				 при оформлении заказа по телефону: банковской картой курьеру (необходимо об этом заранее предупредить оператора во время оформления заказа) или наличными курьеру при получении заказа
			</p>
 </li>
			<li class="delivery-info__item">
			<p class="delivery-info__text">
				 при оформлении заказа через сайт: наличными курьеру при получении заказа
			</p>
 </li>
			<li class="delivery-info__item">
			<p class="delivery-info__text">
				 при оформлении заказа через сайт: онлайн оплата банковской картой
			</p>
 </li>
			<li class="delivery-info__item">
			<p class="delivery-info__text">
				 оплата происходит через ПАО СБЕРБАНК с использованием банковских карт платежных систем: МИР, Visa, MasterCard
			</p>
 </li>
		</ul>
 </section> <section class="delivery-info__inner">
		<p class="delivery-info__term">
			 Обратите внимание
		</p>
		<ul class="delivery-info__list">
			<li class="delivery-info__item">
			<p class="delivery-info__text">
				 в случае, если заказ невозможно выполнить к желаемому сроку по причине загрузки мощностей, оператор предложит вам ближайшее время, к которому заказ может быть изготовлен и доставлен
			</p>
 </li>
			<li class="delivery-info__item">
			<p class="delivery-info__text">
				 при заказе на сумму от 5000 руб. вносится предоплата в размере 30%
			</p>
 </li>
			<li class="delivery-info__item">
			<p class="delivery-info__text">
				 скидки не суммируются
			</p>
 </li>
		</ul>
 <a class="link delivery__link" href="#">Условия возврата</a></section>
	</div>
 </section>
</div>
 <br>