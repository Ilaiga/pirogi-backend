<div class="social">
    <ul class="social__list">
        <li class="social__item">
            <a class="social__link social__link--vk" href="https://vk.com/vot_takie_pirogi_23" target="_blank">
                <svg class="icon-svg">
                    <use xlink:href="#icon-vk"></use>
                </svg>
            </a>
        </li>
        <li class="social__item">
            <a class="social__link social__link--fb"
               href="https://www.facebook.com/vottakiepirogi23/"
               target="_blank">
                <svg class="icon-svg">
                    <use xlink:href="#icon-fb"></use>
                </svg>
            </a>
        </li>
        <li class="social__item">
            <a class="social__link social__link--inst" href="https://www.instagram.com/vot_takie_pirogi_23/"
               target="_blank">
                <svg class="icon-svg">
                    <use xlink:href="#icon-inst"></use>
                </svg>
            </a>
        </li>
    </ul>
</div>