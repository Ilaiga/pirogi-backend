<div class="container container--helper">
	<h1 class="title">Оплата и доставка</h1>
 <section class="delivery">
	<div class="delivery-info">
 <section class="delivery-info__inner">
		<p class="delivery-info__term">
			 Заказы принимаются
		</p>
		<p class="delivery-info__text">
			 Ежедневно с 8:00 до 19:00
		</p>
		<p class="delivery-info__term">
			 Доставка производится
		</p>
		<p class="delivery-info__text">
			 Ежедневно c 10 до 22:00
		</p>
		<p class="delivery-info__term">
			 Самовывоз
		</p>
		<p class="delivery-info__text">
			 ул. Митрофана Седина 40
		</p>
 </section> <section class="delivery-info__inner">
		<p class="delivery-info__term">
			 Оплату можно произвести
		</p>
		<ul class="delivery-info__list">
			<p class="delivery-info__text">
				 Наличными курьеру при получении заказа
			</p>
			<p class="delivery-info__text">
				 Банковской картой курьеру (необходимо об этом заранее предупредить оператора во время оформления заказа)
			</p>
			<p class="delivery-info__text">
				 Онлайн оплата банковской картой при оформлении заказа через сайт (<span style="letter-spacing: 0.64px;">оплата происходит через ПАО СБЕРБАНК с использованием банковских карт платежных систем: МИР, Visa, MasterCard)</span>
			</p>
		</ul>
 </section> <section class="delivery-info__inner">
		<p class="delivery-info__term">
			 Обратите внимание
		</p>
		<p class="delivery-info__text">
			 Скидки и акции не суммируются. На товары по спец.цене скидки и акции не действуют.
		</p>
		<p class="delivery-info__text">
			 В случае, если заказ невозможно выполнить к желаемому сроку по причине загрузки мощностей, оператор предложит вам ближайшее время, к которому заказ может быть изготовлен и доставлен
		</p>
 <a class="link delivery__link" href="#">Условия возврата</a></section>
	</div>
 </section>
</div>
<!-- <section class="delivery-zone-info">-->
<!--<div class="container container--helper">-->
<!--	<h2 class="delivery-zone-info__title">Условия доставки</h2>-->
<!--	<div class="delivery-zone-info__inner delivery-zone-info__inner--green">-->
<!--		<p class="delivery-zone-info__term">-->
<!--			 Зеленая зона-->
<!--		</p>-->
<!--		<p class="delivery-zone-info__text">-->
<!--			 Бесплатно: заказ от 500 ₽-->
<!--		</p>-->
<!--		<p class="delivery-zone-info__text">-->
<!--			 Платная доставка: от 50 ₽-->
<!--		</p>-->
<!--		<p class="delivery-zone-info__text">-->
<!--			 Минимальный заказ: 370 ₽-->
<!--		</p>-->
<!--		<p class="delivery-zone-info__sub-text">-->
<!-- <br>-->
<!--		</p>-->
<!--	</div>-->
<!--	<div class="delivery-zone-info__inner delivery-zone-info__inner--yellow">-->
<!--		<p class="delivery-zone-info__term">-->
<!--			 Оранжевая зона-->
<!--		</p>-->
<!--		<p class="delivery-zone-info__text">-->
<!--			 Бесплатно:&nbsp;заказ от 1000 ₽-->
<!--		</p>-->
<!--		<p class="delivery-zone-info__text">-->
<!--			 Платная доставка: 150 ₽-->
<!--		</p>-->
<!--		<p class="delivery-zone-info__text">-->
<!--			 Минимальный заказ: 750 ₽-->
<!--		</p>-->
<!--		<p class="delivery-zone-info__sub-text">-->
<!-- <br>-->
<!--		</p>-->
<!--	</div>-->
<!--	<div class="delivery-zone-info__inner delivery-zone-info__inner--purple">-->
<!--		<p class="delivery-zone-info__term">-->
<!--			 Фиолетовая зона-->
<!--		</p>-->
<!--		<p class="delivery-zone-info__text">-->
<!--			 Бесплатно:&nbsp;заказ от 1500 ₽-->
<!--		</p>-->
<!--		<p class="delivery-zone-info__text">-->
<!--			 Платная доставка: 200 ₽-->
<!--		</p>-->
<!--		<p class="delivery-zone-info__text">-->
<!--			 Минимальный заказ: 1300 ₽-->
<!--		</p>-->
<!--		<p class="delivery-zone-info__sub-text">-->
<!-- <br>-->
<!--		</p>-->
<!--	</div>-->
<!--	<div class="delivery-zone-info__inner delivery-zone-info__inner--red">-->
<!--		<p class="delivery-zone-info__term">-->
<!--			 Красная зона-->
<!--		</p>-->
<!--		<p class="delivery-zone-info__text">-->
<!--			 Бесплатно:&nbsp;заказ от 2000 ₽-->
<!--		</p>-->
<!--		<p class="delivery-zone-info__text">-->
<!--			 Платная доставка: 250 ₽-->
<!--		</p>-->
<!--		<p class="delivery-zone-info__text">-->
<!--			 Минимальный заказ: 1700 ₽-->
<!--		</p>-->
<!--		<p class="delivery-zone-info__sub-text">-->
<!-- <br>-->
<!--		</p>-->
<!--	</div>-->
<!--</div>-->
<!-- </section>-->