<div class="contacts__inner contacts__inner--address">
    <span class="contacts__inner-icon">
        <svg class="icon-svg _address">
            <use xlink:href="/img/sprite.svg#icon-pin"></use>
        </svg>
    </span>
	<div class="contacts__address-wrapper">
		<p class="contacts__term">
			 Адрес пекарни
		</p>
		<p class="contacts__text">
			 Нижегородский район, Кожевенная 1а лит А
		</p>
	</div>
	<div class="contacts__address-wrapper">
		<p class="contacts__term">
 <br>
		</p>
	</div>
</div>
<div class="contacts__inner contacts__inner--phone">
    <span class="contacts__inner-icon">
    <svg class="icon-svg _phone">
        <use xlink:href="/img/sprite.svg#icon-phone"></use>
    </svg>
</span>
 <a class="link contacts__term" href="tel:+78312622700">26-22-700</a>
	<p class="contacts__text">
		 Принимаем заказы c 9:00 до 20:00
	</p>
	<p class="contacts__text">
		 Доставка c 9:00 до 22:00
	</p>
</div>
<div class="contacts__inner contacts__inner--email">
    <span class="contacts__inner-icon">
                    <svg class="icon-svg _mail">
                        <use xlink:href="/img/sprite.svg#icon-email"></use>
                    </svg>
                </span>
 <a class="link contacts__term" href="mailto:pirogi5452@gmail.com">pirogi5452@gmail.com</a>
	<p class="contacts__text">
		 Почта для партнеров
	</p>
	<p class="contacts__text">
	</p>
</div>
 <br>
 <br>