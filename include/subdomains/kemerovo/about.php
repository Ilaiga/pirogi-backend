<section class="about-block">
<div class="container">
	<div class="row about-block__row about-block__row--text-page">
		<div class="about-block__img col-12 col-xl-6">
 <img alt="Дети которые едят пирог" src="/img/about/about_1.jpg">
		</div>
		<div class="about-block__inner col-12 col-xl-5">
<h1 class="title">О нас</h1>
			<p class="sub-title">
				 Федеральная сеть&nbsp;доставок&nbsp;«Вот такие пироги»&nbsp;работает с 2010 года,&nbsp;за это время мы завоевали любовь и заслуженное признание в 8&nbsp; городах России.&nbsp; &nbsp; &nbsp;
			</p>
			<p class="about-block__desc">
				 У нашей компании два главных принципа: всегда к месту и всегда очень вкусно!
			</p>
			<p class="about-block__desc">
 <span style="letter-spacing: 0.64px;">Чтобы порадовать себя и любимых, не надо стоять у плиты после утомительного дня или в законный выходной. В ассортименте пекарни 50 видов пирогов. Сытные с мясом или курицей, сладкие с фруктами и шоколадом, вегетарианские с овощами, осетинские с сыром — все они призваны не только утолять голод, но и радовать вкусом и своим видом.&nbsp;</span>
			</p>
			<p class="about-block__desc">
 <span style="letter-spacing: 0.64px;">Вы можете заказать пироги в офис по случаю обеденного перерыва или на корпоратив, на большую вечеринку или День Рождения. Не сомневайтесь, каждый из ваших коллег, гостей или друзей останется чрезвычайно сытым и довольным. Попробуйте раз, и вы влюбитесь в них навсегда — тонкое тесто и большое количество начинки никого не оставят равнодушным.&nbsp;</span><br>
			</p>
		</div>
	</div>
</div>
 </section> <section class="about-block about-block--helper">
<div class="container">
	<div class="row about-block__row about-block__row--text-page">
		<div class="about-block__img about-block__img--helper col-12 col-xl-6">
 <img src="/img/about/about_us_01.jpg" alt="Пицца">
		</div>
		<div class="about-block__inner about-block__inner--helper col-12 col-xl-5">
			<p class="sub-title">
				 Путешествие по миру вкусов
			</p>
			<p class="about-block__desc">
				 Главная задача — это возрождение кулинарных традиций разных стран мира. По нашим пирогам можно изучать кулинарную историю народов. Мы используем лучшие рецепты из Германии, Франции, Англии, Греции, Скандинавии, Сербии, Киргизии и других стран.&nbsp;
			</p>
			<p class="about-block__desc">
				 Компания принципиально не использует заготовки, пироги пекутся исключительно из свежих и натуральных продуктов. Поэтому заказ клиент получает не ранее чем через два часа, зато прямо из печи.
			</p>
			<p class="about-block__desc">
 <span style="letter-spacing: 0.64px;">&nbsp;«Вот такие пироги» — это не изделия из обычного теста, это кулинарное произведение искусства, по которому можно изучать историю различных стран, эпох и стилей.</span><br>
			</p>
		</div>
	</div>
</div>
 </section> <section class="about-block about-block--helper">
<div class="container">
	<div class="row about-block__row about-block__row--text-page">
		<div class="about-block__img col-12 col-xl-6">
 <img src="/img/about/abou_us_02.jpg" alt="Пирог">
		</div>
		<div class="about-block__inner col-12 col-xl-5">
			<p class="sub-title">
				 Все пироги готовятся исключительно на заказ!
			</p>
			<p class="about-block__desc">
				 Выпекаются тщательно, сродни домашней выпечке, и доставляются вежливыми курьерами свежими, горячими, «с пылу с жару»&nbsp;прямиком из печи.
			</p>
			<p class="about-block__desc">
				 Увидеть всё разнообразие пирогов и заказать их онлайн можно на сайте: <a href="http://www.vot-takie-pirogi.ru">www.vot-takie-pirogi.ru</a>, или вы можете позвонить оператору по телефону 67-22-62, который легко поможет вам с выбором.&nbsp;
			</p>
			<p class="about-block__desc">
				 Приятного аппетита!
			</p>
			<p class="about-block__address">
				 ИП Смолянкин Олег Николаевич

ИНН 421164047317

ОГРНИП 312420529800072

Юр. Адрес: 650014, Кемерово, пр. Шахтёров, 123&nbsp;
			</p>
		</div>
	</div>
</div>
 </section>