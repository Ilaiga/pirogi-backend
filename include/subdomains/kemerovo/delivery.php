<div class="container container--helper">
	<h1 class="title">Оплата и доставка</h1>
 <section class="delivery">
	<div class="delivery-info">
 <section class="delivery-info__inner">
		<p class="delivery-info__term">
			 Заказы принимаются
		</p>
		<p class="delivery-info__text">
			 Ежедневно с 9:00 до 20:00
		</p>
		<p class="delivery-info__term">
			 Доставка производится
		</p>
		<p class="delivery-info__text">
			 Ежедневно c 10:00 до 22:00
		</p>
		<p class="delivery-info__term">
			 Самовывоз
		</p>
		<p class="delivery-info__text">
			 Заводской район Свободы ул., 12
		</p>
 </section> <section class="delivery-info__inner">
		<p class="delivery-info__term">
			 Оплату можно произвести
		</p>
		<p class="delivery-info__text">
			 Наличными курьеру при получении заказа
		</p>
		<p class="delivery-info__text">
			 Банковской картой курьеру (необходимо об этом заранее предупредить оператора во время оформления заказа)
		</p>
		<p class="delivery-info__text">
 <br>
		</p>
 </section> <section class="delivery-info__inner">
		<p class="delivery-info__term">
			 Обратите внимание
		</p>
		<p class="delivery-info__text">
			 Скидки и акции не суммируются. На товары по спец.цене скидки и акции не действуют.
		</p>
		<p class="delivery-info__text">
			 В случае, если заказ невозможно выполнить к желаемому сроку по причине загрузки мощностей, оператор предложит вам ближайшее время, к которому заказ может быть изготовлен и доставлен
		</p>
 <a class="link delivery__link" href="#">Условия возврата</a></section>
	</div>
 </section>
</div>
<!-- <section class="delivery-zone-info">-->
<!--<div class="container container--helper">-->
<!--	<h2 class="delivery-zone-info__title">Условия доставки</h2>-->
<!--	<div class="delivery-zone-info__inner delivery-zone-info__inner--green">-->
<!--		<p class="delivery-zone-info__term">-->
<!--			 Зеленая зона-->
<!--		</p>-->
<!--		<p class="delivery-zone-info__text">-->
<!--			 Бесплатно: от 900 ₽-->
<!--		</p>-->
<!--		<p class="delivery-zone-info__text">-->
<!--			 Платная доставка:&nbsp;50 ₽-->
<!--		</p>-->
<!--		<p class="delivery-zone-info__text">-->
<!--			 Минимальный заказ: 400 ₽-->
<!--		</p>-->
<!--		<p class="delivery-zone-info__sub-text">-->
<!-- <br>-->
<!--		</p>-->
<!--	</div>-->
<!--	<div class="delivery-zone-info__inner delivery-zone-info__inner--yellow">-->
<!--		<p class="delivery-zone-info__term">-->
<!--			 Оранжевая зона-->
<!--		</p>-->
<!--		<p class="delivery-zone-info__text">-->
<!--			 Бесплатно:&nbsp;от 1500 ₽-->
<!--		</p>-->
<!--		<p class="delivery-zone-info__text">-->
<!--			 Платная доставка: 150 ₽-->
<!--		</p>-->
<!--		<p class="delivery-zone-info__text">-->
<!--			 Минимальный заказ: 800 ₽-->
<!--		</p>-->
<!--	</div>-->
<!--</div>-->
<!-- </section>-->