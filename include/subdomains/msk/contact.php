<div class="contacts__inner contacts__inner--address">
     <span class="contacts__inner-icon">
        <svg class="icon-svg _address">
            <use xlink:href="/img/sprite.svg#icon-pin"></use>
        </svg>
    </span>
	<div class="contacts__address-wrapper">
		<p class="contacts__term">Правый берег (Москва)</p>
		<p class="contacts__text">Тополевая, 26 (заезд с Ипподромской магистрали)</p>
	</div>
	<div class="contacts__address-wrapper">
		<p class="contacts__term">Левый берег (Москва)</p>
		<p class="contacts__text">Вертковская, 5/1</p>
	</div>
</div>
<div class="contacts__inner contacts__inner--phone">
    <span class="contacts__inner-icon">
    <svg class="icon-svg _phone">
        <use xlink:href="/img/sprite.svg#icon-phone"></use>
    </svg>
</span>
    <a class="link contacts__term" href="tel:+73833830888">+7
		(383) 383 08-88</a>
	<p class="contacts__text">Принимаем заказы <span>c 9:00 до 20:00</span></p>
	<p class="contacts__text">Доставка <span>c 10:00 до 22:00</span></p>
</div>
<div class="contacts__inner contacts__inner--email">
    <span class="contacts__inner-icon">
                    <svg class="icon-svg _mail">
                        <use xlink:href="/img/sprite.svg#icon-email"></use>
                    </svg>
                </span>
	<a class="link contacts__term" href="mailto:takiepirogi@gmail.com">takiepirogi@gmail.com</a>
</div>