<div class="container container--helper"><h1 class="title">Оплата и доставка</h1>
	<section class="delivery">
		<div class="delivery-info">
			<section class="delivery-info__inner"><p class="delivery-info__term">Заказы принимаются</p>
				<p class="delivery-info__text">Ежедневно с 9:00 до 20:00</p>
				<p class="delivery-info__term">Доставка производится</p>
				<p class="delivery-info__text">Ежедневно до 20:00</p>
				<p class="delivery-info__term">Самовывоз</p>
				<p class="delivery-info__text">ул. Взлетная, 59. Вход слева (заказ необходимо оформить заранее по
					телефону или на сайте).</p></section>
			<section class="delivery-info__inner"><p class="delivery-info__term">Оплату можно произвести</p>
				<ul class="delivery-info__list">
					<li class="delivery-info__item"><p class="delivery-info__text">при оформлении заказа по
							телефону: банковской картой курьеру (необходимо об этом заранее предупредить оператора во
							время оформления заказа) или наличными курьеру при получении заказа</p></li>
					<li class="delivery-info__item"><p class="delivery-info__text">при оформлении заказа через сайт:
							наличными курьеру при получении заказа</p></li>
					<li class="delivery-info__item"><p class="delivery-info__text">при оформлении заказа через сайт:
							онлайн оплата банковской картой </p></li>
					<li class="delivery-info__item"><p class="delivery-info__text">оплата происходит через ПАО
							СБЕРБАНК с использованием банковских карт платежных систем: МИР, Visa, MasterCard</p></li>
				</ul>
			</section>
			<section class="delivery-info__inner"><p class="delivery-info__term">Обратите внимание</p>
				<ul class="delivery-info__list">
					<li class="delivery-info__item"><p class="delivery-info__text">в случае, если заказ невозможно
							выполнить к желаемому сроку по причине загрузки мощностей, оператор предложит вам ближайшее
							время, к которому заказ может быть изготовлен и доставлен </p></li>
					<li class="delivery-info__item"><p class="delivery-info__text">при заказе на сумму от 5000 руб.
							вносится предоплата в размере 30%</p></li>
					<li class="delivery-info__item"><p class="delivery-info__text">скидки не суммируются</p></li>
				</ul>
				<a class="link delivery__link" href="#">Условия возврата</a></section>
		</div>
	</section>
</div>
<!--<section class="delivery-zone-info">-->
<!--	<div class="container container--helper"><h2 class="delivery-zone-info__title">Условия доставки</h2>-->
<!--		<div class="delivery-zone-info__inner delivery-zone-info__inner--green"><p class="delivery-zone-info__term">-->
<!--				Зеленая зона</p>-->
<!--			<p class="delivery-zone-info__text">Бесплатно: <span>заказ от 480 ₽</span></p>-->
<!--			<p class="delivery-zone-info__text">Платная доставка: <span>от 50 ₽</span></p>-->
<!--			<p class="delivery-zone-info__text">Минимальный заказ: <span>350 ₽</span></p>-->
<!--			<p class="delivery-zone-info__sub-text">Центральный район, Железнодорожный район, пр. имени газеты-->
<!--				Красноярский рабочий, ул. Свердловская</p></div>-->
<!--		<div class="delivery-zone-info__inner delivery-zone-info__inner--yellow"><p-->
<!--				class="delivery-zone-info__term">Оранжевая зона</p>-->
<!--			<p class="delivery-zone-info__text">Бесплатно: <span>2 пирога и больше</span></p>-->
<!--			<p class="delivery-zone-info__text">Платная доставка: <span>50 ₽</span></p>-->
<!--			<p class="delivery-zone-info__text">Минимальный заказ: <span>350 ₽</span></p>-->
<!--			<p class="delivery-zone-info__sub-text">Кировский район, Октябрьский район, ул. Лесная, ш. Северное,-->
<!--				Бугач, Солонцы, тр-т Енисейский.</p></div>-->
<!--		<div class="delivery-zone-info__inner delivery-zone-info__inner--purple"><p-->
<!--				class="delivery-zone-info__term">Фиолетовая зона</p>-->
<!--			<p class="delivery-zone-info__text">Бесплатно: <span>3 пирога и больше</span></p>-->
<!--			<p class="delivery-zone-info__text">Платная доставка: <span>100 ₽</span></p>-->
<!--			<p class="delivery-zone-info__text">Минимальный заказ: <span>350 ₽</span></p>-->
<!--			<p class="delivery-zone-info__sub-text">Емельяново, Творогово, Логовой, Замятино, Шуваево, Сухая Балка,-->
<!--				Старцево, п. Минино, Элита, д. Минино, Бугачево, Березовка, Сухой Лог, Зыково, Лукино, Кузнецово</p>-->
<!--		</div>-->
<!--		<div class="delivery-zone-info__inner delivery-zone-info__inner--red"><p class="delivery-zone-info__term">-->
<!--				Красная зона</p>-->
<!--			<p class="delivery-zone-info__text">Бесплатно: <span>4 пирога и больше</span></p>-->
<!--			<p class="delivery-zone-info__text">Платная доставка: <span>200 ₽</span></p>-->
<!--			<p class="delivery-zone-info__text">Минимальный заказ: <span>800 ₽</span></p>-->
<!--			<p class="delivery-zone-info__sub-text">Березовка, Кузнецово, Зыково, Элита, Минино, Бугачево, Старцево,-->
<!--				Емельяново</p></div>-->
<!--	</div>-->
<!--</section>-->