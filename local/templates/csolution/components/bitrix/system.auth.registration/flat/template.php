<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */

/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


?>


<section class="login">
    <div class="container container--helper">
        <h1 class="title title__form">Регистрация</h1>
        <form class="form" method="post" action="<?=$arResult["AUTH_URL"]?>" name="bform" enctype="multipart/form-data">

			<?if($arResult["BACKURL"] <> ''):?>
                <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
			<?endif?>
            <input type="hidden" name="AUTH_FORM" value="Y" />
            <input type="hidden" name="TYPE" value="REGISTRATION" />

            <div class="input__inner input__inner--required">
                <input class="input" name="USER_NAME" type="text" value="<?=$arResult["USER_NAME"]?>" placeholder="Имя" required="required"/>
            </div>
            <div class="input__inner input__inner--required">
                <input class="input" name="USER_EMAIL" type="email" value="<?=$arResult["USER_EMAIL"]?>" placeholder="Email" required="required"/>
            </div>
            <div class="input__inner input__inner--required">
                <input class="input" name="USER_PASSWORD" type="password" value="<?=$arResult["USER_PASSWORD"]?>" placeholder="Пароль" required="required"/>

				<?if($arResult["SECURE_AUTH"]):?>
                    <span class="bx-auth-secure" id="bx_auth_secure" title="<?echo GetMessage("AUTH_SECURE_NOTE")?>" style="display:none">
					<div class="bx-auth-secure-icon"></div>
				</span>
                    <noscript>
				<span class="bx-auth-secure" title="<?echo GetMessage("AUTH_NONSECURE_NOTE")?>">
					<div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
				</span>
                    </noscript>
                    <script type="text/javascript">
                        document.getElementById('bx_auth_secure').style.display = 'inline-block';
                    </script>
				<?endif?>

            </div>
            <div class="input__inner input__inner--required">
                <input class="input" name="USER_CONFIRM_PASSWORD" value="<?=$arResult["USER_CONFIRM_PASSWORD"]?>" type="password" placeholder="Повторите пароль" required="required"/>
            </div>

            <p><?=$arResult['GROUP_POLICY']['PASSWORD_REQUIREMENTS']?></p>

            <input type = "hidden" name = "USER_LOGIN" value = "<?=$arResult["USER_EMAIL"]?>">
            <input type = "submit" class="btn sub-btn m-auto" name="Register" value="<?=GetMessage("AUTH_REGISTER")?>">
        </form>
    </div>
</section>


<script type="text/javascript">
    document.bform.USER_NAME.focus();
    $(document).ready(function () {
        $('input[name="USER_EMAIL"]').on('change', function () {
            $('input[name="USER_LOGIN"]').attr('value', $(this).val());
        });
    });
</script>
