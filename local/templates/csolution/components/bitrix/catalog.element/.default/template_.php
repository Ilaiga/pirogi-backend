<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(true);

$templateLibrary = array('popup', 'fx');
$currencyList = '';


if (!empty($arResult['CURRENCIES'])) {
	$templateLibrary[] = 'currency';
	$currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}

$templateData = array(
	'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
	'TEMPLATE_LIBRARY' => $templateLibrary,
	'CURRENCIES' => $currencyList,
	'ITEM' => array(
		'ID' => $arResult['ID'],
		'IBLOCK_ID' => $arResult['IBLOCK_ID'],
		'OFFERS_SELECTED' => $arResult['OFFERS_SELECTED'],
		'JS_OFFERS' => $arResult['JS_OFFERS']
	)
);
unset($currencyList, $templateLibrary);

$mainId = $this->GetEditAreaId($arResult['ID']);
$itemIds = array(
	'ID' => $mainId,
	'DISCOUNT_PERCENT_ID' => $mainId . '_dsc_pict',
	'STICKER_ID' => $mainId . '_sticker',
	'BIG_SLIDER_ID' => $mainId . '_big_slider',
	'BIG_IMG_CONT_ID' => $mainId . '_bigimg_cont',
	'SLIDER_CONT_ID' => $mainId . '_slider_cont',
	'OLD_PRICE_ID' => $mainId . '_old_price',
	'PRICE_ID' => $mainId . '_price',
	'DISCOUNT_PRICE_ID' => $mainId . '_price_discount',
	'PRICE_TOTAL' => $mainId . '_price_total',
	'SLIDER_CONT_OF_ID' => $mainId . '_slider_cont_',
	'QUANTITY_ID' => $mainId . '_quantity',
	'QUANTITY_DOWN_ID' => $mainId . '_quant_down',
	'QUANTITY_UP_ID' => $mainId . '_quant_up',
	'QUANTITY_MEASURE' => $mainId . '_quant_measure',
	'QUANTITY_LIMIT' => $mainId . '_quant_limit',
	'BUY_LINK' => $mainId . '_buy_link',
	'ADD_BASKET_LINK' => $mainId . '_add_basket_link',
	'BASKET_ACTIONS_ID' => $mainId . '_basket_actions',
	'NOT_AVAILABLE_MESS' => $mainId . '_not_avail',
	'COMPARE_LINK' => $mainId . '_compare_link',
	'TREE_ID' => $mainId . '_skudiv',
	'DISPLAY_PROP_DIV' => $mainId . '_sku_prop',
	'DISPLAY_MAIN_PROP_DIV' => $mainId . '_main_sku_prop',
	'OFFER_GROUP' => $mainId . '_set_group_',
	'BASKET_PROP_DIV' => $mainId . '_basket_prop',
	'SUBSCRIBE_LINK' => $mainId . '_subscribe',
	'TABS_ID' => $mainId . '_tabs',
	'TAB_CONTAINERS_ID' => $mainId . '_tab_containers',
	'SMALL_CARD_PANEL_ID' => $mainId . '_small_card_panel',
	'TABS_PANEL_ID' => $mainId . '_tabs_panel'
);
$obName = $templateData['JS_OBJ'] = 'ob' . preg_replace('/[^a-zA-Z0-9_]/', 'x', $mainId);
$name = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'])
	? $arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
	: $arResult['NAME'];
$title = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_TITLE'])
	? $arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_TITLE']
	: $arResult['NAME'];
$alt = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_ALT'])
	? $arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_ALT']
	: $arResult['NAME'];

$haveOffers = !empty($arResult['OFFERS']);
if ($haveOffers) {
	$actualItem = isset($arResult['OFFERS'][$arResult['OFFERS_SELECTED']])
		? $arResult['OFFERS'][$arResult['OFFERS_SELECTED']]
		: reset($arResult['OFFERS']);
	$showSliderControls = false;

	foreach ($arResult['OFFERS'] as $offer) {
		if ($offer['MORE_PHOTO_COUNT'] > 1) {
			$showSliderControls = true;
			break;
		}
	}
} else {
	$actualItem = $arResult;
	$showSliderControls = $arResult['MORE_PHOTO_COUNT'] > 1;
}

$skuProps = array();
$price = $actualItem['ITEM_PRICES'][$actualItem['ITEM_PRICE_SELECTED']];
$measureRatio = $actualItem['ITEM_MEASURE_RATIOS'][$actualItem['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'];
$showDiscount = $price['PERCENT'] > 0;

$showDescription = !empty($arResult['PREVIEW_TEXT']) || !empty($arResult['DETAIL_TEXT']);
$showBuyBtn = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION']);
$buyButtonClassName = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION_PRIMARY']) ? 'btn-default' : 'btn-link';
$showAddBtn = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION']);
$showButtonClassName = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION_PRIMARY']) ? 'btn-default' : 'btn-link';
$showSubscribe = $arParams['PRODUCT_SUBSCRIPTION'] === 'Y' && ($arResult['CATALOG_SUBSCRIBE'] === 'Y' || $haveOffers);

$arParams['MESS_BTN_BUY'] = $arParams['MESS_BTN_BUY'] ?: Loc::getMessage('CT_BCE_CATALOG_BUY');
$arParams['MESS_BTN_ADD_TO_BASKET'] = $arParams['MESS_BTN_ADD_TO_BASKET'] ?: Loc::getMessage('CT_BCE_CATALOG_ADD');
$arParams['MESS_NOT_AVAILABLE'] = $arParams['MESS_NOT_AVAILABLE'] ?: Loc::getMessage('CT_BCE_CATALOG_NOT_AVAILABLE');
$arParams['MESS_BTN_COMPARE'] = $arParams['MESS_BTN_COMPARE'] ?: Loc::getMessage('CT_BCE_CATALOG_COMPARE');
$arParams['MESS_PRICE_RANGES_TITLE'] = $arParams['MESS_PRICE_RANGES_TITLE'] ?: Loc::getMessage('CT_BCE_CATALOG_PRICE_RANGES_TITLE');
$arParams['MESS_DESCRIPTION_TAB'] = $arParams['MESS_DESCRIPTION_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_DESCRIPTION_TAB');
$arParams['MESS_PROPERTIES_TAB'] = $arParams['MESS_PROPERTIES_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_PROPERTIES_TAB');
$arParams['MESS_COMMENTS_TAB'] = $arParams['MESS_COMMENTS_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_COMMENTS_TAB');
$arParams['MESS_SHOW_MAX_QUANTITY'] = $arParams['MESS_SHOW_MAX_QUANTITY'] ?: Loc::getMessage('CT_BCE_CATALOG_SHOW_MAX_QUANTITY');
$arParams['MESS_RELATIVE_QUANTITY_MANY'] = $arParams['MESS_RELATIVE_QUANTITY_MANY'] ?: Loc::getMessage('CT_BCE_CATALOG_RELATIVE_QUANTITY_MANY');
$arParams['MESS_RELATIVE_QUANTITY_FEW'] = $arParams['MESS_RELATIVE_QUANTITY_FEW'] ?: Loc::getMessage('CT_BCE_CATALOG_RELATIVE_QUANTITY_FEW');


$price = $_SESSION['GET_CITY']['CODE_PRICE']['VALUE'] ?: 'BASE';
//echo $price;
//$positionClassMap = array(
?>

<? //='<pre>'.print_r($arResult, true).'</pre>';?>

<? if ($arResult['OFFERS']): ?>
    <section class="product">
        <div class="container">
            <div class="product__inner">
                <div class="product__char">


					<? if ($arResult['PROPERTIES']['ORDER_DAY']['VALUE'] == 'Y'): ?>
                        <span class="product__char-note">Заказ за сутки</span>
					<? endif; ?>

					<? if ($name = $arResult['PROPERTIES']['LABELS']['VALUE']): ?>
                        <span class="mark <?= $arResult['PROPERTIES']['LABELS']['DESCRIPTION'] ?>"><?= $name ?></span>
					<? endif; ?>


                    <span class="product__code"><?= $arResult['ID'] ?></span>
                    <div class="product__share">
                        <button class="product__share-toggle">
                            <svg class="icon-svg">
                                <use xlink:href="#icon-share"></use>
                            </svg>
                        </button>
                        <div class="js-product-share-dropdown product__share-dropdown">
                            <button class="product__share-social product__share-social--vk js-share" data-target="vk"
                                    data-url="<?= $arResult['DETAIL_PAGE_URL'] ?>" data-title="<?= $arResult['NAME'] ?>"
                                    data-img="<?= $arResult['PROPERTIES']['IMAGES']['VALUE'][0] ?>"
                                    data-text="Вкусный пирог">
                                <svg class="icon-svg">
                                    <use xlink:href="#icon-vk"></use>
                                </svg>
                            </button>
                            <button class="product__share-social product__share-social--fb js-share"
                                    data-target="facebook" data-url="<?= $arResult['DETAIL_PAGE_URL'] ?>"
                                    data-title="<?= $arResult['NAME'] ?>"
                                    data-img="<?= $arResult['PROPERTIES']['IMAGES']['VALUE'][1] ?>"
                                    data-text="Вкусный пирог">
                                <svg class="icon-svg">
                                    <use xlink:href="#icon-fb"></use>
                                </svg>
                            </button>
                            <button class="product__share-social product__share-social--ok js-share" data-target="ok"
                                    data-url="<?= $arResult['DETAIL_PAGE_URL'] ?>" data-title="<?= $arResult['NAME'] ?>"
                                    data-img="<?= $arResult['PROPERTIES']['IMAGES']['VALUE'][2] ?>"
                                    data-text="Вкусный пирог">
                                <svg class="icon-svg">
                                    <use xlink:href="#icon-ok-social"></use>
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>
                <h1 class="product__title"><?= $arResult['NAME'] ?></h1>
                <p class="product__weight">1000 г / 1400 г</p>
                <div class="product__slider-wrap">
                    <div class="product__slider">
						<? foreach ($arResult['PROPERTIES']['IMAGES']['VALUE'] as $IMAGE): ?>
                            <div class="product__slider-item"><img src="<?= CFile::GetPath($IMAGE) ?>" alt=""/></div>
						<? endforeach; ?>
                    </div>
                    <div class="product__slider-nav">
						<? foreach ($arResult['PROPERTIES']['IMAGES']['VALUE'] as $IMAGE): ?>
                            <div class="product__slider-nav-item"><img src="<?= CFile::GetPath($IMAGE) ?>" alt=""/>
                            </div>
						<? endforeach; ?>
                    </div>
                </div>
                <div class="product__info">
                    <p class="product__desc">
						<?= $arResult['DETAIL_TEXT'] ?>
                    </p>
                    <h3 class="product__sub-title">Состав:</h3>
                    <p class="product__desc">
						<?= $arResult['PROPERTIES']['FILLING']['VALUE']['TEXT'] ?>
                    </p>
                </div>
                <form class="product__controls js-toggles-parent">
                    <p class="product__controls-text">Толщина теста</p>
                    <p class="product__price js-price-wrap">
						<? $count = 0;
						foreach ($arResult['OFFERS'] as $arOffer): ?>
                            <span class="js-thin-price <?= $count ? 'hidden' : '' ?>"
                                  data-product="<?= $arOffer['ID'] ?>"><?= format_price($arOffer['PRICES'][$price]['VALUE']) ?></span>
							<? $count++; endforeach; ?>
                        <span class="currency">₽</span></p>
                    <div class="product__types js-toggles-wrap">
						<? $count = 0;
						foreach ($arResult['OFFERS'] as $arOffer): ?>
                            <button type="button" class="product__types-toggle <?= !$count ? 'toggle-active' : '' ?>"
                                    data-product="<?= $arOffer['ID'] ?>"><?= $arOffer['PROPERTIES']['BUTTON_TEXT']['VALUE'] ? $arOffer['PROPERTIES']['BUTTON_TEXT']['VALUE'] : '-' ?></button>
							<? $count++; endforeach; ?>
                    </div>
                    <div class="product__counter-wrap">
                        <button type="button" class="js-counter-minus product__counter-toggle">-</button>
                        <input class="js-counter-count product__counter-count" type="number" name="count" value="1"/>
                        <button type="button" class="js-counter-plus product__counter-toggle">+</button>
                    </div>

                    <input type="hidden" name="product_id"/>
                    <a class="product__order-btn btn order-btn ajax-order" href="javascript:;">В корзину
                        <svg class="icon-svg">
                            <use xlink:href="#icon-pie"></use>
                        </svg>
                    </a>
                </form>
            </div>
        </div>
    </section>
<? else: ?>
    <section class="product">
        <div class="container">
            <div class="product__inner">
                <div class="product__char">
					<? //=debug($arResult['PROPERTIES']['LABELS'])?>



					<? if ($name = $arResult['PROPERTIES']['LABELS']['VALUE']): ?>
                        <span class="mark <?= $arResult['PROPERTIES']['LABELS']['DESCRIPTION'] ?>"><?= $name ?></span>
					<? endif; ?>


					<? if ($arResult['PROPERTIES']['ORDER_DAY']['VALUE'] == 'Y'): ?>
                        <span class="product__char-note">Заказ за сутки</span>
					<? endif; ?>
                    <span class="product__code"><?= $arResult['ID'] ?></span>
                    <div class="product__share">
                        <button class="product__share-toggle">
                            <svg class="icon-svg">
                                <use xlink:href="#icon-share"></use>
                            </svg>
                        </button>
                        <div class="js-product-share-dropdown product__share-dropdown">
                            <button class="product__share-social product__share-social--vk js-share" data-target="vk"
                                    data-url="<?= $arResult['DETAIL_PAGE_URL'] ?>" data-title="<?= $arResult['NAME'] ?>"
                                    data-img="<?= $arResult['PROPERTIES']['IMAGES']['VALUE'][0] ?>"
                                    data-text="Вкусный пирог">
                                <svg class="icon-svg">
                                    <use xlink:href="#icon-vk"></use>
                                </svg>
                            </button>
                            <button class="product__share-social product__share-social--fb js-share"
                                    data-target="facebook" data-url="<?= $arResult['DETAIL_PAGE_URL'] ?>"
                                    data-title="<?= $arResult['NAME'] ?>"
                                    data-img="<?= $arResult['PROPERTIES']['IMAGES']['VALUE'][1] ?>"
                                    data-text="Вкусный пирог">
                                <svg class="icon-svg">
                                    <use xlink:href="#icon-fb"></use>
                                </svg>
                            </button>
                            <button class="product__share-social product__share-social--ok js-share" data-target="ok"
                                    data-url="<?= $arResult['DETAIL_PAGE_URL'] ?>" data-title="<?= $arResult['NAME'] ?>"
                                    data-img="<?= $arResult['PROPERTIES']['IMAGES']['VALUE'][2] ?>"
                                    data-text="Вкусный пирог">
                                <svg class="icon-svg">
                                    <use xlink:href="#icon-ok-social"></use>
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>
                <h1 class="product__title"><?= $arResult['NAME'] ?></h1>
                <div class="product__slider-wrap">
                    <div class="product__slider">
						<? foreach ($arResult['PROPERTIES']['IMAGES']['VALUE'] as $IMAGE): ?>
                            <div class="product__slider-item">
                                <img src="<?= CFile::GetPath($IMAGE) ?>" alt=""/>
                            </div>
						<? endforeach; ?>
                    </div>
                    <div class="product__slider-nav">
						<? foreach ($arResult['PROPERTIES']['IMAGES']['VALUE'] as $IMAGE): ?>
                            <div class="product__slider-nav-item">
                                <img src="<?= CFile::GetPath($IMAGE) ?>" alt=""/>
                            </div>
						<? endforeach; ?>
                    </div>
                </div>
                <div class="product__info">
                    <p class="product__desc">
						<?= $arResult['DETAIL_TEXT'] ?>
                    </p>
                    <h3 class="product__sub-title">Состав:</h3>
                    <p class="product__desc">
						<?= $arResult['PROPERTIES']['FILLING']['VALUE']['TEXT'] ?>
                    </p>
                </div>
                <form class="product__controls js-toggles-parent">
                    <p class="product__price js-price-wrap">
                        <span class="js-thin-price"><?= format_price($arResult['PRICES'][$price]['VALUE']) ?> </span>
                        <span class="currency">₽</span>
                    </p>
                    <div class="product__counter-wrap">
                        <button type="button" class="js-counter-minus product__counter-toggle">-</button>
                        <input class="js-counter-count product__counter-count" type="number" name="count" value="1"/>
                        <button type="button" class="js-counter-plus product__counter-toggle">+</button>
                    </div>

                    <input type="hidden" name="product_id" value="<?= $arResult['ID'] ?>"/>

                    <a class="product__order-btn btn order-btn ajax-order" href="javascript:;">В корзину
                        <svg class="icon-svg">
                            <use xlink:href="#icon-pie"></use>
                        </svg>
                    </a>
                </form>

            </div>
        </div>
    </section>
<? endif;
global $arrFilterTop;
$arrFilterTop = array("PROPERTY_RECOMMENDED_VALUE" => 'Y');
?>

<section class="reviews">
	<? $APPLICATION->IncludeComponent("bitrix:forum.topic.reviews", "csolutions", Array(
			"FILES_COUNT" => "0",
			"FORUM_ID" => "1",
			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"ELEMENT_ID" => $arResult['ID'],
			"SHOW_RATING" => "Y",
			"MESSAGES_PER_PAGE" => "5",
			"PAGE_NAVIGATION_TEMPLATE" => "csolution_red",
			"PREORDER" => "N",
			"DATE_TIME_FORMAT" => "d.m.Y",
			"RATING_TYPE" => "like",

		)
	); ?>
</section>

<section class="recommend">
    <div class="container">
		<? $APPLICATION->IncludeComponent(
			"bitrix:catalog.section",
			"recommended",
			array(
				"COMPONENT_TEMPLATE" => "catalog_main",
				"FILTER_NAME" => "arrFilterTop",
				"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
				"IBLOCK_ID" => $arParams["IBLOCK_ID"],
				"SECTION_USER_FIELDS" => array(
					0 => "",
					1 => "",
				),
				"INCLUDE_SUBSECTIONS" => "A",
				"SHOW_ALL_WO_SECTION" => "Y",
				"CUSTOM_FILTER" => "",
				"HIDE_NOT_AVAILABLE" => "N",
				"HIDE_NOT_AVAILABLE_OFFERS" => "N",
				"ELEMENT_SORT_FIELD" => "sort",
				"ELEMENT_SORT_ORDER" => "asc",
				"ELEMENT_SORT_FIELD2" => "id",
				"ELEMENT_SORT_ORDER2" => "desc",
				"OFFERS_SORT_FIELD" => "sort",
				"OFFERS_SORT_ORDER" => "asc",
				"OFFERS_SORT_FIELD2" => "id",
				"OFFERS_SORT_ORDER2" => "desc",
				"PAGE_ELEMENT_COUNT" => "3",
				"LINE_ELEMENT_COUNT" => "3",
				"PROPERTY_CODE" => array(
					0 => "",
					1 => "45",
					2 => "",
				),
				"PROPERTY_CODE_MOBILE" => array(),
				"OFFERS_FIELD_CODE" => array(
					0 => "NAME",
					1 => "",
				),
				"OFFERS_PROPERTY_CODE" => array(
					0 => "WEIGHT",
					1 => "",
				),
				"OFFERS_LIMIT" => "5",
				"BACKGROUND_IMAGE" => "-",
				"TEMPLATE_THEME" => "",
				"PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false}]",
				"ENLARGE_PRODUCT" => "PROP",
				"PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
				"SHOW_SLIDER" => "Y",
				"SLIDER_INTERVAL" => "3000",
				"SLIDER_PROGRESS" => "N",
				"PRODUCT_DISPLAY_MODE" => "N",
				"ADD_PICT_PROP" => "-",
				"LABEL_PROP" => array(),
				"PRODUCT_SUBSCRIPTION" => "Y",
				"SHOW_DISCOUNT_PERCENT" => "N",
				"SHOW_OLD_PRICE" => "N",
				"SHOW_MAX_QUANTITY" => "N",
				"SHOW_CLOSE_POPUP" => "N",
				"MESS_BTN_BUY" => "Купить",
				"MESS_BTN_ADD_TO_BASKET" => "В корзину",
				"MESS_BTN_SUBSCRIBE" => "Подписаться",
				"MESS_BTN_DETAIL" => "Подробнее",
				"MESS_NOT_AVAILABLE" => "Нет в наличии",
				"RCM_TYPE" => "personal",
				"RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
				"SHOW_FROM_SECTION" => "Y",
				"SECTION_URL" => "",
				"DETAIL_URL" => "/catalog/#SECTION_CODE#/#ELEMENT_CODE#/",
				"SECTION_ID_VARIABLE" => "SECTION_CODE",
				"SEF_MODE" => "Y",
				"AJAX_MODE" => "N",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N",
				"AJAX_OPTION_ADDITIONAL" => "",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "36000000",
				"CACHE_GROUPS" => "Y",
				"SET_TITLE" => "Y",
				"SET_BROWSER_TITLE" => "Y",
				"BROWSER_TITLE" => "-",
				"SET_META_KEYWORDS" => "Y",
				"META_KEYWORDS" => "-",
				"SET_META_DESCRIPTION" => "Y",
				"META_DESCRIPTION" => "-",
				"SET_LAST_MODIFIED" => "N",
				"USE_MAIN_ELEMENT_SECTION" => "N",
				"ADD_SECTIONS_CHAIN" => "N",
				"CACHE_FILTER" => "N",
				"ACTION_VARIABLE" => "action",
				"PRODUCT_ID_VARIABLE" => "id",
				"PRICE_CODE" => $arParams["~PRICE_CODE"],
				"USE_PRICE_COUNT" => "N",
				"SHOW_PRICE_COUNT" => "1",
				"PRICE_VAT_INCLUDE" => "Y",
				"CONVERT_CURRENCY" => "N",
				"BASKET_URL" => "/personal/basket.php",
				"USE_PRODUCT_QUANTITY" => "N",
				"PRODUCT_QUANTITY_VARIABLE" => "quantity",
				"ADD_PROPERTIES_TO_BASKET" => "Y",
				"PRODUCT_PROPS_VARIABLE" => "prop",
				"PARTIAL_PRODUCT_PROPERTIES" => "N",
				"PRODUCT_PROPERTIES" => array(),
				"OFFERS_CART_PROPERTIES" => array(
					0 => "WEIGHT",
				),
				"ADD_TO_BASKET_ACTION" => "ADD",
				"DISPLAY_COMPARE" => "N",
				"USE_ENHANCED_ECOMMERCE" => "N",
				"PAGER_TEMPLATE" => ".default",
				"DISPLAY_TOP_PAGER" => "N",
				"DISPLAY_BOTTOM_PAGER" => "Y",
				"PAGER_TITLE" => "Товары",
				"PAGER_SHOW_ALWAYS" => "N",
				"PAGER_DESC_NUMBERING" => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "N",
				"PAGER_BASE_LINK_ENABLE" => "N",
				"LAZY_LOAD" => "N",
				"LOAD_ON_SCROLL" => "N",
				"SET_STATUS_404" => "Y",
				"SHOW_404" => "N",
				"MESSAGE_404" => "",
				"COMPATIBLE_MODE" => "Y",
				"DISABLE_INIT_JS_IN_COMPONENT" => "N",
				"ENLARGE_PROP" => "-",
				"SEF_RULE" => "",
				"SECTION_CODE_PATH" => ""
			),
			false
		); ?>
    </div>
</section>

