<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent $component
 */

$component = $this->getComponent();
//$arParams = $component->applyTemplateModifications();
$price = $_SESSION['GET_CITY']['CODE_PRICE']['VALUE'] ?: 'BASE';
if(!$arResult['PRICES'][$price]['VALUE']) {
	LocalRedirect('/404.php');
}