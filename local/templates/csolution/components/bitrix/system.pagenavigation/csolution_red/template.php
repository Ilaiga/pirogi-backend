<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);


/*
 *
 * <ul class="pagination__list">
    <li class="pagination__item"><a class="pagination__link pagination__link--active" href="#">1</a></li>
    <li class="pagination__item"><a class="pagination__link" href="#">2</a></li>
    <li class="pagination__item"><a class="pagination__link" href="#">3</a></li>
    <li class="pagination__item"><a class="pagination__link" href="#">4</a></li>
    <li class="pagination__item"><a class="pagination__link" href="#">5</a></li>
    <li class="pagination__item"><a class="pagination__link" href="#">6</a></li>
</ul>
 *
 */

if (!$arResult["NavShowAlways"]) {
	if (
		(0 == $arResult["NavRecordCount"])
		||
		((1 == $arResult["NavPageCount"]) && (false == $arResult["NavShowAll"]))
	) {
		return;
	}
}

$navQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"] . "&amp;" : "");
$navQueryStringFull = ($arResult["NavQueryString"] != "" ? "?" . $arResult["NavQueryString"] : "");

?>
<ul class="pagination__list">
	<?php while ($arResult["nStartPage"] <= $arResult["nEndPage"]) { ?>
		<?php if ($arResult["nStartPage"] == $arResult["NavPageNomer"]) { ?>
            <li class="pagination__item">
                <a class="pagination__link pagination__link--active"><?= $arResult["nStartPage"] ?></a>
            </li>
		<?php } elseif ((1 == $arResult["nStartPage"]) && (false == $arResult["bSavePage"])) { ?>
            <li class="pagination__item">
                <a class="pagination__link"
                   href="<?php echo $arResult["sUrlPath"] ?><?php echo $navQueryStringFull ?>"><?= $arResult["nStartPage"] ?></a>
            </li>
		<?php } else { ?>
            <li class="pagination__item">
                <a class="pagination__link" href="<?php echo $arResult["sUrlPath"] ?>?<?php echo $navQueryString ?>PAGEN_<?php echo $arResult["NavNum"] ?>=<?php echo $arResult["nStartPage"] ?>">
                    <?= $arResult["nStartPage"] ?>
                </a>
            </li>
		<?php } ?>
		<?php $arResult["nStartPage"]++ ?>
	<?php } ?>
<ul>







