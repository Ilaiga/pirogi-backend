<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main,
	Bitrix\Main\Localization\Loc;

/**
 * @var array $arParams
 * @var array $arResult
 * @var CMain $APPLICATION
 * @var CUser $USER
 * @var SaleOrderAjax $component
 * @var string $templateFolder
 */

$context = Main\Application::getInstance()->getContext();
$request = $context->getRequest();


if($USER->IsAuthorized()) {
	$rsUser = CUser::GetByID($USER->getID());
	$arUser = $rsUser->Fetch();
}


?>

<section class="order">
    <div class="container">
        <div class="order__row row">
            <div class="order__cart col-12 col-lg-6 col-vl-4"><h2 class="order-cart__title title">Корзина</h2>
                <ul class="order__product-list">

                    <?foreach($arResult['BASKET_ITEMS'] as $BASKET_ITEM):?>
                        <li class="order__product-item row">
                            <div class="order__product-img col-5 col-md-3">
                                <img src="<?=$BASKET_ITEM['PREVIEW_PICTURE_SRC']?>" alt=""/>
                            </div>
                            <div class="order__product-inner col-6 col-md-9 row">
                                <div class="order__product-char col-md-9 col-lg-8">
                                    <p class="order__product-count"><?=$BASKET_ITEM['QUANTITY']?> шт</p>
                                    <p class="order__product-title">С капустой и грибами</p>
                                    <p class="order__product-weight">1400 г</p>
                                </div>
                                <p class="order__product-price col-md-3 col-lg-4">
                                    <span class="js-item-price"><?=$BASKET_ITEM['SUM_NUM']?></span>
                                    <span class="currency">₽</span>
                                </p>
                            </div>
                        </li>
                    <?endforeach;?>
                </ul>
                <div class="order__cart-total-wrap row">
                    <p class="order__cart-total col-12">Итого: <?=$arResult['ORDER_TOTAL_PRICE']?>₽</p>
                </div>
            </div>
            <form class="order__form col-12 col-lg-6 col-vl-8" method="post" action="">
                <h2 class="order__form-title title">Оформление заказа</h2>
                <div class="order__form-inner">
                    <div class="order__input-field">
                        <label class="order__label" for="order-name">Имя</label>
                        <input class="order__input input" type="text" value="<?=$arUser['NAME'] ? $arUser['NAME'] : ''?>" name="NAME" placeholder="Имя" id="order-name"/>
                    </div>
                    <div class="order__input-field">
                        <label class="order__label" for="order-phone">Телефон</label>
                        <input class="order__input input phone" type="tel" value="<?=$arUser['PERSONAL_PHONE'] ? $arUser['PERSONAL_PHONE'] : ''?>" name="PERSONAL_PHONE" placeholder="Телефон" id="order-phone"/>
                    </div>
                    <div class="order__input-field">
                        <label class="order__label" for="order-email">Почта</label>
                        <input class="order__input input" type="email" name="EMAIL" value="<?=$arUser['EMAIL'] ? $arUser['EMAIL'] : ''?>" placeholder="Почта" id="order-email"/>
                    </div>
                </div>
                <div class="order__form-inner">
                    <div class="order__form-tabs"><p class="order__form-tabs-text">Способ доставки:</p>
                        <div class="order__form-tabs-inner">
                            <label class="order__radio-label radio-label" for="delivery">
                                <input class="order__radio-input radio-input" type="radio" name="delivery" id="delivery" data-val="delivery" checked="checked"/>
                                <span class="order__radio-icon radio-icon"></span>
                                <span class="order__radio-text radio-text">Доставка курьером</span>
                            </label>
                            <label class="order__radio-label radio-label" for="pickup">
                                <input class="order__radio-input radio-input" type="radio" name="delivery" id="pickup" data-val="pickup"/>
                                <span class="order__radio-icon radio-icon"></span>
                                <span class="order__radio-text radio-text">Самовывоз (-10%)</span>
                            </label>
                        </div>
                    </div>
                    <div class="order__form-info">
                        <div class="order__form-delivery-wrap row">
                            <div class="order__form-delivery-wrap-inner order__form-delivery-wrap-inner--vl-first col-12">
                                <textarea class="order__textarea textarea" name="ADRESS" placeholder="Введите улицу, дом, квартиру, офис"></textarea>
                            </div>
                            <div class="order__form-delivery-wrap-inner order__form-delivery-wrap-inner--vl-right col-12 col-vl-6">
                                <div class="order-map"></div>
                            </div>
                            <div class="order__form-delivery-wrap-inner order__form-delivery-wrap-inner--vl-left col-12 col-vl-6">
                                <select class="order__select js-select-default" name = "PAY_SYSTEM">
                                    <?foreach ($arResult['PAY_SYSTEM'] as $PAY_SYSTEM):?>
                                        <option value="<?=$PAY_SYSTEM['ID']?>"><?=$PAY_SYSTEM['NAME']?></option>
                                        <option value="Картой курьеру">Картой курьеру</option>
                                    <?endforeach;?>
                                </select>
                                <input class="order__input input" type="date" name="DELIVERY_DATE" placeholder="Дата"/>
                                <input class="order__input input" type="time" name="DELIVERY_TIME" placeholder="Время"/>
                                <textarea class="order__comment-textarea textarea" name="" placeholder="Комментарий">
                                </textarea>
                            </div>
                        </div>
                        <div class="order__form-pickup-wrap row minimized">
                            <div class="order__form-delivery-wrap-inner order__form-delivery-wrap-inner--vl-right col-12 col-vl-6">
                                <div class="order__form-pickup-types">

                                    <?foreach($arResult['DELIVERY'] as $delivey):?>
                                        <label class="order__radio-label radio-label" for="label_<?=$delivey['ID']?>">
                                            <input class="order__radio-input radio-input" type="radio" data-delivery = "<?=$delivey['ID']?>" name="DELIVERY_ID" id="label_<?=$delivey['ID']?>"/>
                                            <span class="order__radio-icon radio-icon"></span>
                                            <span class="order__pickup-radio-text radio-text"><?=$delivey['NAME']?></span>
                                        </label>
                                    <?endforeach;?>


                                    <!--<label class="order__pickup-radio-label radio-label" for="left">
                                        <input class="order__pickup-radio-input radio-input" type="radio" name="pickup" id="left"/>
                                        <span class="order__pickup-radio-icon radio-icon"></span>
                                        <span class="order__pickup-radio-text radio-text">Левый берег: Вертковская, 5/1</span>
                                    </label>-->
                                </div>
                                <div class="order-map"></div>
                            </div>
                            <div class="order__form-delivery-wrap-inner order__form-delivery-wrap-inner--vl-left col-12 col-vl-6">
                                <select class="order__select js-select-default-2" name="PAY_SYSTEM_2">
                                    <option value="4">Онлайн оплата картой</option>
                                    <option value="5">Оплата на месте</option>
                                </select>
                                <div class="order__form-date-wrap">
                                    <input class="order__input order__date-input input" type="date" placeholder="Дата"/>
                                    <input class="order__input input" type="time" placeholder="Время"/>
                                </div>
                                <textarea class="order__comment-textarea textarea" name="COMMENTED" placeholder="Комментарий">
                                </textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="order__amount col-vl-6">
                    <div class="order__amount-inner">
                        <p class="order__amount-text col-12">Оплата
                            <span class="order__amount-choose js-amount-choose">наличными при получении</span>
                        </p>
                        <p class="order__amount-text js-amount-type col-12">Доставка
                            <span class="js-amount-type-price">250</span>
                            <span class="currency">₽</span>
                        </p>
                        <p class="order__amount-total col-12">Итого:
                            <span class="js-amount-total-price">5550</span>
                            <span class="currency">₽</span>
                        </p>
                    </div>
                </div>
                <div class="order__submit-wrap">
                    <button class="order__submit-btn btn order-btn" type="submit">Заказать</button>
                    <p class="policy">
                        Нажимая на кнопку «Оформить заказ», вы даете согласие на обработку
                        <a class="policy__link" href="#">своих персональных данных.</a>
                    </p>
                </div>
            </form>
        </div>
    </div>
</section>
