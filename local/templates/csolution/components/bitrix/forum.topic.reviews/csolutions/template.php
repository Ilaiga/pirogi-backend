<?php
CUtil::InitJSCore(array('ajax', 'fx'));
//echo debug($arResult);

global $USER;

if (!empty($arResult['NAV_RESULT'])) {
	$navParams = array(
		'NavPageCount' => $arResult['NAV_RESULT']->NavPageCount,
		'NavPageNomer' => $arResult['NAV_RESULT']->NavPageNomer,
		'NavNum' => $arResult['NAV_RESULT']->NavNum
	);
} else {
	$navParams = array(
		'NavPageCount' => 1,
		'NavPageNomer' => 1,
		'NavNum' => $this->randString()
	);
}
?>


    <div class="container reviews__inner">
        <h2 class="reviews__title h2">Отзывы
            (<?= $arResult['ELEMENT']['PRODUCT']['PROPERTY_FORUM_MESSAGE_CNT_VALUE'] ?: '0' ?>)</h2>

		<?php if (!$arResult['ELEMENT']['PRODUCT']['PROPERTY_FORUM_MESSAGE_CNT_VALUE']): ?>
            <p class="reviews__noreviews">Оставьте отзыв о пироге … первым!</p>
		<?php endif; ?>




		<? if ($arResult['SHOW_LIKE']): ?>
            <div class="reviews__rating"><p class="reviews__rating-text">Средний рейтинг:</p>
                <ul class="reviews__rating-list">
                    <li class="reviews__rating-item">
                        <svg class="icon-svg">
                            <use xlink:href="#icon-star"></use>
                        </svg>
                    </li>
                    <li class="reviews__rating-item">
                        <svg class="icon-svg">
                            <use xlink:href="#icon-star"></use>
                        </svg>
                    </li>
                    <li class="reviews__rating-item">
                        <svg class="icon-svg">
                            <use xlink:href="#icon-star"></use>
                        </svg>
                    </li>
                    <li class="reviews__rating-item">
                        <svg class="icon-svg">
                            <use xlink:href="#icon-star"></use>
                        </svg>
                    </li>
                    <li class="reviews__rating-item">
                        <svg class="icon-svg">
                            <use xlink:href="#icon-star"></use>
                        </svg>
                    </li>
                </ul>
            </div>

            <p class="reviews__statistic">
                <span class="green">100%</span> покупателей рекомендуют
            </p>
		<? endif; ?>


        <ul class="reviews__list">
			<? foreach ($arResult["MESSAGES"] as $MESSAGE): ?>
                <li class="reviews__item">

                    <div class="reviews__item-header">
                        <div class="reviews__item-avatar">
                            <svg class="icon-svg">
                                <use xlink:href="#icon-avatar"></use>
                            </svg>
                        </div>
                        <div class="reviews__item-author-info">
                            <p class="reviews__item-author-name"><?= $MESSAGE['AUTHOR_NAME'] ?></p>
                            <datetime class="reviews__item-date"><?= $MESSAGE['POST_DATE'] ?></datetime>

                            <div class="reviews_rating">
								<?
								$arRatingParams = Array(
									"ENTITY_TYPE_ID" => "FORUM_POST",
									"ENTITY_ID" => $MESSAGE["ID"],
									"OWNER_ID" => $MESSAGE["AUTHOR_ID"],
									"PATH_TO_USER_PROFILE" => strlen($arParams["PATH_TO_USER"]) > 0 ? $arParams["PATH_TO_USER"] : $arParams["~URL_TEMPLATES_PROFILE_VIEW"]
								);
								if (!isset($MESSAGE['RATING']))
									$MESSAGE['RATING'] = array(
										"USER_VOTE" => 0,
										"USER_HAS_VOTED" => 'N',
										"TOTAL_VOTES" => 0,
										"TOTAL_POSITIVE_VOTES" => 0,
										"TOTAL_NEGATIVE_VOTES" => 0,
										"TOTAL_VALUE" => 0
									);
								$arRatingParams = array_merge($arRatingParams, $MESSAGE['RATING']);
								$GLOBALS["APPLICATION"]->IncludeComponent("bitrix:rating.vote", "like", $arRatingParams, $component, array("HIDE_ICONS" => "Y"));
								?>
                            </div>

                        </div>
                    </div>
                    <p class="reviews__item-desc">
						<?= $MESSAGE['POST_MESSAGE'] ?>
                    </p>

					<? if ($USER->IsAdmin()): ?>
                        <div class="reviews_admin">
                            <a rel="nofollow" href="<?= $MESSAGE["URL"]["DELETE"] ?>"
                               style="color:red;"><?= GetMessage("F_DELETE") ?></a>
                            <a rel="nofollow" href="<?= $MESSAGE["URL"]["MODERATE"] ?>"
                               class="reviews-button-small"><?= GetMessage((($MESSAGE["APPROVED"] == 'Y') ? "F_HIDE" : "F_SHOW")) ?></a>
                        </div>
					<? endif; ?>

                </li>
			<? endforeach; ?>
        </ul>


        <button class="reviews__toggle js-modal-toggle btn" type="button" data-modal="#reviewsModal">Оставить
            отзыв
        </button>

        <div class="modal fade js-reviews-modal" id="reviewsModal">
            <div class="modal__dialog" role="document">
                <div class="modal__content">
                    <div class="modal__header"><h5 class="modal__title h2">Оставьте отзыв</h5>
                        <button class="modal__close" type="button"></button>
                    </div>
                    <form name="REPLIER<?= $arParams["form_index"] ?>" id="REPLIER<?= $arParams["form_index"] ?>"
                          action="<?= POST_FORM_ACTION_URI ?>#postform"<?
					?> method="POST" enctype="multipart/form-data" class="reviews-form">
                        <div class="modal__body">
                            <input type="hidden" name="back_page" value="<?= $arResult["CURRENT_PAGE"] ?>"/>
                            <input type="hidden" name="ELEMENT_ID" value="<?= $arParams["ELEMENT_ID"] ?>"/>
                            <input type="hidden" name="SECTION_ID"
                                   value="<?= $arResult["ELEMENT_REAL"]["IBLOCK_SECTION_ID"] ?>"/>
                            <input type="hidden" name="save_product_review" value="Y"/>
                            <input type="hidden" name="preview_comment" value="N"/>
							<?= bitrix_sessid_post() ?>
							<?
							if ($arParams['AUTOSAVE'])
								$arParams['AUTOSAVE']->Init();
							?>
                            <div class="reviews-reply-field reviews-reply-field-text">
								<?
								$LHE = new CLightHTMLEditor();

								$arEditorParams = array(
									'id' => "REVIEW_TEXT",
									'content' => isset($arResult["REVIEW_TEXT"]) ? $arResult["REVIEW_TEXT"] : "",
									'inputName' => "REVIEW_TEXT",
									'inputId' => "",
									'width' => "100%",
									'height' => "200px",
									'minHeight' => "200px",
									'bUseFileDialogs' => false,
									'bUseMedialib' => false,
									'BBCode' => true,
									'bBBParseImageSize' => true,
									'jsObjName' => "oLHE",
									'toolbarConfig' => array(),
									'smileCountInToolbar' => 3,
									'arSmiles' => $arSmiles,
									'bQuoteFromSelection' => true,
									'ctrlEnterHandler' => 'reviewsCtrlEnterHandler' . $arParams["form_index"],
									'bSetDefaultCodeView' => ($arParams['EDITOR_CODE_DEFAULT'] === 'Y'),
									'bResizable' => true,
									'bAutoResize' => true
								);

								$arEditorParams['toolbarConfig'] = forumTextParser::GetEditorToolbar(array('forum' => $arResult['FORUM']));
								$LHE->Show($arEditorParams);
								?>
                            </div>


                        </div>
                        <div class="modal__footer">
                            <button name="send_button" value="<?= GetMessage("OPINIONS_SEND") ?>"
                                    class="btn order-btn modal__submit"
                                    onclick="this.form.preview_comment.value = 'N';">Отправить
                                <svg class="icon-svg">
                                    <use xlink:href="#icon-mail"></use>
                                </svg>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>


    <div data-pagination-num="<?= $navParams['NavNum'] ?>">
		<?= $arResult['NAV_STRING'] ?>
    </div>


<?
if ($arParams['AUTOSAVE'])
	$arParams['AUTOSAVE']->LoadScript(array(
		"formID" => "REPLIER" . CUtil::JSEscape($arParams["form_index"]),
		"controlID" => "REVIEW_TEXT"
	));
?>
<?= ForumAddDeferredScript($this->GetFolder() . '/script_deferred.js') ?>