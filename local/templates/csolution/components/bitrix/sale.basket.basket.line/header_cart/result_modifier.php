<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

if(!empty($_SESSION['TOTAL_SUM_FORMATED']))
	$arResult['TOTAL_PRICE'] = $_SESSION['TOTAL_SUM_FORMATED'];
else
	$_SESSION['TOTAL_SUM_FORMATED'] = $arResult['TOTAL_PRICE'];