<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>



<?if($arParams['MOBILE_SHOW'] == 'Y'):?>
    <?switch($arParams['MOBILE_SHOW_PLACE']) {
        case 'CART': {
        ?>
            <a href="<?=$arParams['PATH_TO_BASKET']?>" class="main-header__cart">
                <button class="main-header__cart-toggle">
                    <svg class="icon-svg">
                        <use xlink:href="#icon-cart"></use>
                    </svg>
                </button>
                <p class="main-header__cart-amount">
                    <span class="cart-amount"><?=$arResult['TOTAL_PRICE'] ?></span>
                </p>
            </a>
        <?php
            break;
        }
        case 'LOGIN': {
        ?>
            <a class="main-header__user" href="<?=$arParams["PATH_TO_PERSONAL"]?>">
                <div class="main-header__user-icon">
                    <svg class="icon-svg">
                        <use xlink:href="#icon-human"></use>
                    </svg>
                </div>
                <p class="main-header__user-title">Вход</p>
                <?php if ($USER->IsAuthorized()) : ?>
                    <small class="main-header__user-note"><?=trim($USER->GetLogin())?></small>
                <?php else: ?>
                    <small class="main-header__user-note">Личный кабинет</small>
                <?php endif; ?>
            </a>

        <?php
            break;
        }
    }?>
<?endif;?>

<?if(!isset($arParams['MOBILE_SHOW']) || $arParams['MOBILE_SHOW'] == 'N'):?>
    <?
    if($arParams["SHOW_PERSONAL_LINK"] == "Y"):?>
        <a class="main-header__user" href="<?=$arParams["PATH_TO_PERSONAL"]?>">
            <div class="main-header__user-icon">
                <svg class="icon-svg">
                    <use xlink:href="#icon-human"></use>
                </svg>
            </div>
            <p class="main-header__user-title">Вход</p>

			<?php if ($USER->IsAuthorized()) : ?>
                <small class="main-header__user-note"><?=trim($USER->GetLogin())?></small>
			<?php else: ?>
                <small class="main-header__user-note">Личный кабинет</small>
			<?php endif; ?>

<!--            <small class="main-header__user-note">Личный кабинет</small>-->
        </a>
    <?endif;?>


    <?if($arParams['SHOW_TOTAL_PRICE'] == 'Y'):?>
        <a class="main-header__cart" href="<?=$arParams['PATH_TO_BASKET']?>">
            <div class="main-header__cart-toggle">
                <svg class="icon-svg">
                    <use xlink:href="#icon-cart"></use>
                </svg>
            </div>
            <p class="main-header__cart-amount">
                <span class="main-header__cart-text">Корзина</span>
                <span class="cart-amount"><?=$arResult['TOTAL_PRICE'] ?></span>
            </p>
        </a>
    <?endif;?>
<?endif;?>

