<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/**
 * @global string $componentPath
 * @global string $templateName
 * @var CBitrixComponentTemplate $this
 */

?>


<a class="sub-nav__cart" href="<?=$arParams['PATH_TO_BASKET']?>">
    <div class="sub-nav__cart-toggle">
        <svg class="icon-svg">
            <use xlink:href="#icon-cart"></use>
        </svg>
    </div>
    <p class="sub-nav__cart-amount">
        <span class="sub-nav__cart-text"></span>
        <span class="cart-amount"><?=$arResult['TOTAL_PRICE'] ?></span>
    </p>
</a>
