<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use Bitrix\Main\Localization\Loc;



//echo '<pre>'.print_r($arResult, true).'</pre>';
?>


<section class="settings-data">


    <div class="container">
        <h1 class="settings-data__title title">Личный кабинет</h1>
        <nav class="settings-nav">
            <ul class="settings-nav__list">
                <li class="settings-nav__item">
                    <a class="settings-nav__link settings-nav__link--active">Личные данные</a>
                </li>
                <li class="settings-nav__item">
                    <a href="/personal/orders/" class="settings-nav__link">История заказов</a>
                </li>

            </ul>
        </nav>
        <form class="settings-data__form" method="post" name="form1" action="<?= $APPLICATION->GetCurUri() ?>" enctype="multipart/form-data"
              role="form">
			<?= $arResult["BX_SESSION_CHECK"] ?>
            <input type="hidden" name="lang" value="<?= LANG ?>"/>
            <input type="hidden" name="ID" value="<?= $arResult["ID"] ?>"/>
            <input type="hidden" name="LOGIN" value="<?= $arResult["arUser"]["LOGIN"] ?>"/>
            <div class="row">

                <div class="settings-data__form-inner col-12 col-md-6">
                    <h2 class="settings-data__sub-title sub-title">Личные данные</h2>
                    <div class="settings-data__input-field">
                        <label class="settings-data__label label" for="settings-name">Имя</label>
                        <input class="settings-data__input input" type="text" name="NAME"
                               placeholder="Иван Иванов" value="<?= $arResult["arUser"]["NAME"] ?>" id="settings-name"/>
                    </div>
                    <div class="settings-data__input-field">
                        <label class="settings-data__label label" for="settings-phone">Телефон</label>
                        <input class="settings-data__input input phone" type="tel" name="PERSONAL_PHONE"
                               placeholder="+7 913 921 09 25" value="<?=$arResult['arUser']['PERSONAL_PHONE']?>" id="settings-phone"/>
                    </div>
                    <div class="settings-data__input-field">
                        <label class="settings-data__label label" for="settings-email">Email</label>
                        <input class="settings-data__input input" type="email" name="EMAIL"
                               placeholder="ivanivanov@gmail.com" value="<?= $arResult["arUser"]["EMAIL"] ?>" id="settings-email"/>
                    </div>
                    <div class="settings-data__input-field">
                        <label class="settings-data__label label" for="settings-password-old">Пароль</label>
                        <input class="settings-data__input input" type="password" placeholder="Введите старый пароль"
                               name="settings-password-old" id="settings-password-old"/>
                    </div>
                    <div class="settings-data__input-field">
                        <input class="settings-data__input input" type="password" placeholder="Новый пароль"
                               name="NEW_PASSWORD"
                               id="settings-password-new"/>
                    </div>
                    <div class="settings-data__input-field">
                        <input class="settings-data__input input" type="password" placeholder="Еще раз новый пароль"
                               name="NEW_PASSWORD_CONFIRM"
                               id="settings-password-repeat"/>
                    </div>
					<?
					ShowError($arResult["strProfileError"]);
					if ($arResult['DATA_SAVED'] == 'Y') {
						ShowNote(Loc::getMessage('PROFILE_DATA_SAVED'));
					}
					?>
                </div>
                <div class="settings-data__form-inner settings-data__form-inner--ml col-12 col-md-6 col-lg-5">
                    <h2 class="settings-data__sub-title sub-title">Адрес доставки</h2>
                    <div class="settings-data__address row">
                        <ul class="settings-data__address-list col-12">
                            <?foreach($arResult['arUser']['UF_ADRESS'] as $uKey => $uAdress):?>
                                <li class="settings-data__address-item">
                                    <p class="settings-data__address-text"><?=$uAdress?></p>
                                    <button data-key = "<?=$uKey?>" class="settings-data__address-remove link">Удалить</button>
                                    <input type="hidden" name="UF_ADRESS[<?=$uKey?>]" value="<?=$uAdress?>">
                                </li>
                            <?endforeach;?>
                        </ul>
                        <div class="settings-data__address-inner col-12">
                            <p class="settings-data__address-add-text">Добавить новый:</p>
                            <textarea class="textarea settings-data__address-textarea" name="UF_ADRESS[<?=count($arResult['arUser']['UF_ADRESS'])?>]"
                                      placeholder="Введите улицу, дом,квартиру, офис"
                                      rows="1"></textarea>
                        </div>
                    </div>
                    <div class="settings-data__subscribe"><h3 class="settings-data__subscribe-title">Рассылка</h3>
                        <p class="settings-data__subscribe-text">Подписка на рассылку акций, скидок, спец-предложений.
                            <label class="settings-data__checkbox-label checkbox-label">
                                <input class="checkbox" type="checkbox"/>
                                <span class="checkbox-custom"></span>
                            </label>
                        </p>
                    </div>
                </div>
                <input value="Сохранить" class="btn submit-btn settings-data__submit-btn col-12" name="save" type="submit">
            </div>
        </form>
    </div>
</section>
<script type="text/javascript">
    $('.settings-data__address-remove').on('click', function () {
        BX.showWait();

        $.ajax({
            url: '<?=SITE_TEMPLATE_PATH?>/ajax/personal.php',
            type: 'POST',
            context: this,
            data: {
                Key: $(this).data('key'),
                UserID: <?=$arResult["ID"]?>,
                RemoveAdress: true
            },
            success: function (response) {
                BX.closeWait();
                $(this).closest('.settings-data__address-item').remove();
                //console.log(response);
            }
        });
        return false;
    });
</script>