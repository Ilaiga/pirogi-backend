<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $APPLICATION;

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\UserTable;

if (strlen($arParams["MAIN_CHAIN_NAME"]) > 0)
{
	$APPLICATION->AddChainItem(htmlspecialcharsbx($arParams["MAIN_CHAIN_NAME"]), $arResult['SEF_FOLDER']);
}

$availablePages = array();

if(CUser::IsAuthorized()) {
	LocalRedirect('/personal/private/');
}
else {

    $APPLICATION->IncludeComponent(
        "bitrix:main.profile",
        "",
        Array(
            "SET_TITLE" =>$arParams["SET_TITLE"],
            "AJAX_MODE" => $arParams['AJAX_MODE_PRIVATE'],
            "SEND_INFO" => $arParams["SEND_INFO_PRIVATE"],
            "CHECK_RIGHTS" => $arParams['CHECK_RIGHTS_PRIVATE']
        ),
        $component
    );
}