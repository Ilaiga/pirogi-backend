<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $elementEdit
 * @var string $elementDelete
 * @var string $elementDeleteParams
 */

global $APPLICATION;

$price = $_SESSION['GET_CITY']['CODE_PRICE']['VALUE'] ?: 'BASE';

//echo $price;


//echo debug($arResult);

//echo debug(CPrice::GetByID(
//	757
//));

?>
<ul class="catalog__list">
    <div class="row">
		<?foreach ($arResult['ITEMS'] as $arItem): if(!$arItem['PRICES'][$price]['VALUE']) continue;
			if(!empty($arItem['OFFERS'])):
				$result = null;
				?>
                <li class="catalog__item catalog__item--large col-12 col-lg-6 col-vl-6">
                    <div class="catalog__item-img" style="background-image:url(<?=$arItem['PREVIEW_PICTURE']['src']?>)">
						<?if($name = $arItem['PROPERTIES']['LABELS']['VALUE']):?>
                            <span class="mark <?=$arItem['PROPERTIES']['LABELS']['DESCRIPTION']?>"><?=$name?></span>
						<?endif;?>
                    </div>

                    <div class="catalog__item-inner">
                        <h2 class="catalog__item-title"><a class="catalog__item-link" href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem['NAME']?></a></h2>
						<?foreach ($arItem['OFFERS'] as $key => $arOffer) {
							$result .= $arOffer['PROPERTIES']['WEIGHT']['VALUE'].' ';
						}?>
                        <small class="catalog__item-weight"><?=$result?></small>
                        <p class="catalog__item-desc"><?=$arItem['PREVIEW_TEXT']?></p>
                        <form class="catalog__item-info js-toggles-parent"><p class="catalog__item-info-text">Толщина теста</p>

                            <p class="catalog__item-price js-price-wrap">
								<?$count = 0; foreach ($arItem['OFFERS'] as $arOffer): ?>
                                    <span class="js-thin-price <?=$count ? 'hidden' : ''?>" data-product="<?=$arOffer['ID']?>"><?=format_price($arOffer['PRICES'][$price]['VALUE'])?></span>
									<?$count++; endforeach;?>
                                <span class="currency">₽</span></p>
                            <div class="catalog__item-types js-toggles-wrap">
								<?$count = 0; foreach ($arItem['OFFERS'] as $arOffer):?>
                                    <button type="button" class="product__types-toggle <?=!$count ? 'toggle-active' : ''?>" data-product="<?=$arOffer['ID']?>"><?=$arOffer['PROPERTIES']['BUTTON_TEXT']['VALUE'] ? $arOffer['PROPERTIES']['BUTTON_TEXT']['VALUE'] : '-'?></button>
									<?$count++; endforeach;?>
                            </div>
							<?php
							$arItemID = array_shift($arItem['OFFERS']);
							?>
                            <input type="hidden" name="product_id" value="<?=$arItemID['ID']?>"/>
                            <input type="hidden" name="priceId" value="<?=$arResult['PRICES'][$price]['ID']?>">
                            <a class="catalog__item-order-btn btn order-btn ajax-order" href="#">Заказать
                                <svg class="icon-svg">
                                    <use xlink:href="#icon-pie"></use>
                                </svg>
                            </a>
                        </form>
                    </div>
                </li>
			<?else:?>
                <li class="catalog__item catalog__item--large col-12 col-lg-6 col-vl-6">
                    <a href="<?=$arItem['DETAIL_PAGE_URL']?>">
                        <div class="catalog__item-img" style="background-image:url(<?=$arItem['PREVIEW_PICTURE']['src']?>)">
							<?if($name = $arItem['PROPERTIES']['LABELS']['VALUE']):?>
                                <span class="mark <?=$arItem['PROPERTIES']['LABELS']['DESCRIPTION']?>"><?=$name?></span>
							<?endif;?>
                        </div>
                    </a>

                    <div class="catalog__item-inner">
                        <h2 class="catalog__item-title"><a class="catalog__item-link" href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem['NAME']?></a></h2>
                        <small class="catalog__item-weight"><?=$result?></small>
                        <p class="catalog__item-desc"><?=$arItem['PREVIEW_TEXT']?></p>
                        <form class="catalog__item-info js-toggles-parent">
                            <p class="recommend__item-info-text"><?=$arItem['PROPERTIES']['WEIGHT']['VALUE']?></p>
                            <p class="recommend__item-price">
                                <span class="js-item-price"><?=format_price($arItem['PRICES'][$price]['VALUE'])?></span>
                                <span class="currency">₽</span>
                            </p>
                            <input type="hidden" name="product_id" value="<?=$arItem['ID']?>"/>
                            <input type="hidden" name="priceId" value="<?=$arResult['PRICES'][$price]['ID']?>">
                            <a class="catalog__item-order-btn btn order-btn ajax-order" href="#">Заказать
                                <svg class="icon-svg">
                                    <use xlink:href="#icon-pie"></use>
                                </svg>
                            </a>
                        </form>
                    </div>
                </li>

			<?endif;?>
		<?endforeach;?>
    </div>
</ul>
