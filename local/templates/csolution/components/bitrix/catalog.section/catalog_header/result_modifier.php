<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogTopComponent $component
 */

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

foreach ($arResult['ITEMS'] as $arKey => $arItem) {
	if(!empty($arItem['PREVIEW_PICTURE'])) {
		$image = CFile::ResizeImageGet($arItem['~PREVIEW_PICTURE'], array('width'=>750, 'height'=>220));
		$arResult['ITEMS'][$arKey]['PREVIEW_PICTURE'] = $image;
	}
}