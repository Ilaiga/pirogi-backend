<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 */


//echo debug($arResult);
//echo '<pre>'.print_r($arResult['ITEMS'], true).'</pre>';

$signer = new \Bitrix\Main\Security\Sign\Signer;
$signedTemplate = $signer->sign($templateName, 'catalog.section');
$signedParams = $signer->sign(base64_encode(serialize($arResult['ORIGINAL_PARAMETERS'])), 'catalog.section');


if (!empty($arResult['NAV_RESULT'])) {
	$navParams = array(
		'NavPageCount' => $arResult['NAV_RESULT']->NavPageCount,
		'NavPageNomer' => $arResult['NAV_RESULT']->NavPageNomer,
		'NavNum' => $arResult['NAV_RESULT']->NavNum
	);
} else {
	$navParams = array(
		'NavPageCount' => 1,
		'NavPageNomer' => 1,
		'NavNum' => $this->randString()
	);
}

if ($arParams['PAGE_ELEMENT_COUNT'] > 0 && $navParams['NavPageCount'] > 1) {
	$showTopPager = $arParams['DISPLAY_TOP_PAGER'];
	$showBottomPager = $arParams['DISPLAY_BOTTOM_PAGER'];
	$showLazyLoad = $arParams['LAZY_LOAD'] === 'Y' && $navParams['NavPageNomer'] != $navParams['NavPageCount'];
}

$showTopPager = true;

$price = $_SESSION['GET_CITY']['CODE_PRICE']['VALUE'] ?: 'BASE';

$discountPrice = false;
?>

    <ul class="catalog__list">
        <div class="row">
			<?
			foreach ($arResult['ITEMS'] as $arItem):

				if (!$arItem['PRICES'][$price]['VALUE']) continue;

				if($_SERVER['REMOTE_ADDR'] == '37.195.203.64')
					echo debug($arItem); ?>

                <li class="catalog__item js-flyToCart-wrap col-12 col-md-6 col-vl-4">
                    <a class="catalog__item-link catalog__item-link--img" href="<?=$arItem['DETAIL_PAGE_URL']?>">
                        <div class="catalog__item-img js-flyToCart-img" style="background-image:url(<?=$arItem['PREVIEW_PICTURE']['src']?>)">
							<?php if($name = $arItem['PROPERTIES']['LABELS']['VALUE']):

								$color = $arItem['PROPERTIES']['LABELS']['DESCRIPTION'];
								if(strlen($color) >= 7) {
									$color = 'ffbe0e';
								}

								?>
                                <span class="mark-item" style="--mark-item-bg: #<?=$color?>;">
                                    <?=$name?>
                                    <svg class="icon-svg">
                                    <use xlink:href="/img/sprite.svg#icon-mark"></use>
                                </svg>
                            </span>
							<?php endif; ?>
                        </div>
                    </a>
                    <div class="catalog__item-inner">
                        <h2 class="catalog__item-title">
                            <a class="catalog__item-link"
                               href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><?= $arItem['NAME'] ?></a>
                        </h2>
                        <p class="catalog__item-weight"><?= $arItem['PROPERTIES']['WEIGHT']['VALUE'] ?></p>
                        <p class="catalog__item-desc"><?= $arItem['PREVIEW_TEXT'] ?></p>
                        <form class="catalog__item-info js-toggles-parent">

							<?if($arItem['PRICES'][$price]['DISCOUNT_VALUE'] != $arItem['PRICES'][$price]['VALUE']): ?>
                                <div class="catalog__item-price-wrap">
                                    <p class="catalog__item-old-price">
                                        <span><?=format_price($arItem['PRICES'][$price]['VALUE'])?></span>
                                        <span class="currency">₽</span>
                                    </p>
                                    <p class="catalog__item-price js-price-wrap">
                                        <span><?=format_price($arItem['PRICES'][$price]['DISCOUNT_VALUE'])?></span>
                                        <span class="currency">₽</span>
                                    </p>
                                </div>
							<?php else: ?>
                                <div class="catalog__item-price-wrap">
                                    <p class="catalog__item-price js-price-wrap">
                                        <span><?=format_price($arItem['PRICES'][$price]['VALUE'])?></span>
                                        <span class="currency">₽</span>
                                    </p>
                                </div>
							<?php endif; ?>

                            <input type="hidden" name="product_id" value="<?= $arItem['ID'] ?>"/>
                            <input type="hidden" name="priceId" value="<?= $arResult['PRICES'][$price]['ID'] ?>">
                            <a class="catalog__item-order-btn btn order-btn ajax-order" href="#">Заказать
                                <svg class="icon-svg">
                                    <use xlink:href="#icon-pie"></use>
                                </svg>
                            </a>
                        </form>
                    </div>
                </li>
			<? endforeach; ?>
        </div>
    </ul>



