<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $elementEdit
 * @var string $elementDelete
 * @var string $elementDeleteParams
 */

global $APPLICATION;

$price = $_SESSION['GET_CITY']['CODE_PRICE']['VALUE'] ?: 'BASE';


?>
<ul class="catalog__list <?=!count($arResult['ITEMS']) ? 'hidden' : ''?>">
    <div class="row">
		<?foreach ($arResult['ITEMS'] as $arItem):

			if(!$arItem['PRICES'][$price]['VALUE']) continue;
            ?>
            <li class="catalog__item js-flyToCart-wrap col-12 col-md-6 col-vl-4">
                <a class="catalog__item-link catalog__item-link--img" href="<?=$arItem['DETAIL_PAGE_URL']?>">
                    <div class="catalog__item-img js-flyToCart-img" style="background-image:url(<?=$arItem['PREVIEW_PICTURE']['src']?>)">
						<?php if($name = $arItem['PROPERTIES']['LABELS']['VALUE']):

                                $color = $arItem['PROPERTIES']['LABELS']['DESCRIPTION'];
						        if(strlen($color) >= 7) {
						            $color = 'ffbe0e';
                                }

                            ?>
                            <span class="mark-item" style="--mark-item-bg: #<?=$color?>;">
                                    <?=$name?>
                                <svg class="icon-svg">
                                    <use xlink:href="/img/sprite.svg#icon-mark"></use>
                                </svg>
                            </span>
						<?php endif; ?>
                    </div>
                </a>
                <div class="catalog__item-inner">
                    <h2 class="catalog__item-title">
                        <a class="catalog__item-link" href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem['NAME']?></a>
                    </h2>
                    <p class="catalog__item-weight"><?=$arItem['PROPERTIES']['WEIGHT']['VALUE']?></p>
                    <p class="catalog__item-desc"><?=$arItem['PREVIEW_TEXT']?></p>
                    <form class="catalog__item-info js-toggles-parent">
                        <p class="catalog__item-price js-price-wrap">
                            <span class="js-thin-price"><?=format_price($arItem['PRICES'][$price]['VALUE'])?></span>
                            <span class="js-fat-price hidden"><?=format_price($arItem['PRICES'][$price]['VALUE'])?></span>
                            <span class="currency">₽</span>
                        </p>
                        <input type="hidden" name="product_id" value="<?=$arItem['ID']?>"/>
                        <input type="hidden" name="priceId" value="<?=$arResult['PRICES'][$price]['ID']?>">
                        <a class="catalog__item-order-btn btn order-btn ajax-order " href="#">Заказать
                            <svg class="icon-svg">
                                <use xlink:href="#icon-pie"></use>
                            </svg>
                        </a>
                    </form>
                </div>
            </li>
		<?endforeach;?>
    </div>
</ul>
