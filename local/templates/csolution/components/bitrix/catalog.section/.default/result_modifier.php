<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogSectionComponent $component
 */

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

foreach ($arResult['ITEMS'] as $arKey => $arItem) {
	if(!empty($arItem['PREVIEW_PICTURE'])) {
		$image = CFile::ResizeImageGet($arItem['~PREVIEW_PICTURE'], array('width'=>350, 'height'=>200));
		$arResult['ITEMS'][$arKey]['PREVIEW_PICTURE'] = $image;
	}
}