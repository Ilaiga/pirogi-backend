<?php

$price = $_SESSION['GET_CITY']['CODE_PRICE']['VALUE'] ?: 'BASE';
$recommendShow = false;

foreach ($arResult['ITEMS'] as $arKey => $arItem) {
	if(!$arItem['PRICES'][$price]['VALUE']) continue;
	if(!empty($arItem['PREVIEW_PICTURE'])) {
		$image = CFile::ResizeImageGet($arItem['~PREVIEW_PICTURE'], array('width'=>350, 'height'=>200));
		$arResult['ITEMS'][$arKey]['PREVIEW_PICTURE'] = $image;
	}
	$recommendShow = true;
}

?>


<?php if($recommendShow): ?>
    <section class="recommend cart">
        <div class="container">
            <h2 class="recommend__title h2">Не забудьте заказать</h2>
            <ul class="recommend__list">
                <div class="row">
					<?foreach ($arResult['ITEMS'] as $arItem):
						    if(!$arItem['PRICES'][$price]['VALUE']) continue;

						?>
                        <li class="recommend__item col-12 col-md-6 col-vl-4">
                            <a class="recommend__item-link recommend__item-link--img" href="<?=$arItem['DETAIL_PAGE_URL']?>">
                                <div class="recommend__item-img" style="background-image:url(<?=$arItem['PREVIEW_PICTURE']['src']?>)"></div>
                            </a>
                            <div class="recommend__item-inner">
                                <h2 class="recommend__item-title">
                                    <a class="recommend__item-link" href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem['NAME']?></a>
                                </h2>
                                <p class="recommend__item-weight"><?=$arItem['PROPERTIES']['WEIGHT']['VALUE']?></p>
                                <p class="recommend__item-desc"><?=$arItem['PREVIEW_TEXT']?></p>
                                <form class="recommend__item-info js-toggles-parent">
                                    <p class="recommend__item-price js-price-wrap">
                                        <span class="js-thin-price"><?=format_price($arItem['PRICES'][$price]['VALUE'])?></span>
                                        <span class="js-fat-price hidden"></span>
                                        <span class="currency">₽</span>
                                    </p>
                                    <input type="hidden" name="product_id" value="<?=$arItem['ID']?>"/>
                                    <input type="hidden" name="priceId" value="<?=$arResult['PRICES'][$price]['ID']?>">
                                    <a class="recommend__item-order-btn btn order-btn ajax-order" href="#">Заказать
                                        <svg class="icon-svg">
                                            <use xlink:href="#icon-pie"></use>
                                        </svg>
                                    </a>
                                </form>
                            </div>
                        </li>
					<?php endforeach; ?>
                </div>
            </ul>
        </div>
    </section>
<?php endif; ?>

