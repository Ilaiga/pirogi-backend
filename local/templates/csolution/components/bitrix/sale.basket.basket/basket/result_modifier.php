<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */

/** @var array $arResult */

use Bitrix\Main;

use Bitrix\Sale\DiscountCouponsManager;

if (!empty($arResult["ERROR_MESSAGE"]))
	ShowError($arResult["ERROR_MESSAGE"]);

$bDelayColumn = false;
$bDeleteColumn = false;
$bWeightColumn = false;
$bPropsColumn = false;
$bPriceType = false;


foreach ($arResult['APPLIED_DISCOUNT_LIST'] as $arCoupon) {
	$COUPON = $arCoupon['COUPON']['COUPON'];
	break;
}

if (!empty($arResult['TOTAL_RENDER_DATA']['PRICE_FORMATED'])) {
	$arResult['TOTAL_RENDER_DATA']['PRICE_FORMATED'] = str_replace(' руб.', '', $arResult['TOTAL_RENDER_DATA']['PRICE_FORMATED']);
}
if (!empty($arResult['TOTAL_RENDER_DATA']['PRICE_WITHOUT_DISCOUNT_FORMATED'])) {
	$arResult['TOTAL_RENDER_DATA']['PRICE_WITHOUT_DISCOUNT_FORMATED'] = str_replace(' руб.', '', $arResult['TOTAL_RENDER_DATA']['PRICE_WITHOUT_DISCOUNT_FORMATED']);
}


$APPLICATION->SetTitle("Корзина");


?>

<ul class="cart__list">
	<? foreach ($arResult['BASKET_ITEM_RENDER_DATA'] as $baskeKey => $basketItem):
		//@TODO переделать

//        echo debug($basketItem);

		$arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_*");
		$arFilter = Array("=ID" => $basketItem['PRODUCT_ID'], "ACTIVE" => "Y");
		$res = CIBlockElement::GetList(Array(), $arFilter, false, array(), $arSelect);
		if ($product = $res->GetNextElement()) {
			$product = $product->GetProperties();
		}

		?>
        <li class="cart__item row" id="<?= $basketItem["ID"] ?>" data-productId="<?= $basketItem['PRODUCT_ID'] ?>">
            <div class="col-10 col-sm-5 col-md-3 col-xl-4 offset-sm-1 offset-md-0 order-sm-2">
                <p class="cart__item-title"><?= $basketItem['NAME'] ?></p>
                <div class="cart__item-weight-wrap">
                    <span class="cart__item-weight"><?= $product['WEIGHT']['VALUE'] ?></span>
                </div>
            </div>
            <div class="cart__item-remove-wrap col-2 col-sm-1 order-sm-3 order-md-5">
                <button class="cart__remove-toggle" onclick="removeItem(<?= $basketItem["ID"] ?>)">
                    <svg class="icon-svg">
                        <use xlink:href="#icon-cross"></use>
                    </svg>
                </button>
            </div>


            <div class="cart__item-img col-12 col-sm-5 col-md-3 col-xl-2 order-sm-1">
                <img src="<?= $basketItem['IMAGE_URL'] ?>" alt=""/>
            </div>
            <div class="col-7 col-sm-5 order-sm-4 col-md-3 order-md-3 d-flex align-items-center">
                <div class="cart__counter-wrap">
                    <button class="js-counter-minus cart__counter-toggle"
                            onclick="setQuantity(<?= $basketItem["ID"] ?>, 1, 'down', false);">-
                    </button>
					<?
					$ratio = isset($basketItem["MEASURE_RATIO"]) ? $basketItem["MEASURE_RATIO"] : 0;
					$max = isset($basketItem["AVAILABLE_QUANTITY"]) ? "max=\"" . $basketItem["AVAILABLE_QUANTITY"] . "\"" : "";
					$useFloatQuantity = ($arParams["QUANTITY_FLOAT"] == "Y") ? true : false;
					$useFloatQuantityJS = ($useFloatQuantity ? "true" : "false");
					?>

                    <input
                            type="text"
                            id="QUANTITY_INPUT_<?= $basketItem["ID"] ?>"
                            name="QUANTITY_INPUT_<?= $basketItem["ID"] ?>"
                            size="2"
                            maxlength="18"
                            min="0"
                            class="js-counter-count cart__counter-count"
						<?= $max ?>
                            step="<?= $ratio ?>"
                            value="<?= $basketItem["QUANTITY"] ?>"
                            onchange="updateQuantity('QUANTITY_INPUT_<?= $basketItem["ID"] ?>', '<?= $basketItem["ID"] ?>', <?= $ratio ?>, <?= $useFloatQuantityJS ?>)"
                    />
                    <button class="js-counter-plus cart__counter-toggle"
                            onclick="setQuantity(<?= $basketItem["ID"] ?>, 1, 'up', false);">+
                    </button>
                </div>
                <input type="hidden" class="input-hidden" id="QUANTITY_<?= $basketItem['ID'] ?>"
                       name="QUANTITY_<?= $basketItem['ID'] ?>" value="<?= $basketItem["QUANTITY"] ?>"/>
            </div>
            <div class="cart__item-price-wrap col-5 col-sm-5 col-md-2 order-md-4 offset-sm-1 offset-md-0 order-sm-5">
                <p class="cart__item-price">


					<?php if ($name = $product['LABELS']['VALUE']):

						$color = $product['LABELS']['DESCRIPTION'];
						if (strlen($color) >= 7) {
							$color = 'ffbe0e';
						}

						?>
                        <span class="mark-item _small" style="--mark-item-bg: #<?= $color ?>;">
                                    <?= $name ?>
                            <svg class="icon-svg">
                                    <use xlink:href="/img/sprite.svg#icon-mark"></use>
                                </svg>
                            </span>
					<?php endif; ?>

					<?php if ($basketItem['SUM_FULL_PRICE'] != $basketItem['SUM_PRICE']): ?>
                        <span class="cart__item-old-price"
                              id="PRODUCT_DISCOUNT_PRICE_<?= $basketItem['ID'] ?>"><?= format_price($basketItem['SUM_FULL_PRICE']) ?>₽</span>
                        <span class="js-item-price"
                              id="PRODUCT_PRICE_<?= $basketItem['ID'] ?>"><?= format_price($basketItem['SUM_PRICE']) ?>₽</span>
					<?php else: ?>
                        <span class="js-item-price"
                              id="PRODUCT_PRICE_<?= $basketItem['ID'] ?>"><?= format_price($basketItem['SUM_FULL_PRICE']) ?>₽</span>
					<?php endif; ?>
                </p>
            </div>
        </li>
	<?php endforeach; ?>
</ul>


<div class="cart__amount row">
    <div class="col-12 col-md-5 col-lg-4 col-xl-3">
        <div class="cart__back">
            <a class="link cart__back-link" href="/catalog">Продолжить покупки
                <svg class="icon-svg">
                    <use xlink:href="#icon-arrow"></use>
                </svg>
            </a>
        </div>
    </div>
    <div class="col-12 col-md-6 col-lg-4 col-xl-5 col-vl-4 offset-md-1 offset-lg-0">
        <div class="cart__promocode-wrap input-promocode-wrap">
            <input class="input input-promocode" name="promocode" type="text" id="coupon"
                   placeholder="Введите промокод" <?= $COUPON ? 'disabled' : '' ?> name="COUPON"
                   value="<?= $COUPON ?>"/>
            <a class="ajax js-input-promocode-submit input-promocode-submit" <?= !$COUPON ? 'onclick="enterCoupon();"' : '' ?>>
                <svg class="icon-svg">
                    <use xlink:href="#icon-arrow"></use>
                </svg>
                <svg class="icon-svg-checked">
                    <use xlink:href="#icon-checked"></use>
                </svg>
                <svg class="icon-svg-error">
                    <use xlink:href="#icon-error"></use>
                </svg>
            </a>
        </div>
    </div>
    <div class="cart__amount-wrap col-12 col-md-5 col-lg-4 col-xl-4 col-vl-5 offset-md-7 offset-lg-0">
        <div class="cart__amount-inner">
            <p class="cart__amount-total">Итого:
				<?php if (!empty($arResult['TOTAL_RENDER_DATA']['PRICE_WITHOUT_DISCOUNT_FORMATED']) && ($arResult['TOTAL_RENDER_DATA']['PRICE_WITHOUT_DISCOUNT_FORMATED'] != $arResult['TOTAL_RENDER_DATA']['PRICE_FORMATED'])): ?>
                    <span class="cart__amount-total--old"><?= format_price((int)$arResult['allSum'] + (int)$arResult['DISCOUNT_PRICE_ALL']) ?>₽</span>
				<? endif; ?>
                <span class="cart__amount-price"> <?= format_price((int)$arResult['TOTAL_RENDER_DATA']['PRICE']) ?>₽</span>
            </p>
        </div>
        <a class="btn btn--accept cart__btn" href="/order">Оформить заказ</a>
    </div>
</div>


<input type="hidden" id="column_headers" value="<?= CUtil::JSEscape(implode($arHeaders, ",")) ?>"/>
<input type="hidden" id="offers_props" value="<?= CUtil::JSEscape(implode($arParams["OFFERS_PROPS"], ",")) ?>"/>
<input type="hidden" id="action_var" value="<?= CUtil::JSEscape($arParams["ACTION_VARIABLE"]) ?>"/>
<input type="hidden" id="quantity_float" value="<?= $arParams["QUANTITY_FLOAT"] ?>"/>
<input type="hidden" id="count_discount_4_all_quantity"
       value="<?= ($arParams["COUNT_DISCOUNT_4_ALL_QUANTITY"] == "Y") ? "Y" : "N" ?>"/>
<input type="hidden" id="price_vat_show_value" value="<?= ($arParams["PRICE_VAT_SHOW_VALUE"] == "Y") ? "Y" : "N" ?>"/>
<input type="hidden" id="hide_coupon" value="<?= ($arParams["HIDE_COUPON"] == "Y") ? "Y" : "N" ?>"/>
<input type="hidden" id="use_prepayment" value="<?= ($arParams["USE_PREPAYMENT"] == "Y") ? "Y" : "N" ?>"/>


<!-- RECOMMENDED_CART -->

<?php

if($_SERVER['REMOTE_ADDR'] == '37.195.203.64') {
//	$APPLICATION->IncludeComponent(
//		"bitrix:sale.gift.basket",
//		".default",
//		array(
//			"SHOW_PRICE_COUNT" => 1,
//			"PRODUCT_SUBSCRIPTION" => 'Y',
//			'PRODUCT_ID_VARIABLE' => 'id',
//			"PARTIAL_PRODUCT_PROPERTIES" => 'N',
//			"USE_PRODUCT_QUANTITY" => 'N',
//			"ACTION_VARIABLE" => "actionGift",
//			"ADD_PROPERTIES_TO_BASKET" => "Y",
//
//			"BASKET_URL" => $APPLICATION->GetCurPage(),
//			"APPLIED_DISCOUNT_LIST" => $arResult["APPLIED_DISCOUNT_LIST"],
//			"FULL_DISCOUNT_LIST" => $arResult["FULL_DISCOUNT_LIST"],
//
//			"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_SHOW_VALUE"],
//
//			'BLOCK_TITLE' => $arParams['GIFTS_BLOCK_TITLE'],
//			'HIDE_BLOCK_TITLE' => $arParams['GIFTS_HIDE_BLOCK_TITLE'],
//			'TEXT_LABEL_GIFT' => $arParams['GIFTS_TEXT_LABEL_GIFT'],
//			'PRODUCT_QUANTITY_VARIABLE' => $arParams['GIFTS_PRODUCT_QUANTITY_VARIABLE'],
//			'PRODUCT_PROPS_VARIABLE' => $arParams['GIFTS_PRODUCT_PROPS_VARIABLE'],
//			'SHOW_OLD_PRICE' => $arParams['GIFTS_SHOW_OLD_PRICE'],
//			'SHOW_DISCOUNT_PERCENT' => $arParams['GIFTS_SHOW_DISCOUNT_PERCENT'],
//			'SHOW_NAME' => $arParams['GIFTS_SHOW_NAME'],
//			'SHOW_IMAGE' => $arParams['GIFTS_SHOW_IMAGE'],
//			'MESS_BTN_BUY' => $arParams['GIFTS_MESS_BTN_BUY'],
//			'MESS_BTN_DETAIL' => $arParams['GIFTS_MESS_BTN_DETAIL'],
//			'PAGE_ELEMENT_COUNT' => $arParams['GIFTS_PAGE_ELEMENT_COUNT'],
//			'CONVERT_CURRENCY' => $arParams['GIFTS_CONVERT_CURRENCY'],
//			'HIDE_NOT_AVAILABLE' => $arParams['GIFTS_HIDE_NOT_AVAILABLE'],
//
//			"LINE_ELEMENT_COUNT" => $arParams['GIFTS_PAGE_ELEMENT_COUNT'],
//		),
//		false
//	);
}

/*
 *
 *
 *
$arParams['GIFTS_BLOCK_TITLE'] = isset($arParams['GIFTS_BLOCK_TITLE']) ? trim((string)$arParams['GIFTS_BLOCK_TITLE']) : Loc::getMessage('SBB_GIFTS_BLOCK_TITLE');

CBitrixComponent::includeComponentClass('bitrix:sale.products.gift.basket');

$giftParameters = array(
    'SHOW_PRICE_COUNT' => 1,
    'PRODUCT_SUBSCRIPTION' => 'N',
    'PRODUCT_ID_VARIABLE' => 'id',
    'USE_PRODUCT_QUANTITY' => 'N',
    'ACTION_VARIABLE' => 'actionGift',
    'ADD_PROPERTIES_TO_BASKET' => 'Y',
    'PARTIAL_PRODUCT_PROPERTIES' => 'Y',

    'BASKET_URL' => $APPLICATION->GetCurPage(),
    'APPLIED_DISCOUNT_LIST' => $arResult['APPLIED_DISCOUNT_LIST'],
    'FULL_DISCOUNT_LIST' => $arResult['FULL_DISCOUNT_LIST'],

    'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
    'PRICE_VAT_INCLUDE' => $arParams['PRICE_VAT_SHOW_VALUE'],
    'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],

    'BLOCK_TITLE' => $arParams['GIFTS_BLOCK_TITLE'],
    'HIDE_BLOCK_TITLE' => $arParams['GIFTS_HIDE_BLOCK_TITLE'],
    'TEXT_LABEL_GIFT' => $arParams['GIFTS_TEXT_LABEL_GIFT'],

    'DETAIL_URL' => isset($arParams['GIFTS_DETAIL_URL']) ? $arParams['GIFTS_DETAIL_URL'] : null,
    'PRODUCT_QUANTITY_VARIABLE' => $arParams['GIFTS_PRODUCT_QUANTITY_VARIABLE'],
    'PRODUCT_PROPS_VARIABLE' => $arParams['GIFTS_PRODUCT_PROPS_VARIABLE'],
    'SHOW_OLD_PRICE' => $arParams['GIFTS_SHOW_OLD_PRICE'],
    'SHOW_DISCOUNT_PERCENT' => $arParams['GIFTS_SHOW_DISCOUNT_PERCENT'],
    'DISCOUNT_PERCENT_POSITION' => $arParams['DISCOUNT_PERCENT_POSITION'],
    'MESS_BTN_BUY' => $arParams['GIFTS_MESS_BTN_BUY'],
    'MESS_BTN_DETAIL' => $arParams['GIFTS_MESS_BTN_DETAIL'],
    'CONVERT_CURRENCY' => $arParams['GIFTS_CONVERT_CURRENCY'],
    'HIDE_NOT_AVAILABLE' => $arParams['GIFTS_HIDE_NOT_AVAILABLE'],

    'PRODUCT_ROW_VARIANTS' => '',
    'PAGE_ELEMENT_COUNT' => 0,
    'DEFERRED_PRODUCT_ROW_VARIANTS' => \Bitrix\Main\Web\Json::encode(
        SaleProductsGiftBasketComponent::predictRowVariants(
            $arParams['GIFTS_PAGE_ELEMENT_COUNT'],
            $arParams['GIFTS_PAGE_ELEMENT_COUNT']
        )
    ),
    'DEFERRED_PAGE_ELEMENT_COUNT' => $arParams['GIFTS_PAGE_ELEMENT_COUNT'],

    'ADD_TO_BASKET_ACTION' => 'BUY',
    'PRODUCT_DISPLAY_MODE' => 'Y',
    'PRODUCT_BLOCKS_ORDER' => isset($arParams['GIFTS_PRODUCT_BLOCKS_ORDER']) ? $arParams['GIFTS_PRODUCT_BLOCKS_ORDER'] : '',
    'SHOW_SLIDER' => isset($arParams['GIFTS_SHOW_SLIDER']) ? $arParams['GIFTS_SHOW_SLIDER'] : '',
    'SLIDER_INTERVAL' => isset($arParams['GIFTS_SLIDER_INTERVAL']) ? $arParams['GIFTS_SLIDER_INTERVAL'] : '',
    'SLIDER_PROGRESS' => isset($arParams['GIFTS_SLIDER_PROGRESS']) ? $arParams['GIFTS_SLIDER_PROGRESS'] : '',
    'LABEL_PROP_POSITION' => $arParams['LABEL_PROP_POSITION'],

    'USE_ENHANCED_ECOMMERCE' => $arParams['USE_ENHANCED_ECOMMERCE'],
    'DATA_LAYER_NAME' => $arParams['DATA_LAYER_NAME'],
    'BRAND_PROPERTY' => $arParams['BRAND_PROPERTY']
);

$APPLICATION->IncludeComponent(
	'bitrix:sale.products.gift.basket',
	'csolution',
	$giftParameters,
	$component
);*/


?>


