
function couponCreate(couponBlock, oneCoupon)
{
    var couponClass = 'disabled';

    if (!BX.type.isElementNode(couponBlock))
        return;
    if (oneCoupon.JS_STATUS === 'BAD')
        couponClass = 'bad';
    else if (oneCoupon.JS_STATUS === 'APPLYED')
        couponClass = 'good';

    couponBlock.appendChild(BX.create(
        'div',
        {
            props: {
                className: 'bx_ordercart_coupon'
            },
            children: [
                BX.create(
                    'input',
                    {
                        props: {
                            className: couponClass,
                            type: 'text',
                            value: oneCoupon.COUPON,
                            name: 'OLD_COUPON[]'
                        },
                        attrs: {
                            disabled: true,
                            readonly: true
                        }
                    }
                ),
                BX.create(
                    'span',
                    {
                        props: {
                            className: couponClass
                        },
                        attrs: {
                            'data-coupon': oneCoupon.COUPON
                        }
                    }
                ),
                BX.create(
                    'div',
                    {
                        props: {
                            className: 'bx_ordercart_coupon_notes'
                        },
                        html: oneCoupon.JS_CHECK_CODE
                    }
                )
            ]
        }
    ));
}

/**
 * @param {COUPON_LIST : []} res
 */
function couponListUpdate(res)
{
    var couponBlock,
        couponClass,
        fieldCoupon,
        couponsCollection,
        couponFound,
        i,
        j,
        key;

    if (!!res && typeof res !== 'object')
    {
        return;
    }

    couponBlock = BX('coupons_block');
    if (!!couponBlock)
    {
        if (!!res.COUPON_LIST && BX.type.isArray(res.COUPON_LIST))
        {
            fieldCoupon = BX('coupon');
            if (!!fieldCoupon)
            {
                fieldCoupon.value = '';
            }
            couponsCollection = BX.findChildren(couponBlock, { tagName: 'input', property: { name: 'OLD_COUPON[]' } }, true);

            if (!!couponsCollection)
            {
                if (BX.type.isElementNode(couponsCollection))
                {
                    couponsCollection = [couponsCollection];
                }
                for (i = 0; i < res.COUPON_LIST.length; i++)
                {
                    couponFound = false;
                    key = -1;
                    for (j = 0; j < couponsCollection.length; j++)
                    {
                        if (couponsCollection[j].value === res.COUPON_LIST[i].COUPON)
                        {
                            couponFound = true;
                            key = j;
                            couponsCollection[j].couponUpdate = true;
                            break;
                        }
                    }
                    if (couponFound)
                    {
                        couponClass = 'disabled';
                        if (res.COUPON_LIST[i].JS_STATUS === 'BAD')
                            couponClass = 'bad';
                        else if (res.COUPON_LIST[i].JS_STATUS === 'APPLYED')
                            couponClass = 'good';

                        BX.adjust(couponsCollection[key], {props: {className: couponClass}});
                        BX.adjust(couponsCollection[key].nextSibling, {props: {className: couponClass}});
                        BX.adjust(couponsCollection[key].nextSibling.nextSibling, {html: res.COUPON_LIST[i].JS_CHECK_CODE});
                    }
                    else
                    {
                        couponCreate(couponBlock, res.COUPON_LIST[i]);
                    }
                }
                for (j = 0; j < couponsCollection.length; j++)
                {
                    if (typeof (couponsCollection[j].couponUpdate) === 'undefined' || !couponsCollection[j].couponUpdate)
                    {
                        BX.remove(couponsCollection[j].parentNode);
                        couponsCollection[j] = null;
                    }
                    else
                    {
                        couponsCollection[j].couponUpdate = null;
                    }
                }
            }
            else
            {
                for (i = 0; i < res.COUPON_LIST.length; i++)
                {
                    couponCreate(couponBlock, res.COUPON_LIST[i]);
                }
            }
        }
    }
    couponBlock = null;
}

function skuPropClickHandler(e)
{
    if (!e)
    {
        e = window.event;
    }
    var target = BX.proxy_context,
        basketItemId,
        property,
        property_values = {},
        postData = {},
        action_var,
        all_sku_props,
        i,
        sku_prop_value,
        m;

    if (!!target && target.hasAttribute('data-value-id'))
    {
        BX.showWait();

        basketItemId = target.getAttribute('data-element');
        property = target.getAttribute('data-property');
        action_var = BX('action_var').value;

        property_values[property] = target.getAttribute('data-value-id');

        // if already selected element is clicked
        if (BX.hasClass(target, 'bx_active'))
        {
            BX.closeWait();
            return;
        }

        // get other basket item props to get full unique set of props of the new product
        all_sku_props = BX.findChildren(BX(basketItemId), {tagName: 'ul', className: 'sku_prop_list'}, true);
        if (!!all_sku_props && all_sku_props.length > 0)
        {
            for (i = 0; all_sku_props.length > i; i++)
            {
                if (all_sku_props[i].id !== 'prop_' + property + '_' + basketItemId)
                {
                    sku_prop_value = BX.findChildren(BX(all_sku_props[i].id), {tagName: 'li', className: 'bx_active'}, true);
                    if (!!sku_prop_value && sku_prop_value.length > 0)
                    {
                        for (m = 0; sku_prop_value.length > m; m++)
                        {
                            if (sku_prop_value[m].hasAttribute('data-value-id'))
                            {
                                property_values[sku_prop_value[m].getAttribute('data-property')] = sku_prop_value[m].getAttribute('data-value-id');
                            }
                        }
                    }
                }
            }
        }

        postData = {
            'basketItemId': basketItemId,
            'sessid': BX.bitrix_sessid(),
            'site_id': BX.message('SITE_ID'),
            'props': property_values,
            'action_var': action_var,
            'select_props': BX('column_headers').value,
            'offers_props': BX('offers_props').value,
            'quantity_float': BX('quantity_float').value,
            'count_discount_4_all_quantity': BX('count_discount_4_all_quantity').value,
            'price_vat_show_value': BX('price_vat_show_value').value,
            'hide_coupon': BX('hide_coupon').value,
            'use_prepayment': BX('use_prepayment').value
        };

        postData[action_var] = 'select_item';

        BX.ajax({
            url: '/cart/ajax.php',
            method: 'POST',
            data: postData,
            dataType: 'json',
            onsuccess: function(result)
            {
                BX.closeWait();
                updateBasketTable(basketItemId, result);
            }
        });
    }
}

function getColumnName(result, columnCode)
{
    if (BX('col_' + columnCode))
    {
        return BX.util.trim(BX('col_' + columnCode).innerHTML);
    }
    else
    {
        return '';
    }
}

function leftScroll(prop, id, count)
{
    count = parseInt(count, 10);
    var el = BX('prop_' + prop + '_' + id);

    if (el)
    {
        var curVal = parseInt(el.style.marginLeft, 10);
        if (curVal <= (6 - count)*20)
            el.style.marginLeft = curVal + 20 + '%';
    }
}

function rightScroll(prop, id, count)
{
    count = parseInt(count, 10);
    var el = BX('prop_' + prop + '_' + id);

    if (el)
    {
        var curVal = parseInt(el.style.marginLeft, 10);
        if (curVal > (5 - count)*20)
            el.style.marginLeft = curVal - 20 + '%';
    }
}

function checkOut()
{
    if (!!BX('coupon'))
        BX('coupon').disabled = true;
    BX("basket_form").submit();
    return true;
}

function removeItem(id)
{
    //if(confirm('Вы уверены ?')) {
        var action = 'DELETE_' + id;
        var obj = {};
        obj[action] = 'Y'
		obj['basketID'] = id;
        obj['basketAction'] = 'DELETE';

        recalcBasketAjax(obj);
        $('#' + id).fadeOut('normal', function(){
            $(this).remove();
        });
    //}

}

function enterCoupon()
{

    console.log('COUPON');

    var newCoupon = BX('coupon');
    if (!!newCoupon)
        recalcBasketAjax({'coupon' : newCoupon.value, 'basketAction' : 'COUPON'});
}

// check if quantity is valid
// and update values of both controls (text input field for PC and mobile quantity select) simultaneously
function updateQuantity(controlId, basketId, ratio, bUseFloatQuantity)
{
    var oldVal = BX(controlId).defaultValue,
        newVal = parseFloat(BX(controlId).value) || 0,
        bIsCorrectQuantityForRatio = false;

    if (ratio === 0 || ratio == 1)
    {
        bIsCorrectQuantityForRatio = true;
    }
    else
    {

        var newValInt = newVal * 10000,
            ratioInt = ratio * 10000,
            reminder = newValInt % ratioInt,
            newValRound = parseInt(newVal);

        if (reminder === 0)
        {
            bIsCorrectQuantityForRatio = true;
        }
    }

    var bIsQuantityFloat = false;

    if (parseInt(newVal) != parseFloat(newVal))
    {
        bIsQuantityFloat = true;
    }

    newVal = (bUseFloatQuantity === false && bIsQuantityFloat === false) ? parseInt(newVal) : parseFloat(newVal).toFixed(2);

    if (bIsCorrectQuantityForRatio)
    {
        BX(controlId).defaultValue = newVal;

        BX("QUANTITY_INPUT_" + basketId).value = newVal;

        // set hidden real quantity value (will be used in actual calculation)
        BX("QUANTITY_" + basketId).value = newVal;

        recalcBasketAjax({'basketID': basketId, 'value': newVal});
    }
    else
    {
        newVal = getCorrectRatioQuantity(newVal, ratio, bUseFloatQuantity);

        if (newVal != oldVal)
        {
            BX("QUANTITY_INPUT_" + basketId).value = newVal;
            BX("QUANTITY_" + basketId).value = newVal;
            recalcBasketAjax({'basketID': basketId, 'value': newVal});
        }else
        {
            BX(controlId).value = oldVal;
        }
    }
}

// used when quantity is changed by clicking on arrows
function setQuantity(basketId, ratio, sign, bUseFloatQuantity)
{
    var curVal = parseFloat(BX("QUANTITY_INPUT_" + basketId).value),
        newVal;

    newVal = (sign == 'up') ? curVal + ratio : curVal - ratio;

    if (newVal < 0)
        newVal = 0;

    if (bUseFloatQuantity)
    {
        newVal = newVal.toFixed(2);
    }

    if (ratio > 0 && newVal < ratio)
    {
        newVal = ratio;
    }

    if (!bUseFloatQuantity && newVal != newVal.toFixed(2))
    {
        newVal = newVal.toFixed(2);
    }

    newVal = getCorrectRatioQuantity(newVal, ratio, bUseFloatQuantity);

    BX("QUANTITY_INPUT_" + basketId).value = newVal;
    BX("QUANTITY_INPUT_" + basketId).defaultValue = newVal;

    updateQuantity('QUANTITY_INPUT_' + basketId, basketId, ratio, bUseFloatQuantity);
}

function getCorrectRatioQuantity(quantity, ratio, bUseFloatQuantity)
{
    var newValInt = quantity * 10000,
        ratioInt = ratio * 10000,
        reminder = newValInt % ratioInt,
        result = quantity,
        bIsQuantityFloat = false,
        i;
    ratio = parseFloat(ratio);

    if (reminder === 0)
    {
        return result;
    }

    if (ratio !== 0 && ratio != 1)
    {
        for (i = ratio, max = parseFloat(quantity) + parseFloat(ratio); i <= max; i = parseFloat(parseFloat(i) + parseFloat(ratio)).toFixed(2))
        {
            result = i;
        }

    }else if (ratio === 1)
    {
        result = quantity | 0;
    }

    if (parseInt(result, 10) != parseFloat(result))
    {
        bIsQuantityFloat = true;
    }

    result = (bUseFloatQuantity === false && bIsQuantityFloat === false) ? parseInt(result, 10) : parseFloat(result).toFixed(2);

    return result;
}
/**
 *
 * @param {} params
 */
function recalcBasketAjax(params)
{
    BX.showWait();

    var property_values = {},
        action_var = BX('action_var').value,
        items = BX('basket_items'),
        delayedItems = BX('delayed_items'),
        postData,
        i;

    /*postData = {
        'sessid': BX.bitrix_sessid(),
        'site_id': BX.message('SITE_ID'),
        'basketId': params.baskedID,
		'quanty': params.value
    };*/
    postData = {
        'sessid': BX.bitrix_sessid(),
        'site_id': BX.message('SITE_ID'),
        'props': property_values,
        'action_var': action_var,
        'select_props': BX('column_headers').value,
        'offers_props': BX('offers_props').value,
        'quantity_float': BX('quantity_float').value,
        'price_vat_show_value': BX('price_vat_show_value').value,
        'hide_coupon': BX('hide_coupon').value,
        'use_prepayment': BX('use_prepayment').value
    };
    postData[action_var] = 'recalculate';
    /*if (!!params && typeof params === 'object')
    {
        for (i in params)
        {
            if (params.hasOwnProperty(i))
                postData[i] = params[i];
        }
    }*/

    if (!!params && typeof params === 'object')
    {
        for (i in params)
        {
            if (params.hasOwnProperty(i))
                postData[i] = params[i];
        }
    }

    $('.cart__item').each(function () {
        let element = $(this).find('.input-hidden');
        let name = element.attr('name');
        postData[element.attr('name')] = element.val();
    });



    BX.ajax({
        url: '/bitrix/components/bitrix/sale.basket.basket/ajax.php',
        method: 'POST',
        data: postData,
        dataType: 'json',
        onsuccess: function(result)
        {
            BX.closeWait();


            $('.cart__amount-price').html((parseInt(result.BASKET_DATA.allSum).toLocaleString('ru')) + '₽');

            //console.log($('body').find('.cart__amount-total--old').length);



            if($('input[name="promocode"]').val() != '') {


                if ($('body').find('.cart__amount-total--old').length == 0) {

                    $('.cart__amount-price').before(
                        '<span class="cart__amount-total--old">'
                        +
                        parseInt(result.BASKET_DATA.allSum + result.BASKET_DATA.DISCOUNT_PRICE_ALL).toLocaleString('ru') + '₽'
                        +
                        '</span>'
                    );
                }
                $('.input-promocode').attr('disabled', 'disabled');
            }



            for (id in result.BASKET_DATA.GRID.ROWS){
                let item = result.BASKET_DATA.GRID.ROWS[id];
                let element = $('#PRODUCT_PRICE_' + id);
                element.html(item.SUM_VALUE.toLocaleString('ru') + '₽');

                if($('input[name="promocode"]').val() != '') {

                    if (element.closest('.cart__item-price').find('.cart__item-old-price').length != 0) {
                        $('#PRODUCT_DISCOUNT_PRICE_' + id).html(item.SUM_FULL_PRICE.toLocaleString('ru') + '₽');
                    } else {
                        element.before(
                            '<span class="cart__item-old-price" id="PRODUCT_DISCOUNT_PRICE_' + id + '">' + item.SUM_FULL_PRICE.toLocaleString('ru') + '₽</span>'
                        );
                    }
                }
            }
            if($('body').find('.cart__amount-total--old').length == 1)
            $('body').find('.cart__amount-total--old').html(parseInt(result.BASKET_DATA.allSum + result.BASKET_DATA.DISCOUNT_PRICE_ALL).toLocaleString('ru') + '₽');
        }
    });
}

function deleteCoupon(e)
{
    var target = BX.proxy_context,
        value;

    if (!!target && target.hasAttribute('data-coupon'))
    {
        value = target.getAttribute('data-coupon');
        if (!!value && value.length > 0)
        {
            recalcBasketAjax({'delete_coupon' : value});
        }
    }
}

BX.ready(function() {
    var sku_props = BX.findChildren(BX('basket_items'), {tagName: 'li', className: 'sku_prop'}, true),
        i,
        couponBlock;
    if (!!sku_props && sku_props.length > 0)
    {
        for (i = 0; sku_props.length > i; i++)
        {
            BX.bind(sku_props[i], 'click', BX.delegate(function(e){ skuPropClickHandler(e);}, this));
        }
    }
    couponBlock = BX('coupons_block');
    if (!!couponBlock)
        BX.bindDelegate(couponBlock, 'click', { 'attribute': 'data-coupon' }, BX.delegate(function(e){deleteCoupon(e); }, this));
});
