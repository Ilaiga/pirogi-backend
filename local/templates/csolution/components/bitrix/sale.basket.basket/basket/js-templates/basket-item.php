<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

$usePriceInAdditionalColumn = in_array('PRICE', $arParams['COLUMNS_LIST']) && $arParams['PRICE_DISPLAY_MODE'] === 'Y';
$useSumColumn = in_array('SUM', $arParams['COLUMNS_LIST']);
$useActionColumn = in_array('DELETE', $arParams['COLUMNS_LIST']);

$restoreColSpan = 2 + $usePriceInAdditionalColumn + $useSumColumn + $useActionColumn;

?>

<script id="basket-item-template" type="text/html">
    <li class="cart__item row">
        <div class="cart__item-inner col-12 col-sm-3">
            <h3 class="cart__item-title">{{NAME}}</h3>
            <div class="cart__item-weight-wrap">
                <span class="cart__item-weight">1400 г</span>
            </div>
            <p class="cart__item-price cart__item-price--mobile">
                <span class="js-item-price">
                    {{{PRICE_FORMATED}}}
                </span>
            </p>
        </div>
        <div class="cart__item-img col-12 col-sm-3">
            <img src="{{{IMAGE_URL}}}{{^IMAGE_URL}}<?=$templateFolder?>/images/no_photo.png{{/IMAGE_URL}}" alt=""/>
        </div>
        <div class="cart__counter-wrap">


            <button class="js-counter-minus cart__counter-toggle" data-entity="basket-item-quantity-minus">-</button>

            <input type="text" class="js-counter-count cart__counter-count basket-item-amount-filed" value="{{QUANTITY}}"
                   {{#NOT_AVAILABLE}} disabled="disabled"{{/NOT_AVAILABLE}}
            data-value="{{QUANTITY}}" data-entity="basket-item-quantity-field"
            id="basket-item-quantity-{{ID}}">



            <button class="js-counter-plus cart__counter-toggle" data-entity="basket-item-quantity-plus">+</button>
        </div>
        <div class="cart__item-price cart__item-price--tablet col-sm-2">
            <span class="js-item-price">{{{SUM_PRICE_FORMATED}}}</span>
        </div>
        <button class="cart__remove-toggle" data-entity="basket-item-delete">
            <svg class="icon-svg">
                <use xlink:href="#icon-cross"></use>
            </svg>
        </button>
    </li>
</script>