

<section class="slider owl-carousel owl-theme">
	<? foreach ($arResult['ITEMS'] as $ITEM):

		    if($ITEM['PROPERTIES']['CITY']['VALUE'] != $_SESSION['GET_CITY']['ID']['VALUE']) continue;
        ?>



        <div class="slider__item _bg" data-type = "<?=$ITEM['PROPERTIES']['TYPE_SLIDER']['VALUE_XML_ID']?>">

			<? if ($video = $ITEM['PROPERTIES']['VIDEO']['VALUE']): ?>
                <iframe data-src="<?= $video ?>"
                        src="<?= $video ?>" frameborder="0"
                        allow="autoplay; encrypted-media" width="100%" height="100%" allowfullscreen></iframe>
			<? elseif($ITEM['PROPERTIES']['TYPE_SLIDER']['VALUE_XML_ID'] == 'A'): ?>
                <a class="slider__item-link" href="<?=$ITEM['PROPERTIES']['LINK']['VALUE'] ?: 'javascript:;'?>">
                    <picture>
                        <source media="(min-width: 768px)" srcset="<?= $ITEM['PREVIEW_PICTURE']['SRC'] ?>"/>
                        <img src="<?=CFile::GetPath($ITEM['PROPERTIES']['IMAGE_MOBILE']['VALUE']) ?: $ITEM['PREVIEW_PICTURE']['SRC']?>"/>
                    </picture>
                </a>
            <? else: ?>
                <div class="slider__item-img">
                    <picture>
                        <source media="(min-width: 768px)" srcset="<?= $ITEM['PREVIEW_PICTURE']['SRC'] ?>"/>
                        <img src="<?=CFile::GetPath($ITEM['PROPERTIES']['IMAGE_MOBILE']['VALUE']) ?: $ITEM['PREVIEW_PICTURE']['SRC']?>"/>
                    </picture>
                </div>
                <?if($ITEM['PROPERTIES']['TITLE']['VALUE']): ?>
                    <div class="container">
                        <div class="slider__item-inner">
                            <? if ($name = $ITEM['PROPERTIES']['TITLE']['VALUE']): ?>
                                <h1 class="slider__item-title"><?= $ITEM['PROPERTIES']['TITLE']['VALUE'] ?></h1>
                            <? endif; ?>
                            <p class="slider__item-desc"><?= $ITEM['PREVIEW_TEXT'] ?></p>
                            <? if ($ITEM['PROPERTIES']['LINK']['VALUE']): ?>
                                <a class="slider__item-order-btn btn order-btn" href="<?= $ITEM['PROPERTIES']['LINK']['VALUE'] ?>">
                                    <?= $ITEM['PROPERTIES']['BUTTON_TEXT']['VALUE'] ? $ITEM['PROPERTIES']['BUTTON_TEXT']['VALUE'] : 'Заказать' ?>
                                    <svg class="icon-svg">
                                        <use xlink:href="#icon-pie"></use>
                                    </svg>
                                </a>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endif; ?>

			<? endif; ?>
        </div>
	<? endforeach; ?>
</section>