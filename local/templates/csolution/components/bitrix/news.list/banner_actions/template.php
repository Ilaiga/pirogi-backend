<section class="sales">

    <?//=debug($arResult['ITEMS'])?>
    <div class="container"><h2 class="sales__title"><?=$arParams['PAGER_TITLE']?></h2>
        <div class="sales__slider owl-carousel owl-theme">
			<?foreach($arResult['ITEMS'] as $KEY => $ITEM):
                if($ITEM['PROPERTIES']['CITY']['VALUE'] != $_SESSION['GET_CITY']['ID']['VALUE']) continue;
                ?>
            <div class="sales__item">
                <picture>
                    <source media="(min-width: 768px)" srcset="<?=CFile::getPath($ITEM['PROPERTIES']['IMAGES']['VALUE'])?>"/>
                    <img src="<?=CFile::getPath($ITEM['PROPERTIES']['IMAGES_MOBILE']['VALUE'])?>" alt=""/>
                </picture>
            </div>
			<?endforeach;?>
        </div>
    </div>
</section>