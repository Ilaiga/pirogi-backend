

<?foreach($arResult['ITEMS'] as $KEY => $ITEM):
    if($ITEM['PROPERTIES']['CITY']['VALUE'] != $_SESSION['GET_CITY']['ID']['VALUE']) continue;
?>
<!--    <div class="actions__item">-->
<!--        <picture>-->
<!--            <source media="(min-width: 768px)" srcset="--><?//=CFile::getPath($ITEM['PROPERTIES']['IMAGES']['VALUE'])?><!--"/>-->
<!--            <img src="--><?//=CFile::getPath($ITEM['PROPERTIES']['IMAGES_MOBILE']['VALUE'])?><!--" alt="акции"/>-->
<!--        </picture>-->
<!--    </div>-->

    <div class="actions__item">
        <img src="<?=CFile::getPath($ITEM['PROPERTIES']['IMAGES']['VALUE'])?>" alt=""/>
        <h3 class="actions__title"><?=$ITEM['PROPERTIES']['NAME']['VALUE']?></h3>
        <p class="actions__text"><?=$ITEM['PREVIEW_TEXT']?></p>
    </div>
<?endforeach;?>