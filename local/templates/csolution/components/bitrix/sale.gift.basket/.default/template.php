<?php
	echo debug($arResult);

?>

<script type="text/javascript">
    BX.message({
        CVP_MESS_BTN_BUY: '<? echo('' != $arParams['MESS_BTN_BUY'] ? CUtil::JSEscape($arParams['MESS_BTN_BUY']) : GetMessageJS('CVP_TPL_MESS_BTN_BUY_GIFT')); ?>',
        CVP_MESS_BTN_ADD_TO_BASKET: '<? echo('' != $arParams['MESS_BTN_ADD_TO_BASKET'] ? CUtil::JSEscape($arParams['MESS_BTN_ADD_TO_BASKET']) : GetMessageJS('CVP_TPL_MESS_BTN_ADD_TO_BASKET')); ?>',

        CVP_MESS_BTN_DETAIL: '<? echo('' != $arParams['MESS_BTN_DETAIL'] ? CUtil::JSEscape($arParams['MESS_BTN_DETAIL']) : GetMessageJS('CVP_TPL_MESS_BTN_DETAIL')); ?>',

        CVP_MESS_NOT_AVAILABLE: '<? echo('' != $arParams['MESS_BTN_DETAIL'] ? CUtil::JSEscape($arParams['MESS_BTN_DETAIL']) : GetMessageJS('CVP_TPL_MESS_BTN_DETAIL')); ?>',
        CVP_BTN_MESSAGE_BASKET_REDIRECT: '<? echo GetMessageJS('CVP_CATALOG_BTN_MESSAGE_BASKET_REDIRECT'); ?>',
        CVP_BASKET_URL: '<? echo $arParams["BASKET_URL"]; ?>',
        CVP_ADD_TO_BASKET_OK: '<? echo GetMessageJS('CVP_ADD_TO_BASKET_OK'); ?>',
        CVP_TITLE_ERROR: '<? echo GetMessageJS('CVP_CATALOG_TITLE_ERROR') ?>',
        CVP_TITLE_BASKET_PROPS: '<? echo GetMessageJS('CVP_CATALOG_TITLE_BASKET_PROPS') ?>',
        CVP_TITLE_SUCCESSFUL: '<? echo GetMessageJS('CVP_ADD_TO_BASKET_OK'); ?>',
        CVP_BASKET_UNKNOWN_ERROR: '<? echo GetMessageJS('CVP_CATALOG_BASKET_UNKNOWN_ERROR') ?>',
        CVP_BTN_MESSAGE_SEND_PROPS: '<? echo GetMessageJS('CVP_CATALOG_BTN_MESSAGE_SEND_PROPS'); ?>',
        CVP_BTN_MESSAGE_CLOSE: '<? echo GetMessageJS('CVP_CATALOG_BTN_MESSAGE_CLOSE') ?>'
    });
</script>

<?if(count($arResult['ITEMS'])):?>
	<div class="col-sm-8" id="gifts_items_block">
		<div class="b-cart" id="basket_gifts">
			<div class="b-cart__title">
				<? if (empty($arParams['HIDE_BLOCK_TITLE']) || $arParams['HIDE_BLOCK_TITLE'] !== 'Y') {
					echo $arParams['BLOCK_TITLE'] ? htmlspecialcharsbx($arParams['BLOCK_TITLE']) : GetMessage('SGB_TPL_BLOCK_TITLE_DEFAULT');
				}
				?>
			</div>
			<div class="b-cart__products">
				<?
				$elementEdit = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT');
				$elementDelete = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE');
				$elementDeleteParams = array('CONFIRM' => GetMessage('CVP_TPL_ELEMENT_DELETE_CONFIRM'));
				foreach ($arResult['ITEMS'] as $key => $arItem):
					$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $elementEdit);
					$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $elementDelete, $elementDeleteParams);
					$strMainID = $this->GetEditAreaId($arItem['ID'] . $key);

					$arItemIDs = array(
						'ID' => $strMainID,
						'PICT' => $strMainID . '_pict',
						'SECOND_PICT' => $strMainID . '_secondpict',
						'MAIN_PROPS' => $strMainID . '_main_props',

						'QUANTITY' => $strMainID . '_quantity',
						'QUANTITY_DOWN' => $strMainID . '_quant_down',
						'QUANTITY_UP' => $strMainID . '_quant_up',
						'QUANTITY_MEASURE' => $strMainID . '_quant_measure',
						'BUY_LINK' => $strMainID . '_buy_link',
						'SUBSCRIBE_LINK' => $strMainID . '_subscribe',

						'PRICE' => $strMainID . '_price',
						'DSC_PERC' => $strMainID . '_dsc_perc',
						'SECOND_DSC_PERC' => $strMainID . '_second_dsc_perc',

						'PROP_DIV' => $strMainID . '_sku_tree',
						'PROP' => $strMainID . '_prop_',
						'DISPLAY_PROP_DIV' => $strMainID . '_sku_prop',
						'BASKET_PROP_DIV' => $strMainID . '_basket_prop'
					);

					$strObName = 'ob' . preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);

					$strTitle = (
					isset($arItem["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"]) && '' != isset($arItem["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"])
						? $arItem["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"]
						: $arItem['NAME']
					);
					$showImgClass = $arParams['SHOW_IMAGE'] != "Y" ? "no-imgs" : "";


					$arJSParams = array(
						'PRODUCT_TYPE' => $arItem['CATALOG_TYPE'],
						'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
						'SHOW_ADD_BASKET_BTN' => false,
						'SHOW_BUY_BTN' => true,
						'SHOW_ABSENT' => true,
						'PRODUCT' => array(
							'ID' => $arItem['ID'],
							'NAME' => $arItem['~NAME'],
							'PICT' => ('Y' == $arItem['SECOND_PICT'] ? $arItem['PREVIEW_PICTURE_SECOND'] : $arItem['PREVIEW_PICTURE']),
							'CAN_BUY' => $arItem["CAN_BUY"],
							'SUBSCRIPTION' => ('Y' == $arItem['CATALOG_SUBSCRIPTION']),
							'CHECK_QUANTITY' => $arItem['CHECK_QUANTITY'],
							'MAX_QUANTITY' => $arItem['CATALOG_QUANTITY'],
							'STEP_QUANTITY' => $arItem['CATALOG_MEASURE_RATIO'],
							'QUANTITY_FLOAT' => is_double($arItem['CATALOG_MEASURE_RATIO']),
							'ADD_URL' => $arItem['~ADD_URL'],
							'SUBSCRIBE_URL' => $arItem['~SUBSCRIBE_URL']
						),
						'BASKET' => array(
							'ADD_PROPS' => ('Y' == $arParams['ADD_PROPERTIES_TO_BASKET']),
							'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
							'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
							'EMPTY_PROPS' => $emptyProductProperties
						),
						'VISUAL' => array(
							'ID' => $arItemIDs['ID'],
							'PICT_ID' => ('Y' == $arItem['SECOND_PICT'] ? $arItemIDs['SECOND_PICT'] : $arItemIDs['PICT']),
							'QUANTITY_ID' => $arItemIDs['QUANTITY'],
							'QUANTITY_UP_ID' => $arItemIDs['QUANTITY_UP'],
							'QUANTITY_DOWN_ID' => $arItemIDs['QUANTITY_DOWN'],
							'PRICE_ID' => $arItemIDs['PRICE'],
							'BUY_ID' => $arItemIDs['BUY_LINK'],
							'BASKET_PROP_DIV' => $arItemIDs['BASKET_PROP_DIV']
						),
						'LAST_ELEMENT' => $arItem['LAST_ELEMENT']
					);
					?>
					<script type="text/javascript">
                        console.log('work');
                        var <? echo $strObName; ?> = new JCSaleGiftBasket(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
					</script>

					<div class="cart-product">
						<div class="cart-product__preview">
							<a href="<? echo $arItem['DETAIL_PAGE_URL']; ?>" class="cart-product__preview-inner">
								<img class="cart-product__preview-img"
									 src="<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>">
							</a>
						</div>
						<div class="cart-product__title g-va">
							<a href="<? echo $arItem['DETAIL_PAGE_URL']; ?>">
								<div class="cart-product__title-text"><? echo $arItem['NAME']; ?></div>
							</a>
						</div>
						<a href="<?= $arItem['~ADD_URL']?>" class="cart-product_gift_select bx_bt_button bx_medium">Выбрать</a>
					</div>
				<? endforeach; ?>
			</div>
		</div>
	</div>
<?endif;?>