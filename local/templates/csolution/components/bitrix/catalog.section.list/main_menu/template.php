<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);


?>

<ul class="sub-nav__list">
	<?
    //
	$SECTIONS = array_values($_SESSION['GET_CITY']['SECTIONS']['VALUE']);
	foreach ($arResult['SECTIONS'] as $SECTION):
		if (in_array($SECTION['ID'], $SECTIONS) == false) continue;
		?>


        <li class="sub-nav__item">
            <a class="sub-nav__link <?=$_SERVER['REQUEST_URI'] == $SECTION['SECTION_PAGE_URL'] ? '_active' : ''?>" href="<?= $SECTION['SECTION_PAGE_URL'] ?>"><?= strtolower($SECTION['NAME']) ?></a>
        </li>

	<? endforeach; ?>
</ul>

