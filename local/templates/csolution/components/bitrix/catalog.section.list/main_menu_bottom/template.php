<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

?>

<nav class="footer-nav__wrap">
    <ul class="footer-nav__list">
        <?
        $SECTIONS = array_values($_SESSION['GET_CITY']['SECTIONS']['VALUE']);
        foreach ($arResult['SECTIONS'] as $SECTION):
            if (in_array($SECTION['ID'], $SECTIONS) == false) continue;
            ?>


            <li class="footer-nav__item footer-nav__item ">
                <a class="footer-nav__link footer-nav__link link <?=$_SERVER['REQUEST_URI'] == $SECTION['SECTION_PAGE_URL'] ? '_active' : ''?>"
                                         href="<?= $SECTION['SECTION_PAGE_URL'] ?>"><?=$SECTION['NAME'] ?></a>
            </li>

        <? endforeach; ?>
    </ul>
</nav>

