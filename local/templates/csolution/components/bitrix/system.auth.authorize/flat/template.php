<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponent $component
 */

if($_SERVER["REQUEST_METHOD"] == "POST") {

	ShowMessage($arParams["~AUTH_RESULT"]);
	ShowMessage($arResult['ERROR_MESSAGE']);
}

//one css for all system.auth.* forms
?>

<div class="bx-authform">



    <section class="login">
        <h1 class="title title__form">Вход</h1>
        <div class="container container--helper">
            <h1 class="title title__form"><?=GetMessage("AUTH_PLEASE_AUTH")?></h1>
            <form name="form_auth" class="form" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">

                <input type="hidden" name="AUTH_FORM" value="Y" />
                <input type="hidden" name="TYPE" value="AUTH" />
				<?if (strlen($arResult["BACKURL"]) > 0):?>
                    <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
				<?endif?>
				<?foreach ($arResult["POST"] as $key => $value):?>
                    <input type="hidden" name="<?=$key?>" value="<?=$value?>" />
				<?endforeach?>


                <div class="input__inner input__inner--required">
                    <input class="input" name="USER_LOGIN" value="<?=$arResult["LAST_LOGIN"]?>" placeholder="Email" required="required"/>
                </div>

                <div class="input__inner input__inner--required">

					<?if($arResult["SECURE_AUTH"]):?>
                        <div class="bx-authform-psw-protected" id="bx_auth_secure" style="display:none">
                            <div class="bx-authform-psw-protected-desc">
                                <span></span><?echo GetMessage("AUTH_SECURE_NOTE")?>
                            </div>
                        </div>
                        <script type="text/javascript">
                            document.getElementById('bx_auth_secure').style.display = '';
                        </script>
					<?endif?>

                    <input class="input" name="USER_PASSWORD" type="password" placeholder="Пароль" required="required"/>
                </div>

                <a class="link login__forgot-pass" href="/auth/forgotpassword/">Забыли пароль?</a>
                <div class="login-form__inner">
                    <button class="btn sub-btn" value="<?=GetMessage("AUTH_AUTHORIZE")?>" name="Login">Войти</button>
                    <a class="btn sub-btn sub-btn--secondary" href="<?=$arParams['PATH_TO_REGISTER']?>?register=yes">Регистрация</a>
                </div>
            </form>
        </div>
    </section>




