<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/**
 * @global CMain $APPLICATION
 */

global $APPLICATION;

//delayed function must return a string
if(empty($arResult))
	return "";

$strReturn = '';


$itemSize = count($arResult);
for($index = 0; $index < $itemSize; $index++)
{
    $title = htmlspecialcharsex($arResult[$index]["TITLE"]);


    if($arResult[$index]["LINK"] <> "" && $index != $itemSize-1) {
        $strReturn .= '
            <li class="breadcrumbs__item" itemprop="itemListElement" itemscope="itemscope" itemtype="http://schema.org/ListItem">
            <a class="breadcrumbs__link" href="'.$arResult[$index]["LINK"].'" itemprop="item">
                    <span itemprop="name">'.$title.'</span>
            </a>
        </li>';
    }
    else {
        $strReturn .= '
            <li class="breadcrumbs__item" itemprop="itemListElement" itemscope="itemscope" itemtype="http://schema.org/ListItem">
            <p class="breadcrumbs__link breadcrumbs__link--active" href="'.$arResult[$index]["LINK"].'" itemprop="item">
                    <span itemprop="name">'.$title.'</span>
            </p>
        </li>';
    }
}
return $strReturn;


