<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}
/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 */

//one css for all system.auth.* forms
?>



<section class="login">
	<div class="container container--helper">
		<h1 class="title title__form"><?=GetMessage("AUTH_CHANGE_PASSWORD")?></h1>
		<?
		if(!empty($arParams["~AUTH_RESULT"])):
			$text = str_replace(array("<br>", "<br />"), "\n", $arParams["~AUTH_RESULT"]["MESSAGE"]);
			?>
			<div class="alert <?=($arParams["~AUTH_RESULT"]["TYPE"] == "OK"? "alert-success":"alert-danger")?>"><?=nl2br(htmlspecialcharsbx($text))?></div>
		<?endif?>
		<form class="form" action="<?=$arResult["AUTH_FORM"]?>" name="bform" method="post">

			<?if (strlen($arResult["BACKURL"]) > 0): ?>
				<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
			<? endif ?>
			<input type="hidden" name="AUTH_FORM" value="Y">
			<input type="hidden" name="TYPE" value="CHANGE_PWD">


			<div class="input__inner input__inner--required">
				<input class="input" name="USER_LOGIN" type="text" placeholder="Имя" value="<?=$arResult["LAST_LOGIN"]?>" required="required"/>
			</div>
			<div class="input__inner input__inner--required">
				<input class="input" name="USER_CHECKWORD" type="text" value="<?=$arResult["USER_CHECKWORD"]?>" required="required"/>
			</div>

			<div class="input__inner input__inner--required">
				<input class="input" name="USER_PASSWORD" type="text" placeholder="Пароль" value="<?=$arResult["USER_PASSWORD"]?>" required="required"/>
			</div>

			<div class="input__inner input__inner--required">
				<input class="input" name="USER_CONFIRM_PASSWORD" type="text" placeholder="Подтверждение пароля" value="<?=$arResult["USER_CONFIRM_PASSWORD"]?>" required="required"/>
			</div>

			<button class="btn sub-btn m-auto" name="change_pwd" value="<?=GetMessage("AUTH_CHANGE")?>">Восстановить пароль</button>
		</form>
	</div>
</section>


