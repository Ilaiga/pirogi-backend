<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;

$this->setFrameMode(true);

$SECTIONS = array_values($_SESSION['GET_CITY']['SECTIONS']['VALUE']);
if(in_array($arResult['VARIABLES']['SECTION_ID'], $SECTIONS) == false) {
	LocalRedirect("/404");
}
$arResult['SECTIONS_ALLOW'] = $SECTIONS;
include($_SERVER["DOCUMENT_ROOT"]."/".$this->GetFolder()."/section_horizontal.php");
?>