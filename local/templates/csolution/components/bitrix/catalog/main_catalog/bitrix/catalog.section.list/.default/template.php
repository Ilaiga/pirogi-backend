<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);


$SECTIONS = array_values($_SESSION['GET_CITY']['SECTIONS']['VALUE']);
foreach($arResult['SECTIONS'] as $SECTION):
    if(in_array($SECTION['ID'], $SECTIONS) == false) continue;
	?>
    <li class="item catalog-intro__filter-item">
		<a class="catalog-intro__filter-link"
           href = "<?=$SECTION['SECTION_PAGE_URL']?>">
            <?=strtolower($SECTION['NAME'])?>
        </a>
	</li>
<?endforeach;?>

