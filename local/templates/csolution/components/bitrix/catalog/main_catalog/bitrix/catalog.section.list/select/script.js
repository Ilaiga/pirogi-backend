$(document).ready(function () {
    $('#filter-select').on('change', function () {
        let element = $(this).children('option:selected');
        return window.location.href = element.val();

    })
});