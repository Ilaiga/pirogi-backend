<?
//$APPLICATION->AddHeadString('<script src="https://api-maps.yandex.ru/2.1/?load=package.full&lang=ru_RU" type="text/javascript"></script>',true);
$APPLICATION->AddHeadString('<script src="https://api-maps.yandex.ru/2.1/?apikey=23c9073a-673c-477c-bf0f-6bd5b4725fbd&mode=debug&lang=ru_RU" type="text/javascript"></script>', true);
?>

<script type="text/javascript">

    ymaps.ready(init);
    function init() {


        let date = '<?=$arResult['MAP_DATA']?>';

        /*let date = JSON.stringify({
            "map": {
                "center": [55.03101101590557, 82.9317208579699],
                "zoom": 14
            },
            "type": "FeatureCollection",
            "features": [{
                "type": "Feature",
                "id": 0,
                "geometry": {
                    "type": "Polygon",
                    "coordinates": [[
                        [55.02725195707347, 82.92059851325762],
                        [55.02611790419142, 82.90635061897049],
                        [55.03375981122371, 82.90429068244708],
                        [55.03671757583662, 82.91845274604569],
                        [55.02725195707347, 82.92059851325762]
                    ]]
                },
                "options": {
                    "strokeColor": "#8080ff",
                    "fillColor": "#8080ff"
                },
                "properties": { "minDelivery": "2000", "deliveryPrice": "200" }
            }, {
                "type": "Feature",
                "id": 1,
                "geometry": {
                    "type": "Polygon",
                    "coordinates": [[
                        [55.036532379551865, 82.91991913830435],
                        [55.03761684332935, 82.93056214367544],
                        [55.026918798043276, 82.93365204846063],
                        [55.027461164295964, 82.92197907482782],
                        [55.036532379551865, 82.91991913830435]
                    ]]
                },
                "options": { "strokeColor": "#FFFFFF", "fillColor": "#ff7140" },
                "properties": {
                    "minDelivery": "5000",
                    "deliveryPrice": "500"
                }
            }]});*/

        var data = JSON.parse(date);

        // преобразуем в массив из строки date
        var suggestView = new ymaps.SuggestView('order-suggest');

        $('#order-suggest').focusout(function (e) {
            geocode();
        });

        $('#order-suggest').keypress(function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            //console.log(keycode);
            if (keycode == '13') {
                geocode();
            }
        });

        $('#order-suggest').focus(function (e) {
            let tooltip = $('#order-suggest').siblings('.error-label');
            $('#order-suggest').removeClass('error');
            tooltip.hide();
            $('.map_has_errors').attr('value', 'N');
        });

        function geocode() {

            // Забираем запрос из поля ввода.
            var request = $('#order-suggest').val();

            if (!request) {
                myMap.setCenter(data.map.center);
            }

            // Геокодируем введённые данные.
            ymaps.geocode(request).then(function (res) {

                var obj = res.geoObjects.get(0),
                    error, hint;
                // Проверим попадание результата поиска в одну из зон доставки.
                suggestView.events.add('resultshow', function (e) {
                });
                if (obj) {
                    // Об оценке точности ответа геокодера можно прочитать тут: https://tech.yandex.ru/maps/doc/geocoder/desc/reference/precision-docpage/
                    switch (obj.properties.get('metaDataProperty.GeocoderMetaData.precision')) {
                        case 'exact':
                            break;
                        case 'number':
                        case 'near':
                        case 'range':
                            error = 'Неточный адрес, требуется уточнение';
                            hint = 'Уточните номер дома';

                            break;
                        case 'street':
                            error = 'Неполный адрес, требуется уточнение';
                            hint = 'Уточните номер дома';

                            break;
                        case 'other':
                        default:
                            error = 'Неточный адрес, требуется уточнение';
                            hint = 'Уточните адрес';
                    }
                } else {
                    error = 'Адрес не найден';
                    hint = 'Уточните адрес';
                }


                // Если геокодер возвращает пустой массив или неточный результат, то показываем ошибку.
                if (error && request && error != undefined) {
                    showError(error, hint);
                }
                else {
                    let tooltip = $('#order-suggest').siblings('.error-tooltip');
                    tooltip.hide();
                    $('.map_has_errors').attr('value', 'N');

                }

                highlightResult(obj);
            }, function (e) {
                console.log(e)
            });

            function showError(error, hint) {
                let tooltip = $('#order-suggest').siblings('.error-label');
                $('#order-suggest').addClass('error');
                tooltip.text(error + ', ' + hint);
                tooltip.show();
                $('.map_has_errors').attr('value', 'Y');
            }


        }

        // Создание экземпляра карты и его привязка к контейнеру с id = angerro_map
        // var center = data.map.center;
        // var zoom = data.map.zoom;

        var myMap = new ymaps.Map('order-map-delivery_<?=$arParams["MAP_ID"]?><?=$arParams['COUNT'] ? '_'.$arParams['COUNT'] : ''?>', {
            center: data.map.center,
            zoom: data.map.zoom,
            controls: []
            //controls: ['zoomControl']
        });

        var deliveryZones = ymaps.geoQuery(data).addToMap(myMap);

        console.log(deliveryZones)


        deliveryZones.each(function (obj) {
            var color = obj.options.get('fillColor');
            // color = color.substring(0, color.length - 2);
            obj.options.set({
                fillColor: color,
                fillOpacity: 0.4
            });
            // obj.properties.set('balloonContent', obj.properties.get('name'));
            // obj.properties.set('balloonContentHeader', 'Стоимость доставки: ' + obj.properties.get('price') + ' р.')
        });


        function highlightResult(obj) {


            // Сохраняем координаты переданного объекта.
            var coords = obj.geometry.getCoordinates(),

                // Находим полигон, в который входят переданные координаты.
                polygon = deliveryZones.searchContaining(obj).get(0);

            if (polygon) {
                editPrice(polygon);

                $('input[name="CORRECTADDRESS"]').attr('value', 'Y');

                // Уменьшаем прозрачность всех полигонов, кроме того, в который входят переданные координаты.
                deliveryZones.setOptions('fillOpacity', 0.4);
                polygon.options.set('fillOpacity', 0.8);
                myMap.setCenter(coords);
                //Добавляем данные о карте для бека
                let id = polygon.options._options.id;
                let minDelivery = polygon.properties._data.minDelivery;
                let accessPrice = polygon.properties._data.accessPrice;

                //$('input[name="bitrix_delivery_map"]').val(id);
                $('input[name="bitrix_inside_area"]').val(id);
                $('input[name="bitrix_delivery_map_accessprice"]').val(accessPrice);

                // Задаем подпись для метки.
                if (typeof (obj.getThoroughfare) === 'function') {
                    setData(obj);
                } else {
                    editPrice(polygon);
                    // Если вы не хотите, чтобы при каждом перемещении метки отправлялся запрос к геокодеру,
                    // закомментируйте код ниже.
                    ymaps.geocode(coords, { results: 1 }).then(function (res) {
                        var obj = res.geoObjects.get(0);

                        setData(obj);
                    });
                }
            } else {

                $('input[name="CORRECTADDRESS"]').attr('value', 'N');

                // editPrice(polygon);
                // Если переданные координаты не попадают в полигон, то задаём стандартную прозрачность полигонов.
                deliveryZones.setOptions('fillOpacity', 0.4);
            }

            function setData(obj) {
                var address = [obj.getThoroughfare(), obj.getPremiseNumber(), obj.getPremise()].join(' ');

                if (address.trim() === '') {
                    address = obj.getAddressLine();
                }
            }

            function editPrice(obj) {
                // TODO: переписать с атрибутами

                let delivery = $('.js-amount-type-price');
                let amount = $('.js-amount-total-price');
                let amountPrice = parseInt(amount.data('price'));

                let price = obj.properties._data.deliveryPrice;
                let minPrice = obj.properties._data.minDelivery;



                //console.log('minDelivery' + minPrice);
                //console.log('delivery ' + price);

                console.log("amountPrice" + amountPrice);
                console.log("price" + price);



                if(amountPrice <= price) {
                    let deliveryPrice = +minPrice;
                    amountPrice = deliveryPrice + amountPrice;
                    $('.order__amount-text').show();
                    delivery.text(deliveryPrice.toLocaleString('ru'));
                    amount.text(amountPrice.toLocaleString('ru'));
                    $('.js-amount-type .currency').show();
                } else {
                    amount.text(amountPrice.toLocaleString('ru'));
                    $('.js-amount-type .js-amount-type-price').html('Бесплатно');
                    $('.js-amount-type .currency').hide();
                }


            }
        }
    }

    /*
	ymaps.ready(function () {
		// преобразуем в массив из строки date
		date = '<?//=$arResult['MAP_DATA']?>';
	data = JSON.parse(date);

	// Создание экземпляра карты и его привязка к контейнеру с id = angerro_map
	var center = data[0].map.center;
	var zoom = data[0].map.zoom;

	myMap = new ymaps.Map('angerro_map_<?//=$arParams["MAP_ID"]?>', {
		center: center,
		zoom: zoom,
		controls: ['zoomControl']
	});

	// Создание коллекций геообъектов
	//в этой коллекции хранятся зоны доставки (полигоны)
	myGeoObjectsCollection_map = new ymaps.GeoObjectCollection();

	for (var k = 0; k < data[0].delivery_areas.length; k++) {
		var areaCoord = data[0].delivery_areas[k].area_coordinates;
		var areaColor = data[0].delivery_areas[k].settings.color;
		var areaTitle = data[0].delivery_areas[k].settings.title;
		myGeoObjectsCollection_map.add(new ymaps.Polygon(areaCoord,
			{pointColor: areaColor, deliveryZone: areaTitle, hintContent: areaTitle}, {fillColor: areaColor, opacity: 0.5}));

		//добавим названия зон доставки:
	    var delivery_area = document.createElement('div');
		delivery_area.className = "angerro_map_block_description";
		delivery_area.innerHTML = '<div class="angerro_map_block_color" style="background-color:'+areaColor+';"></div><div class="angerro_map_block_text">'+areaTitle+'</div><div class="angerro_map_block_clear"></div>';
		angerro_map_<?//=$arParams["MAP_ID"]?>_description.className = "angerro_map_block_description";
		angerro_map_<?//=$arParams["MAP_ID"]?>_description.appendChild(delivery_area);


	}
	// Устанавливаем опции всем геообъектам в коллекции прямо через коллекцию
	myGeoObjectsCollection_map.options
		.set({
			draggable: false,
			inderactive: 'none'
		});

	// Добавление коллекций геообъектов на карту
	myMap.geoObjects
		.add(myGeoObjectsCollection_map);
});
*/
</script>


<div class="order-map">
    <div id="order-map-delivery_<?=$arParams["MAP_ID"]?><?=$arParams['COUNT'] ? '_'.$arParams['COUNT'] : ''?>" style="width: <?=$arParams["WIDTH"]?>; height: <?=$arParams["HEIGHT"]?>;">
    </div>
    <div id="angerro_map_<?=$arParams["MAP_ID"]?>_description">
    </div>
    <input type="hidden" value="N" name="MAP_ERROR" class="map_has_errors">
    <input type="hidden" value="N" name="CORRECTADDRESS">
</div>