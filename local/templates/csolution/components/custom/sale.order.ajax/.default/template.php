<?

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var customOrderComponent $component */

//echo '<pre>'.print_r($arResult, true).'</pre>';

if($USER->IsAuthorized()) {
	$rsUser = CUser::GetByID($USER->getID());
	$arUser = $rsUser->Fetch();
}

$APPLICATION->SetTitle("Оформление заказа");
$APPLICATION->AddChainItem("Оформление заказа");


//echo 'MAP_ID ='.$arResult['CURRENT_MAP_ID'].PHP_EOL;

if($USER->isAdmin()) {

	echo 'DELIVERY_ID =' . $arResult['CURRENT_DELIVERY_ID'] . PHP_EOL;
	if ($arResult["ORDER_DAY_CART"])
		echo '[!!!] В товаре есть заказа который можно заказать только на сл.сутки';
}

$coord = [];
if($map = $_SESSION['GET_CITY']['CONTACTS_MAP']['VALUE']) {
	foreach($map as $item) {
		$item = explode(',', $item);
		$coord['POSITION'][] = [
			'LON' => $item[1],// LON и LAT - координаты элемента
			'LAT' => $item[0],
		];
	}
}
if($_COOKIE['currCity'] == 'main') {

	$arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_*");
	$arFilter = Array("IBLOCK_ID" => 5, "=CODE" => "MAIN", "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array(), $arFilter, false, array(), $arSelect);
	if($ob = $res->GetNextElement()) {

		$property = $ob->GetProperties();
	}

	$coord = [];
	if(is_array($property))
		foreach($property['CONTACTS_MAP']['VALUE'] as $item) {
			$item = explode(',', $item);
			$coord['POSITION'][] = [
				'LON' => $item[1],// LON и LAT - координаты элемента
				'LAT' => $item[0],
			];
		}
}

?>




<section class="order">
    <div class="container">
        <div class="order__row row">

            <?php if($arResult['ERRORS']): ?>
                <div class="col-12 error">
                    <ul class="error__list">
                        <?php foreach ($arResult['ERRORS'] as $ERROR): ?>
                            <li class="error__item"><?=$ERROR?></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php endif; ?>

            <div class="col-12 col-lg-6 col-vl-4 order-lg-2">
                <h2 class="order__cart-title h2">Корзина</h2>
                <ul class="order__product-list">

                    <?foreach($arResult['BASKET_ITEMS'] as $BASKET_ITEM):

                        //echo '<pre>'.print_r($BASKET_ITEM, true).'</pre>';

                        ?>

                        <li class="order__product-item d-flex">
                            <div class="order__product-img col-5 col-md-3">
                                <img src="<?=$BASKET_ITEM['PREVIEW_PICTURE_SRC']?>" alt=""/>
                            </div>
                            <div class="order__product-inner col-12 col-md-9 row">
                                <div class="order__product-char col-md-9 col-lg-8">
                                    <p class="order__product-count"><?=$BASKET_ITEM['QUANTITY']?> шт</p>
                                    <p class="order__product-title"><?=$BASKET_ITEM['NAME']?></p>
                                    <p class="order__product-weight"><?=$BASKET_ITEM['PROPERTY']['WEIGHT']['VALUE']?></p>
                                </div>
                                <p class="order__product-price col-md-3 col-lg-4">
                                    <span class="js-item-price"><?=format_price($BASKET_ITEM['SUM_NUM'])?></span>
                                    <span class="currency">₽</span>
                                </p>
                            </div>
                        </li>
                    <?endforeach;?>
                </ul>
                <div class="order__cart-total-wrap d-flex">
                    <p class="order__cart-total col-12">Итого: <span class="product-price"><?=format_price($component->order->getPrice())?></span>₽</p>
                </div>
            </div>
            <form class="order__form col-12 col-lg-6 col-vl-8 order-lg-1" method="post" action="">
                <h2 class="order__form-title h2">Оформление заказа</h2>
                <div class="order__form-inner">
                    <div class="order__input-field">
                        <label class="order__label" for="order-name">Имя</label>
                        <input class="order__input input" required type="text" value="<?=$arUser['NAME'] ? $arUser['NAME'] : $arResult['USER_FIELDS']['USER_NAME']?>" name="USER_NAME" placeholder="Имя" id="order-name"/>
                    </div>
                    <div class="order__input-field">
                        <label class="order__label" for="order-phone">Телефон</label>
                        <input class="order__input input phone" required type="tel" value="<?=$arUser['PERSONAL_PHONE'] ? $arUser['PERSONAL_PHONE'] : $arResult['USER_FIELDS']['USER_PHONE']?>" name="USER_PHONE" placeholder="Телефон" id="order-phone"/>
                    </div>
                    <div class="order__input-field">
                        <label class="order__label" for="order-email">Почта</label>
                        <input class="order__input input" type="email" required name="USER_EMAIL" value="<?=$arUser['EMAIL'] ? $arUser['EMAIL'] : $arResult['USER_FIELDS']['USER_EMAIL']?>" placeholder="Почта" id="order-email"/>
                    </div>
                </div>
                <div class="order__form-inner">
                    <div class="order__form-tabs">
                        <p class="order__form-tabs-text">Способ доставки:</p>
                        <div class="order__form-tabs-inner">
                            <label class="order__radio-label radio-label" for="delivery">
                                <input class="order__radio-input radio-input" type="radio" value="1" name="delivery" id="delivery" data-val="delivery" checked="checked"/>
                                <span class="order__radio-icon radio-icon"></span>
                                <span class="order__radio-text radio-text">Доставка курьером</span>
                            </label>

                            <label class="order__radio-label radio-label" for="pickup">
                                <input class="order__radio-input radio-input" type="radio" value="2" name="delivery" id="pickup" data-val="pickup"/>
                                <span class="order__radio-icon radio-icon"></span>
                                <span class="order__radio-text radio-text">Самовывоз</span>
                            </label>
                        </div>
                    </div>

                    <div class="order__form-info">
                        <div class="order__form-delivery-wrap row">
                            <div class="order__form-delivery-wrap-inner order__form-delivery-wrap-inner--vl-first col-12">
                                <input class="order__input input" id="order-suggest" name="USER_ADRESS_1" placeholder="Введите город, улицу, дом"
                                    value="<?=$arUser['UF_ADRESS'] ? array_pop($arUser['UF_ADRESS']) : ''?>"
                                />
                            </div>
                            <div class="order__form-delivery-wrap-inner order__form-delivery-wrap-inner--vl-right col-12 col-vl-6">
                                <?php //Карта
                                    $APPLICATION->IncludeComponent(
                                        "angerro:angerro.yadelivery",
                                        "",
                                        Array(
                                            "WIDTH" => "100%",
                                            //"WIDTH" => "490px",
                                            "HEIGHT" => "263px",
                                            "MAP_ID" => $arResult['CURRENT_MAP_ID']
                                        )
                                    );
                                ?>
                                <input type = "hidden" value="<?=$arResult['CURRENT_MAP_ID']?>" name = "bitrix_delivery_map">
                                <input type = "hidden" value="1" name = "bitrix_inside_area">
                                <input type = "hidden" value="0" name = "bitrix_delivery_map_accessprice">
                            </div>

                            <div class="order__form-delivery-wrap-inner order__form-delivery-wrap-inner--vl-left col-12 col-vl-6">
                                <input class="order__input input" name="USER_COMMENT_1" placeholder="Введите квартиру, офис"/>


                                <div class="order__select-wrap">
                                    <select class="order__select js-select-default" name="payment_id_1" data-label="Выберите способ оплаты" required>
										<?php foreach($arResult['PAY_SYSTEM'] as $payment):?>
                                            <option value="<?=$payment['ID']?>"><?=$payment['NAME']?></option>
                                        <?php endforeach;?>
                                    </select>
                                    <svg class="icon-svg">
                                        <use xlink:href="/img/sprite.svg#icon-arrow"></use>
                                    </svg>
                                </div>
                                <input class="order__input input js-datepicker form-control" name="USER_DATE_1" value="<?=date('d.m')?>" type="text" placeholder="Дата"/>
                                <div class="order__select-wrap">
                                    <select class="order__select js-select-date" name="USER_TIME_1" data-label="Выберите время" required>
										<?for($i = 1; $i < count($arResult['TIME']); $i++):
											    if($i % 2 == 0) continue;
											    $time = sprintf('%s до %s', $arResult['TIME'][$i - 1], $arResult['TIME'][$i]);
											?>
                                            <option value="<?=$time?>"><?=$time?></option>
										<?endfor;?>
                                    </select>
                                    <svg class="icon-svg">
                                        <use xlink:href="/img/sprite.svg#icon-arrow"></use>
                                    </svg>
                                </div>

                            </div>
                            <input name="delivery_id_1" value="7" type="hidden">
                        </div>
                        <div class="order__form-pickup-wrap row minimized">
                            <div class="order__form-delivery-wrap-inner order__form-delivery-wrap-inner--vl-right col-12 col-vl-6">
                                <div class="order__form-pickup-types">
                                    <?php
									$count = null;
									foreach($arResult['DELIVERY'] as $deliveries)
                                        foreach($deliveries as $delivery):

                                            if(strpos( $delivery['MAIN_NAME'],'Самовывоз') === false) continue;
                                            if($delivery['PARENT_ID'] != $arResult['CURRENT_DELIVERY_ID']) continue;

                                    ?>
                                        <label class="order__radio-label radio-label" for="<?='delivery_'.$delivery['ID']?>">
                                            <input class="order__radio-input radio-input" <?=!$count ? 'checked' : ''?> type="radio" value="<?=$delivery['ID']?>" name="delivery_id_2" id="<?='delivery_'.$delivery['ID']?>"/>
                                            <span class="order__radio-icon radio-icon"></span>
                                            <span class="order__pickup-radio-text radio-text"><?=$delivery['NAME']?></span>
                                        </label>
                                    <?php $count++; endforeach; ?>
                                </div>

<!--                                <div class="order-map" id="order-map-pickup"></div>-->
                            </div>
                            <div class="order__form-delivery-wrap-inner order__form-delivery-wrap-inner--vl-left col-12 col-vl-6">
                                <div class="order__select-wrap">
                                    <select class="order__select js-select-default-2" name="payment_id_2" data-label="Выберите способ оплаты" required>
										<?php foreach($arResult['PAY_SYSTEM'] as $payment):?>
                                            <option value="<?=$payment['ID']?>"><?=$payment['NAME']?></option>
										<?php endforeach;?>
                                    </select>
                                    <svg class="icon-svg">
                                        <use xlink:href="/img/sprite.svg#icon-arrow"></use>
                                    </svg>
                                </div>
                                <div class="order__form-date-wrap">
                                    <input class="order__input order__date-input input js-datepicker form-control" name="USER_DATE_2" value="<?=date('d.m')?>" type="text" placeholder="Дата"/>


                                    <div class="order__select-wrap">
                                        <select class="order__select js-select-date" name="USER_TIME_2" data-label="Выберите время" required>
											<?for($i = 1; $i < count($arResult['TIME']); $i++):
												if($i % 2 == 0) continue;
												$time = sprintf('%s до %s', $arResult['TIME'][$i - 1], $arResult['TIME'][$i]);
												?>
                                                <option value="<?=$time?>"><?=$time?></option>
											<?endfor;?>
                                        </select>
                                        <svg class="icon-svg">
                                            <use xlink:href="/img/sprite.svg#icon-arrow"></use>
                                        </svg>
                                    </div>

                                </div>
                                <textarea class="order__comment-textarea textarea" name="USER_COMMENT_2" placeholder="Комментарий"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="order__amount col-vl-6">
                    <div class="order__amount-inner">
                        <p class="order__amount-text col-12">
                            Оплата
<!--                            <span class="order__amount-choose js-amount-choose">наличными при получении</span>-->
                        </p>

                        <p class="order__amount-text js-amount-type col-12">
                            Доставка:
                            <span class="js-amount-type-price">Бесплатно</span>
                            <span class="currency" style="display: none">₽</span>
                        </p>

                        <p class="order__amount-total col-12">Итого:
                            <span class="js-amount-total-price" data-price = "<?=$component->order->getPrice()?>"><?=$component->order->getPrice()?></span>
                            <span class="currency">₽</span>
                        </p>
                    </div>
                </div>

                <!--
                    <input type="hidden" name ="delivery_id" value="3">
                    <input type="hidden" name ="payment_id" value="7">
                -->

                <input type ="hidden" name = "save" value="Y">
                <div class="order__submit-wrap">
                    <button class="order__submit-btn btn order-btn" type="submit">Заказать</button>
                    <p class="policy">
                        Нажимая на кнопку «Оформить заказ», вы даете согласие на обработку
                        <a class="policy__link" href="#">своих персональных данных.</a>
                    </p>
                </div>
            </form>
        </div>
    </div>
</section>
