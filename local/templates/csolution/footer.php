</main>



<footer class="main-footer">
    <div class="container">
        <div class="row main-footer__main-row">
            <div class="col-12 col-sm-8 col-md-6 col-lg-4 col-xl-3 order-md-3 d-lg-none d-xl-flex">
                <div class="main-footer__contacts">
					<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
						"AREA_FILE_SHOW" => "file",
						"PATH" => $includePath ? $includePath.'/footer_phone.php' : "/include/footer_phone.php",
						"EDIT_TEMPLATE" => ""
					),
						array(
							'HIDE_ICONS' => 'Y'
						)
					);?>
                    <div class="main-footer__contacts-inner">
						<p class="main-footer__contacts-text">Принимаем заказы c&nbsp;9:00 до&nbsp;20:00</p>
						<p class="main-footer__contacts-text">Доставка c&nbsp;10:00 до&nbsp;22:00</p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-8 col-xl-6 order-md-1 main-footer__nav">
                <div class="footer-nav">
                    <h2 class="footer-nav__title">Заказать</h2>
					<?
					$APPLICATION->IncludeComponent(
						"bitrix:catalog.section.list",
						"main_menu_bottom",
						array(
							"IBLOCK_TYPE" => "catalog",
							"IBLOCK_ID" => 1,
							"CACHE_TYPE" => $arParams["CACHE_TYPE"],
							"CACHE_TIME" => $arParams["CACHE_TIME"],
							"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
							"COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
							"TOP_DEPTH" => 1,
							"SECTION_URL" => '/catalog/'.'#SECTION_CODE_PATH#/',
							"VIEW_MODE" => $arParams["SECTIONS_VIEW_MODE"],
							"SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
							"HIDE_SECTION_NAME" => (isset($arParams["SECTIONS_HIDE_SECTION_NAME"]) ? $arParams["SECTIONS_HIDE_SECTION_NAME"] : "N"),
							"ADD_SECTIONS_CHAIN" => (isset($arParams["ADD_SECTIONS_CHAIN"]) ? $arParams["ADD_SECTIONS_CHAIN"] : '')
						),
						$component,
						($arParams["SHOW_TOP_ELEMENTS"] !== "N" ? array("HIDE_ICONS" => "Y") : array())
					);
					?>
                </div>
                <div class="footer-nav">
                    <h2 class="footer-nav__title">О компании</h2>
					<?
					$APPLICATION->IncludeComponent(
						"bitrix:menu",
						"footer_menu_1",
						array(
							"ROOT_MENU_TYPE" => "footer2",
							"MENU_CACHE_TYPE" => "Y",
							"MENU_CACHE_TIME" => "36000000",
							"MENU_CACHE_USE_GROUPS" => "N",
							"MENU_CACHE_GET_VARS" => array(
							),
							"MAX_LEVEL" => "1",
							"CHILD_MENU_TYPE" => "left",
							"USE_EXT" => "Y",
							"DELAY" => "N",
							"ALLOW_MULTI_SELECT" => "N",
						),
						false,
						array(
							"ACTIVE_COMPONENT" => "Y"
						)
					);
					?>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4 col-xl-3 order-md-2">
                <div class="payment"><h3 class="payment__title">Способы оплаты</h3>
                    <div class="payment__inner">
                        <img class="payment__img payment__img--visa" src="/img/visa.png" alt="visa"/>
                        <img class="payment__img payment__img--master" src="/img/mastercard.png" alt="mastercard"/>
                    </div>
                </div>
                <div class="main-footer__contacts d-none d-lg-block d-xl-none">

					<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
						"AREA_FILE_SHOW" => "file",
						"PATH" => $includePath ? $includePath.'/footer_phone.php' : "/include/footer_phone.php",
						"EDIT_TEMPLATE" => ""
					),
						array(
							'HIDE_ICONS' => 'Y'
						)
					);?>
                    <div class="main-footer__contacts-inner">
                        <p class="main-footer__contacts-text">Принимаем заказы c 9:00 до 20:00</p>
                        <p class="main-footer__contacts-text">Доставка c 10:00 до 22:00</p></div>
                </div>
            </div>
        </div>
    </div>
    <div class="main-footer__copyright-row">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-5 col-xl-4 col-vl-6 order-sm-2 order-md-1">
                    <div class="main-footer__copyright-header">
                        <a class="main-footer__logo" href="/">
                            <picture>
                                <source media="(min-width: 1200px)" srcset="/img/footer-logo.png"/>
                                <img src="/img/footer-logo-320.png" alt=""/>
                            </picture>
                        </a>
                        <p class="main-footer__copyright-text">Все права защищены «Вот такие пироги» © 2010-<?=date('Y')?></p>
                        <p class="main-footer__info">
                            <a class="main-footer__link link" href="/publichnaya-oferta/">Условия публичной оферты </a>и
                            <a class="main-footer__link link" href="/politika-konfidentsialnosti/">Политика конфиденциальности</a></p>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-3 col-vl-2 order-sm-1 order-md-2 offset-lg-1 offset-vl-0 ml-vl-auto mr-vl-auto">

					<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
						"AREA_FILE_SHOW" => "file",
						"PATH" => $includePath ? $includePath.'/socials.php' : "/include/socials.php",
						"EDIT_TEMPLATE" => ""
					),
						array(
							'HIDE_ICONS' => 'Y'
						)
					);?>

                    <!--
                    <div class="social">
                        <ul class="social__list">
                            <li class="social__item">
                                <a class="social__link social__link--vk" href="#" target="_blank">
                                    <svg class="icon-svg">
                                        <use xlink:href="#icon-vk"></use>
                                    </svg>
                                </a>
                            </li>
                            <li class="social__item">
                                <a class="social__link social__link--fb" href="#" target="_blank">
                                    <svg class="icon-svg">
                                        <use xlink:href="#icon-fb"></use>
                                    </svg>
                                </a>
                            </li>
                            <li class="social__item">
                                <a class="social__link social__link--inst" href="#" target="_blank">
                                    <svg class="icon-svg">
                                        <use xlink:href="#icon-inst"></use>
                                    </svg>
                                </a>
                            </li>
                        </ul>
                    </div> -->
                    <a class="main-footer__create d-none d-lg-block d-xl-none" href="https://constructivesolutions.ru/" target="_blank">Дизайн и разработка —
                        <svg class="icon-svg">
                            <use xlink:href="/img/sprite.svg#icon-cs"></use>
                        </svg>
                    </a>
                </div>
                <div class="col-12 col-md-6 col-lg-4 col-xl-4 col-vl-3 ml-md-auto ml-lg-0 ml-xl-auto order-sm-3 d-lg-none d-xl-block">
                    <a class="main-footer__create" href="https://constructivesolutions.ru/" target="_blank">
                        Дизайн и разработка —
                        <svg class="icon-svg">
                            <use xlink:href="/img/sprite.svg#icon-cs"></use>
                        </svg>
                    </a>
                </div>
            </div>
        </div>
    </div>
</footer>

<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	"AREA_FILE_SHOW" => "file",
	"PATH" => $includePath ? $includePath.'/scripts.php' : "/include/scripts.php",
	"EDIT_TEMPLATE" => ""
),
	array(
		'HIDE_ICONS' => 'Y'
	)
);?>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/main-select.js?v=1"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/order.js?v=1"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/main.js?v=3"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/select.js?v=1"></script>


<script src="https://www.google.com/recaptcha/api.js?render=6LcLzHcUAAAAAEnMayuLaFJt6oYzqc57btJ1ygX0"></script>
<script>grecaptcha.ready(function () {
        grecaptcha.execute('6LcLzHcUAAAAAEnMayuLaFJt6oYzqc57btJ1ygX0', {action: 'action_name'}).then(function (token) {
        })
    });</script>


<script type="text/javascript">
    $(function() {
        
        $('.main-header__phone-toggle').attr('href', $('.main-header__info-phone').attr('href'));

        $(document).on('click', '.ajax-order' , function (e) {
            e.preventDefault();

            let reload = 0;
            if($(this).closest('section.recommend').hasClass('cart'))
                reload = 1;

            console.log(reload);


            function productStr(n) {
                let word;
                if ((n === 1) || (n > 20 && n % 10 === 1)) word = "товар";
                else if ((n >= 2 && n <= 4) || (n > 20 && n % 10 >= 2 && n % 10 <= 4)) word = "товара";
                else word = "товаров";
                return word;
            }

            //console.log($(this).closest('.js-toggles-parent').serialize());
            $.ajax({
                'url': '<?=SITE_TEMPLATE_PATH.'/ajax/add_basket.php'?>',
                'type': 'POST',
                'data': $(this).closest('.js-toggles-parent').serialize(),
                'success': function (response) {
                    if(reload)
                        window.location.reload(true);
                    let data = $.parseJSON(response);



                    
                    $('.cart-amount').html(data.TOTAL_SUM_FORMATED);
                    $('.cart-count').html(data.ELEMENTS + ' ' + productStr(data.ELEMENTS));
                }
            });
            
            if ($('.js-sticky-subnav').hasClass('_sticky')) {
                $(this).closest('.js-flyToCart-wrap').find('.js-flyToCart-img')
                    .clone()
                    .html('')
                    .css({
                        'position': 'absolute',
                        'border-radius': '50%',
                        'height': '50px',
                        'width': '50px',
                        'z-index': '99999999',
                        top: $(this).offset().top,
                        left: $(this).offset().left
                    })
                    .appendTo("body")
                    .animate({
                        opacity: 1,
                        left: $(".sub-nav__cart").offset()['left'],
                        top: $(".sub-nav__cart").offset()['top'],
                        width: 20,
                        height: 20
                    }, 1000, function () {
                        $(this).remove();
                    });
            } else {
                $(this).closest('.js-flyToCart-wrap').find('.js-flyToCart-img')
                    .clone()
                    .html('')
                    .css({
                        'position': 'absolute',
                        'border-radius': '50%',
                        'height': '50px',
                        'width': '50px',
                        'z-index': '99999999',
                        top: $(this).offset().top,
                        left: $(this).offset().left
                    })
                    .appendTo("body")
                    .animate({
                        opacity: 1,
                        left: $(".main-header__cart").offset()['left'],
                        top: $(".main-header__cart").offset()['top'],
                        width: 20,
                        height: 20
                    }, 1000, function () {
                        $(this).remove();
                    });
            }
        });
		
    });
</script>


<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(34386710, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true,
        trackHash:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/34386710" style="position:absolute; left:-9999px;" alt="" /></div>
</noscript>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-WJ4B4K2');</script>
<!-- End Google Tag Manager -->
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WJ4B4K2"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>


</body>