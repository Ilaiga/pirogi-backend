<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

session_start();

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
if (CModule::IncludeModule("catalog") && CModule::IncludeModule("sale")) {

	$PRODUCT_ID = $_POST['product_id'];
	$COUNT = $_POST['count'];
	$PRICE_ID = $_POST['priceId'];

	/*
	   $res = CCatalogGroup::GetListEx(array(), array('=NAME' => $_SESSION['GET_CITY']['CODE_PRICE']['VALUE']), false, false, array('ID'));
	   if ($group = $res->Fetch())
	*/


	//echo print_r($_POST);
	if (IntVal($PRODUCT_ID) > 0 && !empty($PRICE_ID)) {


		$PRICE_CODE = $_SESSION['GET_CITY']['CODE_PRICE']['VALUE'] ?: 'BASE';

		$priceID = (CCatalogGroup::GetListEx(array(), array('=NAME' => $PRICE_CODE), false, false, array('ID')))->Fetch();

		//$product = (CIBlockElement::GetByID($PRODUCT_ID))->GetNext();
		//$arPrice = CPrice::GetByID($priceID['ID']);

        $dbPrice = CPrice::GetList(
            array("QUANTITY_FROM" => "ASC", "QUANTITY_TO" => "ASC", "SORT" => "ASC"),
            array("PRODUCT_ID" => $PRODUCT_ID),
            false,
            false,
            array("ID", "CATALOG_GROUP_ID", "PRICE", "CURRENCY", "QUANTITY_FROM", "QUANTITY_TO")
        );
        while ($arPrice = $dbPrice->Fetch())
        {
            if($arPrice['CATALOG_GROUP_ID'] == $PRICE_ID) {

                $arDiscounts = CCatalogDiscount::GetDiscountByPrice(
                    $arPrice["ID"],
                    $USER->GetUserGroupArray(),
                    "N",
                    SITE_ID
                );
                $discountPrice = CCatalogProduct::CountPriceWithDiscount(
                    $arPrice["PRICE"],
                    $arPrice["CURRENCY"],
                    $arDiscounts
                );
                $arPrice["DISCOUNT_PRICE"] = $discountPrice;
                break;
            }
        }

		$product = CCatalogProduct::GetByIDEx($PRODUCT_ID, true);
		$price = $product['PRICES'][$priceID['ID']]['PRICE'];
		if($arPrice["DISCOUNT_PRICE"] != 0)
            $price = $arPrice["DISCOUNT_PRICE"];
        

		$arFields = array(
			"NAME" => $product['NAME'],
			"PRODUCT_ID" => $PRODUCT_ID,
			//"PRODUCT_PRICE_ID" => $PRICE_ID,
			"PRICE" => $price,
			"QUANTITY" => $COUNT ? $COUNT : 1,
			"CURRENCY" => "RUB",
			"LID" => LANG,
		);
		CSaleBasket::Add($arFields);

		/*Add2BasketByProductID(
			$PRODUCT_ID,
			$COUNT ? $COUNT : 1,
			array(
				array("NAME" => "Название продукта", "CODE" => "ORIGINAL_NAME", "VALUE" => 'ORIGINAL NAME'),
				//array("NAME" => "Вес", "CODE" => "WEIGHT", "VALUE" => $WEIGHT)
			)
		);
		*/

		if ($ex = $APPLICATION->GetException()) {
			echo '<br>' . $ex->GetString();
			return true;
		}
		$basket = (new MyBasketClass())->BasketInit();

		echo json_encode($basket);
	}
	$_SESSION['TOTAL_SUM_FORMATED'] = $basket['TOTAL_SUM_FORMATED'];
	$_SESSION['TOTAL_COUNT'] = $basket['ELEMENTS'];
}

