<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();


CModule::IncludeModule('CUser');

use Bitrix\Main\UserTable;
$user = new CUser;
if(!empty($_POST['RemoveAdress']) && $user->IsAuthorized()) {

	$arFields = $user->GetByID($user->GetID())->Fetch();
	if(is_array($arFields['UF_ADRESS'])) {
		unset($arFields['UF_ADRESS'][$_POST['Key']]);
	}
	$fields = Array(
		"UF_ADRESS" => $arFields['UF_ADRESS'],
	);

	$user->Update($user->GetID(), $fields);

	echo '<pre>'.print_r($arFields, true).'</pre>';
}