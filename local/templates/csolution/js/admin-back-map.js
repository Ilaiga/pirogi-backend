for (var k = 0; k < data.features.length; k++) {
    var areaCoord = data.features[k].geometry.coordinate;
    var areaColor = data.features[k].options.fillColor;
    var areaTitle = data.features[k].properties.title;

    var AreaMinDelivery = data.features[k].properties.minDelivery;
    var AreadeliveryPrice = data.features[k].properties.deliveryPrice;


    myMap_tab3.geoObjects.add(new ymaps.Polygon(areaCoord, {
        //TODO: коммент для мерджа потом удалить!!!!
        pointColor: areaColor,
        deliveryZone: areaTitle,
        hintContent: areaTitle
    }, {
        fillColor: areaColor,
        opacity: 0.5
    }));
    colors_tab_3[k] = areaColor;

    area_titles_tab_3.push(areaTitle);
    minDeliveryPrice_tab_3.push(AreaMinDelivery);
    DeliveryPrice_tab_3.push(AreadeliveryPrice);


}