(function ($) {
    $.fn.mainSelect = function (className, text) {
        let selectOption = $(this).find('option');
        let selectOptionLength = selectOption.length;
        let selectedOption = selectOption.filter(':selected');

        $(this).hide();
        $(this).wrap('<div class="'+ className + '-wrap"></div>');
        $('<div>', {
            class: className,
            text: text
        }).insertAfter($(this));

        let selectGap = $(this).next('.' + className);

        $('<ul>', {
            class: className + '__list'
        }).insertAfter(selectGap);

        let selectList = selectGap.next('.' + className + '__list');

        for (var i = 0; i < selectOptionLength; i++) {
            $('<li>', {
                class: className + '__item',
                html: $('<span>', {
                    class: className + '__item-text',
                    text: selectOption.eq(i).text()
                })
            })
            .attr('data-value', selectedOption.eq(i).val())
            .appendTo(selectList);
        }

        let selectItem = selectList.find('li');

        selectList.slideUp();

        selectGap.click(function() {
            if (!$(this).hasClass('on')) {

                if(!$(this).hasClass(className + '--active')) {
                    $(this).addClass(className + '--active');
                }
                    
                selectGap.addClass('on');
                selectList.slideDown();

                selectItem.click(function() {
                    selectItem.each(function() {
                        $(this).removeClass(className + '__item--active');
                    });

                    if (!$(this).hasClass(className + '__item--active')) {
                        $(this).addClass(className + '__item--active');
                    }
                    let chooseItem = $(this).data('value');

                    $('.js-select-default').val(chooseItem).attr('selected', 'selected');
                    selectGap.text($(this).find('span').text());

                    selectList.slideUp();
                    selectGap.removeClass('on');
                });
            } else {
                $(this).removeClass('on');
                selectList.slideUp();
            }
        });
    }
        
})(jQuery);