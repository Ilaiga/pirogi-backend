//





// Скрипты кликов и типичных событий



// Выпадашка мобильного меню
let menuToggle = $('.main-header__toggle');
let menuWrap = $('.main-header__nav-wrap');

showBlock('main-header__nav-wrap', menuWrap,menuToggle);

function showBlock(className, block, toggle) {
    /*toggle.click( function() {
        block.toggleClass(className + '--active');
        toggle.toggleClass('animate');
    });*/
    toggle.click( function() {
        block.toggleClass(className + '--active');
        toggle.toggleClass('animate');

        if($(window).width() < 768 && (block.hasClass(className+'--active'))) {
            $('body').css({
                'min-width': '100%',
                'width': $(window).width(),
                'position': 'fixed',
                'overflow': 'hidden'
            })
        } else {
            $('body').css({
                'min-width': 'none',
                'width': 'auto',
                'position': 'initial',
                'overflow': 'auto'
            });
        }
    });
}

$('.order__select').on('selectric-init', function () {
    $(this).closest('.order__select-wrapper').find('.label').text($(this).data('label'))
});

// Слайдеры 

$('.slider').owlCarousel(
    {
        loop: true,
        dots: true,
        nav: false,
        dotsClass: 'dots-wrap',
        dotClass: 'dot',
        items: 1,
        autoHeight: true,
        // autoplay:true,
        autoplayTimeout:10000,
    }
);

$('.sales__slider').owlCarousel({
    loop: true,
    dots: true,
    nav: false,
    dotsClass: 'dots-wrap',
    dotClass: 'dot',
    margin: 10,
    responsive: {
        0: {
            items: 1
        },
        1200: {
            items: 2
        }

    }
});

$('.life__slider').owlCarousel({
    loop: true,
    dots: false,
    nav: false,
    dotsClass: 'dots-wrap',
    dotClass: 'dot',
    margin: 10,
    responsive: {
        0: {
            items: 1
        },
        768: {
            items: 2
        },
        1200: {
            items: 4,
            margin: 25
        }

    }
});

if($('.product__slider').find('.product__slider-item').length === 3) {
    $('.product__slider').slick({
        slidesToShow: 1,
        arrows: true,
        slidesToScroll: 1,
        dots: false,
        prevArrow: '<button type="button" class="slick-prev slick-arrow"><svg class="icon-svg"><use xlink:href="img/sprite.svg#icon-arrow"></use></svg></button>',
        nextArrow: '<button type="button" class="slick-next slick-arrow"><svg class="icon-svg"><use xlink:href="img/sprite.svg#icon-arrow"></use></svg></button>',
        dotsClass: 'dots-wrap',
    });

    $('.product__slider-nav-item').each(function() {
        $(this).click(function(e) {
            e.preventDefault();
            let slideIndex = $(this).index();
            // $('.product__slider').slickGoTo(parseInt(slideIndex));
            $('.product__slider').slick('slickGoTo', parseInt(slideIndex));
        });
    });
} else {
    $('.product__slider').slick({
        slidesToShow: 1,
        arrows: true,
        slidesToScroll: 1,
        dots: false,
        prevArrow: '<button type="button" class="slick-prev slick-arrow"><svg class="icon-svg"><use xlink:href="img/sprite.svg#icon-arrow"></use></svg></button>',
        nextArrow: '<button type="button" class="slick-next slick-arrow"><svg class="icon-svg"><use xlink:href="img/sprite.svg#icon-arrow"></use></svg></button>',
        dotsClass: 'dots-wrap',
        asNavFor: '.product__slider-nav',
    });

    $('.product__slider-nav').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.product__slider',
        arrows: false,
        dots: false,
        //centerMode: true,
        focusOnSelect: true,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 5
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3
                }
            }
        ]
    });
}

// Смена количества в карточке товара


$('.product__counter-toggle').on('click', function () {
    let $this = $(this),
        $countElem = $($this.closest('.product__counter-wrap')).find('.product__counter-count');

    let singlePrice = $('.product-single__prices');
    let price = parseInt($('.product-single__prices').data('price'));
    let count = +$countElem.val();

    if ($this.hasClass('js-counter-minus') && count > 1) {
        count--;
    } else if ($this.hasClass('js-counter-plus')) {
        count++;
    } else {
        alert('что-то пошло не так...');
        return;
    }
    $countElem.val(count);

    let price_format = price * count;
    singlePrice.html(price_format.toLocaleString('ru'));

});



// Аккордеон

let orderItems = $('.settings-history__item');

orderItems.each(function() {
    let toggle = $(this).children('.js-accordeon-toggle');
    toggle.click(function() {
        let list = $(this).closest('.settings-history__item').find('.settings-history__product-list');
        if(list.is(':hidden')) {
            list.slideDown();
            $(this).toggleClass('settings-history__item-toggle--close');
        } else {
            list.slideUp();
            $(this).toggleClass('settings-history__item-toggle--close');
        }
    });
});

// Промокод
/*$('input[name="promocode"]').keyup(function () {
    let data = $('input[name="promocode"]').val();

    if ($(this).hasClass("js-input-promocode-submit--error")) {
        $('input[name="promocode"]').val("");
        $(".input-promocode-submit").removeClass("input-promocode-submit--error");
        $(".input-promocode-wrap").removeClass("input-promocode-wrap--error");
        $(".input-promocode-submit-text").show();
        $(".input-promocode-submit").removeClass("input-promocode-submit--ok");
        $(".input-promocode-submit").removeClass("js-input-promocode-submit--error");
    } else {
        if (data === "test") {
            $(".input-promocode-submit").removeClass("input-promocode-submit--error");
            $(".input-promocode-submit").removeClass("js-input-promocode-submit--error");
            $(".input-promocode-submit-text").hide();
            $(".input-promocode-submit").addClass("input-promocode-submit--ok");
            $(".input-promocode-wrap").removeClass("input-promocode-wrap--error");

        } else {
            $(".input-promocode-submit").removeClass("input-promocode-submit--ok");
            $(".input-promocode-wrap").addClass("input-promocode-wrap--error");
            $(".input-promocode-submit-text").hide();
            $(".input-promocode-submit").addClass("input-promocode-submit--error");
            $(".input-promocode-submit").addClass("js-input-promocode-submit--error");
        }
    }
});*/

//Маска на телефон
$(".phone").inputmask("+7 (999) 999-99-99");

const delivery = $('.order__form-inner');
const deliveryRadio = delivery.find('.order__form-tabs-inner input');
const defaultDeliveryindow = delivery.find('.order__form-delivery-wrap');
const pickupDeliveryindow = delivery.find('.order__form-pickup-wrap');


deliveryRadio.on('change', function () {
    var th = $(this);

    if (th.attr("data-val") === 'pickup') {
        defaultDeliveryindow.addClass('minimized');
        pickupDeliveryindow.removeClass('minimized');
    } else if (th.attr("data-val") === 'delivery') {
        defaultDeliveryindow.removeClass('minimized');
        pickupDeliveryindow.addClass('minimized');
    }
});


// Переключение таглов

let toggles = $('.js-toggles-wrap');

toggles.each(function() {
    let thinToggle = $(this).children('.js-thin-toggle');
    let fatToggle = $(this).children('.js-fat-toggle');

    let thinPrice = $(this).parents('.js-toggles-parent').find('.js-price-wrap').find('.js-thin-price');
    let fatPrice = $(this).parents('.js-toggles-parent').find('.js-price-wrap').find('.js-fat-price');

    thinToggle.click(function(event) {
        $(this).prop('disabled', !$(this).prop('disabled'));
        fatToggle.removeAttr('disabled');
        $(this).toggleClass('toggle-active');
        fatToggle.toggleClass('toggle-active');
        thinPrice.show();
        fatPrice.hide();
    });

    fatToggle.click(function() {
        $(this).prop('disabled', !$(this).prop('disabled'));
        thinToggle.removeAttr('disabled');
        $(this).toggleClass('toggle-active');
        thinToggle.toggleClass('toggle-active');
        fatPrice.show();
        thinPrice.hide();
    });
});

//Кнопки поделиться на страница карточки товара

const Share = {
    vk: function (purl, ptitle, pimg, text) {
        var url = 'http://vkontakte.ru/share.php?';
        url += 'url=' + encodeURIComponent(purl);
        url += '&title=' + encodeURIComponent(ptitle);
        url += '&description=' + encodeURIComponent(text);
        url += '&image=' + encodeURIComponent(pimg);
        url += '&noparse=true';
        Share.open(url);
    },
    facebook: function (purl, ptitle, pimg, text) {
        var url = 'https://www.facebook.com/sharer.php?s=100';
        url += '&p[title]=' + encodeURIComponent(ptitle);
        url += '&p[summary]=' + encodeURIComponent(text);
        url += '&p[url]=' + encodeURIComponent(purl);
        url += '&p[images][0]=' + encodeURIComponent(pimg);
        Share.open(url);
    },
    ok: function (purl, ptitle, pimg) {
        var url = 'https://connect.ok.ru/offer?';
        url += 'url=' + encodeURIComponent(purl);
        url += '&title=' + encodeURIComponent(ptitle);
        url += '&imageUrl=' + encodeURIComponent(pimg);
        Share.open(url);
    },
    open: function (url) {
        window.open(url, '_blank');
    }
};

var $shareBtn = $('.product__share');
$shareBtn.on('click', function () {
    var $this = $(this);
    if ($this.hasClass('product__share--active')) {
        $this.removeClass('product__share--active').find('.js-product-share-dropdown').fadeOut(300);
    } else {
        $this.addClass('product__share--active').find('.js-product-share-dropdown').fadeIn(300);
    }
});

$(".js-share").on('click', function (e) {
    e.preventDefault();

    Share[$(this).data('target')]($(this).data('url'), $(this).data('title'), $(
        this).data('img'), $(this).data('text'));
});

// City Select

$('.geo__select').on('change', function () {

    let element = $(this).children('option:selected');
    let city = element.val();
    let request = element.data('request');


    $.cookie('currCity', city, { expires: 31, path: '/', domain: '.vot-takie-pirogi.ru' });


    if(city == 'main')
        return window.location.href = '//vot-takie-pirogi.ru' + request == undefined ? '' : request;
    else
        return window.location.href = '//' + city + '.vot-takie-pirogi.ru' + request == undefined ? '' : request;
});

$('.order__form').keypress(function (event) {
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if (keycode == '13') {
        event.preventDefault();
    }
});

// sticky header
let subnav = $('.js-sticky-subnav');
let stickyHeader = $('.js-sticky-header');
if($(window).width() >= 1200) {


    subnav.sticky({
        zIndex: 20,
    });

    subnav.on('sticky-start', function() {
        subnav.addClass('_sticky');
        $('.sub-nav').addClass('_sticky');
    });
    subnav.on('sticky-end', function () {
        subnav.removeClass('_sticky');
        $('.sub-nav').removeClass('_sticky');
    })
} else {
    subnav.unstick();
}


if($(window).width() <= 767) {
    stickyHeader.sticky({
        zIndex: 20,
    });

    stickyHeader.on('sticky-start', function() {
        stickyHeader.addClass('_sticky');
        $('.main-header__top-row').addClass('_sticky');
    });
    stickyHeader.on('sticky-end', function () {
        stickyHeader.removeClass('_sticky');
        $('.main-header__top-row').removeClass('_sticky');
    })
} else {
	console.log('work');
    stickyHeader.unstick();
}

// selects

$('.geo__select').selectric({
    disableOnMobile: true,
    nativeOnMobile: true,
    customClass: {
        prefix: 'geo-select',
        camelCase: false
    },
    arrowButtonMarkup: '<button class="geo__select-button"><svg class="icon-svg"><use xlink:href="img/sprite.svg#icon-arrow"></use></svg></button>'
});

$('.order__select').selectric({
    disableOnMobile: true,
    nativeOnMobile: true,
    customClass: {
        prefix: 'order__select',
        camelCase: false
    },
    arrowButtonMarkup: '<button type="button" class="order__select-button"><svg class="icon-svg"><use xlink:href="img/sprite.svg#icon-arrow"></use></svg></button>'
})

//resize reload
// let breakpoint = {
//     xs: 575,
//     sm: 767,
//     md: 991,
//     lg: 1199,
//     xl: 1599
// }
function resizer() {
    var windowWidth = $(window).width();
    var breakPoints = {
        sm: 576,
        md: 768,
        lg: 992,
        xl: 1200
    };

    let breakPoint = false;

    if (windowWidth < breakPoints.sm) {
        breakPoint = 'xs';
    } else if (windowWidth < breakPoints.md) {
        breakPoint = 'sm';
    } else if (windowWidth < breakPoints.lg) {
        breakPoint = 'md';
    } else if (windowWidth < breakPoints.xl) {
        breakPoint = 'lg';
    } else {
        breakPoint = 'xl';
    }

    return breakPoint;
};

let textOverflowArr = [];
$(window).resize(function() {
    let breakPoint = resizer();


    if(breakPoint === 'sm' || breakPoint === 'xs') {
        $('.geo').appendTo('.main-header__nav-wrap .container .row');
        $('.main-header__nav-wrap-inner').appendTo('.main-header__nav-wrap .container .row');
        $('.main-header__info').appendTo('.main-header__nav-wrap-inner');
        $('.main-header__user').appendTo('.main-header__nav-wrap-inner');
        $('.sub-nav').appendTo('.main-header__nav-wrap .container .row');
        $('.main-nav').appendTo('.main-header__nav-wrap .container .row');

        if (textOverflowArr.length) {
            textOverflowArr.forEach(function (item) {
                item.API.restore();
            });
            textOverflowArr = [];
        }

    } else if (breakPoint === 'md') {
        $('.geo').appendTo('.main-header__top-row');
        $('.main-nav').appendTo('.main-header__top-row');
        $('.main-header__nav-wrap-inner').appendTo('.main-header__nav-wrap .container .row');
        $('.main-header__info').appendTo('.main-header__nav-wrap-inner');
        $('.main-header__user').appendTo('.main-header__nav-wrap-inner');
        $('.sub-nav').appendTo('.main-header__nav-wrap .container .row');


        if(!textOverflowArr.length) {
            $('.js-catalog-item-desc').each(function () {
                let dot = new Dotdotdot($(this)[0], {
                    height: 75
                });
                textOverflowArr.push(dot);

            });

            $('.js-catalog-item-title').each(function () {
                let dot = new Dotdotdot($(this)[0], {
                    height: 65
                });
                textOverflowArr.push(dot);

            });
        }

    } else if (breakPoint === 'lg') {
        $('.main-header__nav-wrap').removeClass('main-header__nav-wrap--active');
        $('.main-header__toggle').removeClass('animate');
        $('.geo').appendTo('.main-header__top-row');
        $('.main-nav').appendTo('.main-header__top-row');
        $('.main-header__info').prependTo('.main-header__inner');
        $('.sub-nav').appendTo('.main-header__nav-wrap .container .row');
        $('.main-header__nav-wrap-inner').appendTo('.main-header__nav-wrap .container .row');
        $('.main-header__user').appendTo('.main-header__nav-wrap-inner');

        if (!textOverflowArr.length) {
            $('.js-catalog-item-desc').each(function () {
                let dot = new Dotdotdot($(this)[0], {
                    height: 75
                });
                textOverflowArr.push(dot);

            });

            $('.js-catalog-item-title').each(function () {
                let dot = new Dotdotdot($(this)[0], {
                    height: 65
                });
                textOverflowArr.push(dot);

            });
        }

    } else if (breakPoint === 'xl') {
        $('.main-header__nav-wrap').removeClass('main-header__nav-wrap--active');
        $('.main-header__toggle').removeClass('animate');
        $('.geo').appendTo('.main-header__top-row');
        $('.main-nav').appendTo('.main-header__top-row');
        $('.main-header__info').prependTo('.main-header__inner');
        $('.main-header__user').insertAfter('.main-header__logo');
        $('.sub-nav').appendTo('.main-header__sub-nav-row .container');

        if (!textOverflowArr.length) {
            $('.js-catalog-item-desc').each(function () {
                let dot = new Dotdotdot($(this)[0], {
                    height: 75
                });
                textOverflowArr.push(dot);
            });

            $('.js-catalog-item-title').each(function () {
                let dot = new Dotdotdot($(this)[0], {
                    height: 65
                });
                textOverflowArr.push(dot);

            });
        }
    }
});

$(window).trigger('resize');


// if ($(window).width() > breakpoint.sm) {
//     $('.geo').appendTo('.main-header__top-row');
//     $('.main-nav').appendTo('.main-header__top-row');
// }
//
// if ($(window).width() > breakpoint.md) {
//     $('.main-header__info').prependTo('.main-header__inner');
//     $('.sub-nav').insertBefore('.main-header__nav-wrap-inner');
// }
//
// if ($(window).width() > breakpoint.lg) {
//     $('.sub-nav').appendTo('.main-header__sub-nav-row .container');
//     $('.main-header__user').insertAfter('.main-header__logo');
// }
// $(window).resize(function() {
//     if ($(window).width() > breakpoint.sm) {
//         $('.geo').appendTo('.main-header__top-row');
//         $('.main-nav').appendTo('.main-header__top-row');
//     } else if ($(window).width() <= breakpoint.sm && $(window).width() > breakpoint.xs) {
//         $('.main-nav').insertAfter('.sub-nav');
//         //$('.geo').insertAfter('.main-nav');
//         $('.geo').insertBefore('.sub-nav');
//         $('.main-header__nav-wrap-inner').insertBefore('.sub-nav');
//     }
//     if ($(window).width() > breakpoint.md) {
//         $('.main-header__info').prependTo('.main-header__inner');
//     } else if ($(window).width() <= breakpoint.md && $(window).width() > breakpoint.sm) {
//         $('.main-header__info').prependTo('.main-header__nav-wrap-inner');
//     }
//
//     if ($(window).width() > breakpoint.lg) {
//         $('.sub-nav').appendTo('.main-header__sub-nav-row .container');
//         $('.main-header__user').insertAfter('.main-header__logo');
//         $('.main-header__nav-wrap').removeClass('main-header__nav-wrap--active');
//         $('.main-header__toggle').removeClass('animate');
//     } else if ($(window).width() <= breakpoint.lg && $(window).width() > breakpoint.md) {
//         $('.main-header__user').prependTo('.main-header__nav-wrap-inner');
//         $('.sub-nav').insertBefore('.main-header__nav-wrap-inner');
//     }
// });

function reload_js(src) {
    $('script[src="' + src + '"]').remove();
    $('<script>').attr('src', src).appendTo('body');
}

$('.js-modal-toggle').click(function () {
    let modal = $(this).data('modal');

    $(modal).addClass('_active');
});

$('.modal__close').click(function() {
    $(this).closest('.modal').removeClass('_active');
});

$('.modal').click(function (event) {
    if (!$(event.target).closest('.modal__dialog').length) {
        $(this).removeClass('_active');
    }
});
