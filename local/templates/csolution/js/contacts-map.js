ymaps.ready(init);

var myGeoObjects;



function init() {
    var map = new ymaps.Map("contactsMap", {
        center: [55.76, 37.57],
        zoom: 25,
        controls: []
    }, {
        maxZoom: 25
    });


    $.getJSON('/data/contactsMap.json', function (data) {
        // Создадим объект точек из data.Points
        myGeoObjects = data.features.map(item => {
            return new ymaps.GeoObject({
                geometry: item.geometry,
                properties: item.properties

            }, {
                // Опции.
                // Необходимо указать данный тип макета.
                iconLayout: 'default#image',
                // Своё изображение иконки метки.
                iconImageHref: '/img/marker.svg',
                // Размеры метки.
                iconImageSize: [50, 50],
                // Смещение левого верхнего угла иконки относительно
                // её "ножки" (точки привязки).
                iconImageOffset: [-5, -38]
            });
        })

        // Создадим кластеризатор после получения и добавления точек
        var clusterer = new ymaps.Clusterer({
            preset: 'islands#invertedDarkGreenClusterIcons',
            clusterDisableClickZoom: true,
            clusterBalloonContentLayoutWidth: 800,
            clusterBalloonLeftColumnWidth: 160
        });
        clusterer.add(myGeoObjects);
        map.geoObjects.add(clusterer);
        map.setBounds(clusterer.getBounds(), {
            checkZoomRange: false
        });
        if (myGeoObjects.length < 2) {
            map.setBounds(myGeoObjects[0].geometry.getBounds(), {
                checkZoomRange: true
            });
        }
    });

}