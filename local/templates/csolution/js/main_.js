// Скрипты кликов и типичных событий

// Выпадашка мобильного меню
let mobileMenuToggle = $('.main-header--mobile .main-header__toggle');
let mobileMenuWrap = $('.main-header--mobile .main-header__nav-wrap');
let tabletMenuToggle = $('.main-header--tablet .main-header__toggle');
let tabletMenuWrap = $('.main-header--tablet .main-header__nav-wrap');



showBlock('main-header__nav-wrap', mobileMenuWrap, mobileMenuToggle);
showBlock('main-header__nav-wrap', tabletMenuWrap, tabletMenuToggle);

function showBlock(className, block, toggle) {
    toggle.click( function() {
        block.toggleClass(className + '--active');
        toggle.toggleClass('animate');
    });
}

//

$('.order__radio-label').on('click', function () {
    let type = $(this).attr('for');
    let price = $('.js-amount-total-price').data('price');

    if(type == 'pickup') {
        $('.js-amount-total-price').html((price * 0.9).toLocaleString('ru'));
        $('.product-price').html((price * 0.9).toLocaleString('ru'));
        $('.order__amount-text').hide();
        $('.js-amount-type').addClass('toggle_pickup');
    } else {
        $('.js-amount-total-price').html(price.toLocaleString('ru'));
        $('.product-price').html(price.toLocaleString('ru'));
        $('.order__amount-text').show();
        $('.js-amount-type').removeClass('toggle_pickup');

    }

});



// Слайдеры 

$('.slider').owlCarousel(
    {
        loop: true,
        dots: true,
        nav: false,
        dotsClass: 'dots-wrap',
        dotClass: 'dot',
        items: 1,
        autoHeight: true,
        autoplay:false,
        autoplayTimeout:10000,
    }
);

$('.sales__slider').owlCarousel({
    loop: true,
    dots: true,
    nav: false,
    dotsClass: 'dots-wrap',
    dotClass: 'dot',
    margin: 10,
    responsive: {
        0: {
            items: 1
        },
        1200: {
            items: 2
        }

    }
});

$('.life__slider').owlCarousel({
    loop: true,
    dots: false,
    nav: false,
    dotsClass: 'dots-wrap',
    dotClass: 'dot',
    margin: 10,
    responsive: {
        0: {
            items: 1
        },
        768: {
            items: 2
        },
        1200: {
            items: 4,
            margin: 25
        }

    }
});

if($('.product__slider').find('.product__slider-item').length === 3) {
    $('.product__slider').slick({
        slidesToShow: 1,
        arrows: false,
        slidesToScroll: 1,
        dots: false,
        dotsClass: 'dots-wrap',

        responsive: [{
            breakpoint: 1200,
            settings: {
                dots: true
            }
        }]
    });

    $('.product__slider-nav-item').each(function() {
        $(this).click(function(e) {
            e.preventDefault();
            let slideIndex = $(this).index();
            // $('.product__slider').slickGoTo(parseInt(slideIndex));
            $('.product__slider').slick('slickGoTo', parseInt(slideIndex));
        });
    });
} else {
    $('.product__slider').slick({
        slidesToShow: 1,
        arrows: false,
        slidesToScroll: 1,
        dots: false,
        dotsClass: 'dots-wrap',
        asNavFor: '.product__slider-nav',

        responsive: [{
            breakpoint: 1200,
            settings: {
                dots: true
            }
        }]
    });

    $('.product__slider-nav').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.product__slider',
        arrows: false,
        dots: false,
        centerMode: true,
        focusOnSelect: true,
    });
}

// Смена количества в карточке товара


$('.product__counter-toggle').on('click', function () {
    let $this = $(this),
        $countElem = $($this.closest('.product__counter-wrap')).find('.product__counter-count');

    let singlePrice = $('.product-single__prices');
    let price = parseInt($('.product-single__prices').data('price'));
    let count = +$countElem.val();

    if ($this.hasClass('js-counter-minus') && count > 1) {
        count--;
    } else if ($this.hasClass('js-counter-plus')) {
        count++;
    } else {
        alert('что-то пошло не так...');
        return;
    }
    $countElem.val(count);

    let price_format = price * count;
    singlePrice.html(price_format.toLocaleString('ru'));
    console.log(price);

});



// Аккордеон

let orderItems = $('.settings-history__item');

orderItems.each(function() {
    console.log('op');
    let toggle = $(this).children('.js-accordeon-toggle');
    console.log(toggle);
    toggle.click(function() {
        let list = $(this).closest('.settings-history__item').find('.settings-history__product-list');
        if(list.is(':hidden')) {
            list.slideDown();
            $(this).toggleClass('settings-history__item-toggle--close');
        } else {
            list.slideUp();
            $(this).toggleClass('settings-history__item-toggle--close');
        }
    });
});

// Промокод
$('input[name="promocode"]').keyup(function () {
    let data = $('input[name="promocode"]').val();

    if ($(this).hasClass("js-input-promocode-submit--error")) {
        $('input[name="promocode"]').val("");
        $(".input-promocode-submit").removeClass("input-promocode-submit--error");
        $(".input-promocode-wrap").removeClass("input-promocode-wrap--error");
        $(".input-promocode-submit-text").show();
        $(".input-promocode-submit").removeClass("input-promocode-submit--ok");
        $(".input-promocode-submit").removeClass("js-input-promocode-submit--error");
    } else {
        if (data === "test") {
            $(".input-promocode-submit").removeClass("input-promocode-submit--error");
            $(".input-promocode-submit").removeClass("js-input-promocode-submit--error");
            $(".input-promocode-submit-text").hide();
            $(".input-promocode-submit").addClass("input-promocode-submit--ok");
            $(".input-promocode-wrap").removeClass("input-promocode-wrap--error");

        } else {
            $(".input-promocode-submit").removeClass("input-promocode-submit--ok");
            $(".input-promocode-wrap").addClass("input-promocode-wrap--error");
            $(".input-promocode-submit-text").hide();
            $(".input-promocode-submit").addClass("input-promocode-submit--error");
            $(".input-promocode-submit").addClass("js-input-promocode-submit--error");
        }
    }
});

//Маска на телефон
$(".phone").inputmask("+7 (999) 999-99-99");

const delivery = $('.order__form-inner');
const deliveryRadio = delivery.find('.order__form-tabs-inner input');
const defaultDeliveryindow = delivery.find('.order__form-delivery-wrap');
const pickupDeliveryindow = delivery.find('.order__form-pickup-wrap');


deliveryRadio.on('change', function () {
    var th = $(this);

    if (th.attr("data-val") === 'pickup') {
        defaultDeliveryindow.addClass('minimized');
        pickupDeliveryindow.removeClass('minimized');
    } else if (th.attr("data-val") === 'delivery') {
        defaultDeliveryindow.removeClass('minimized');
        pickupDeliveryindow.addClass('minimized');
    }
});


// Переключение таглов

let toggles = $('.js-toggles-wrap');

toggles.each(function() {
    let thinToggle = $(this).children('.js-thin-toggle');
    let fatToggle = $(this).children('.js-fat-toggle');

    let thinPrice = $(this).parents('.js-toggles-parent').find('.js-price-wrap').find('.js-thin-price');
    let fatPrice = $(this).parents('.js-toggles-parent').find('.js-price-wrap').find('.js-fat-price');

    thinToggle.click(function(event) {
        $(this).prop('disabled', !$(this).prop('disabled'));
        fatToggle.removeAttr('disabled');
        $(this).toggleClass('toggle-active');
        fatToggle.toggleClass('toggle-active');
        thinPrice.show();
        fatPrice.hide();
    });

    fatToggle.click(function() {
        $(this).prop('disabled', !$(this).prop('disabled'));
        thinToggle.removeAttr('disabled');
        $(this).toggleClass('toggle-active');
        thinToggle.toggleClass('toggle-active');
        fatPrice.show();
        thinPrice.hide();
    });
});

//Кнопки поделиться на страница карточки товара

const Share = {
    vk: function (purl, ptitle, pimg, text) {
        var url = 'http://vkontakte.ru/share.php?';
        url += 'url=' + encodeURIComponent(purl);
        url += '&title=' + encodeURIComponent(ptitle);
        url += '&description=' + encodeURIComponent(text);
        url += '&image=' + encodeURIComponent(pimg);
        url += '&noparse=true';
        Share.open(url);
    },
    facebook: function (purl, ptitle, pimg, text) {
        var url = 'https://www.facebook.com/sharer.php?s=100';
        url += '&p[title]=' + encodeURIComponent(ptitle);
        url += '&p[summary]=' + encodeURIComponent(text);
        url += '&p[url]=' + encodeURIComponent(purl);
        url += '&p[images][0]=' + encodeURIComponent(pimg);
        Share.open(url);
    },
    ok: function (purl, ptitle, pimg) {
        var url = 'https://connect.ok.ru/offer?';
        url += 'url=' + encodeURIComponent(purl);
        url += '&title=' + encodeURIComponent(ptitle);
        url += '&imageUrl=' + encodeURIComponent(pimg);
        Share.open(url);
    },
    open: function (url) {
        window.open(url, '_blank');
    }
};

var $shareBtn = $('.product__share');
$shareBtn.on('click', function () {
    var $this = $(this);
    if ($this.hasClass('product__share--active')) {
        $this.removeClass('product__share--active').find('.js-product-share-dropdown').fadeOut(300);
    } else {
        $this.addClass('product__share--active').find('.js-product-share-dropdown').fadeIn(300);
    }
});

$(".js-share").on('click', function (e) {
    e.preventDefault();

    Share[$(this).data('target')]($(this).data('url'), $(this).data('title'), $(
        this).data('img'), $(this).data('text'));
});

// City Select

$('.geo__select').on('change', function () {

    let element = $(this).children('option:selected');
    let city = element.val();
    let request = element.data('request');


    $.cookie('currCity', city, { expires: 31, path: '/', domain: '.mangodress.ru' });

    console.log(city);

    if(city == 'main')
        return window.location.href = 'http://mangodress.ru' + request == undefined ? '' : request;
    else
        return window.location.href = 'http://' + city + '.mangodress.ru' + request == undefined ? '' : request;
});

$('.order__form').keypress(function (event) {
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if (keycode == '13') {
        event.preventDefault();
    }
});

// sticky header
let subnav = $('.js-sticky-subnav');
if($(window).width() >= 1200) {


    subnav.sticky({
        zIndex: 20,
        topSpacing: -40,
    });

    subnav.on('sticky-start', function() {
        subnav.addClass('_sticky');
        $('.sub-nav').addClass('_sticky');
    });
    subnav.on('sticky-end', function () {
        subnav.removeClass('_sticky');
        $('.sub-nav').removeClass('_sticky');
    })
} else {
    subnav.unstick();
}