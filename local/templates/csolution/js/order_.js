$('.js-select-default').mainSelect('main-select','Выберите способ оплаты');
$('.js-select-default-2').mainSelect('main-select', 'Выберите способ оплаты');

$('.js-select-date').mainSelect('main-select', 'Выберите время');
$('.js-select-date-2').mainSelect('main-select', 'Выберите время');

//Маска на телефон
$(".phone").inputmask("+7 (999) 999-99-99");

// datepicker for date on order page

let options = {
    language: "ru",
    orientation: "bottom left",
    format: 'dd.mm',
    autoclose: true,
    startDate: new Date(),
    endDate: addDays(30)
}



$('.js-datepicker').each(function () {
    $(this).datepicker(options);
});

function addDays(days) {
    var result = new Date();
    result.setDate(result.getDate() + days);
    return result;
}