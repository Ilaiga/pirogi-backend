(function( $ ) {
  $.fn.selectbox = function() {
    
    // начальные параметры
    // задаем стандартную высоту div'a. 
    var selectDefaultHeight = $('.js-catalog-select').height();
    // угол поворота изображения в div'e 
    var rotateDefault = "rotate(0deg)";

    var firstElement = $('li.item:first > a.js-catalog-select-link').text();
    $('li.item:first').hide();

    $('.js-catalog-select > p.valueTag').html(firstElement);
   
        // после нажатия кнопки срабатывает функция, в которой 
        // вычисляется исходная высота нашего div'a. 
        // очень удобно для сравнения с входящими параметрами (то, что задается в начале скрипта) 
        $('.js-catalog-select > p.valueTag').click(function() {
          $('.js-catalog-select-list').slideDown();
          // вычисление высоты объекта методом height() 
          // var currentHeight = $('.js-catalog-select').height();
          // проверка условия на совпадение/не совпадение с заданной высотой вначале,
          // чтобы понять. что делать дальше. 
          // if (currentHeight < 100 || currentHeight == selectDefaultHeight) {
              // если высота блока не менялась и равна высоте, заданной по умолчанию,
              // тогда мы открываем список и выбираем нужный элемент.
              // $('.js-catalog-select').height("250px");  // «точка остановки анимации»
              // здесь стилизуем нашу стрелку и делаем анимацию средствами CSS3 
            $('.js-catalog-select > svg.icon-svg').css({borderRadius: "1000px", transition: ".2s", transform: "rotate(180deg)"});
          // }


         // иначе если список развернут (высота больше или равна 250 пикселям), 
         // то при нажатии на абзац с классом valueTag, сворачиваем наш список и
         // и присваиваем блоку первоначальную высоту + поворот стрелки в начальное положение
          // if (currentHeight >= 250) {
          //   $('.js-catalog-select').height(selectDefaultHeight);
          //   $('.js-catalog-select > svg.icon-svg').css({
          //     transform: rotateDefault
          //   });
          // }
      });

     // так же сворачиваем список при выборе нужного элемента 
     // и меняем текст абзаца на текст элемента в списке
      $('li.item').click(function() {
        var thisItem = $(this);
        setTimeout(function() {
          $('li.item').each(function () {
            $(this).show();
          });
          thisItem.hide();
        }, 500);
        $('.js-catalog-select-list').slideUp();
        $('.js-catalog-select > svg.icon-svg').css({
          transform: rotateDefault
        });
        $('p.valueTag').text($(this).text());
      });
  };
})( jQuery );