<?
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");

class angerro_yadelivery_db {

    var $db;

    function angerro_yadelivery_db(){
        global $DB;
        $this->db = $DB;
    }

    /*
     * ФУНКЦИИ ДЛЯ АДМИНКИ
     */

    public function install_main_table()
    {
        $this->db->Query("CREATE TABLE IF NOT EXISTS angerro_yadelivery(
        id INTEGER NOT NULL auto_increment,
        name VARCHAR(256) NOT NULL,
        data mediumtext NOT NULL,
        PRIMARY KEY(id))
        ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;");
    }

    public function add_demo_data()
    {
        $example_data = '{"map":{"zoom":12,"center":[59.93011694890914,30.318256677246065],"map_id":"Карта с зонами доставки"},"type":"FeatureCollection","features":[{"type":"Feature","id":0,"geometry":{"type":"Polygon","coordinates":[[[59.94424375640625,30.220409692382802],[59.946310587663305,30.3519023071289],[59.91701841808011,30.247875512695284],[59.91512214826709,30.237575830078114],[59.94424375640625,30.220409692382802]]]},"options":{"strokeColor":"#ffd100","fillColor":"#ffd100"},"properties":{"title":"Зона доставки 1","minDelivery":"0","deliveryPrice":"0"}}]}';

        $format_string = sprintf("INSERT INTO angerro_yadelivery (name, data) values('%s', '%s')", GetMessage("DEF_MAP_NAME"), $example_data);
		$this->db->Query($format_string);

        //$this->db->Query('INSERT INTO angerro_yadelivery (name, data) values("'.GetMessage("DEF_MAP_NAME").'", "' . $example_data . '")');
    }

    public function unistall_main_table()
    {
        $this->db->Query("DROP TABLE IF EXISTS angerro_yadelivery");
    }

    public function get_map_list()
    {
        $ret = array();
        $result = $this->db->Query("SELECT id, name FROM angerro_yadelivery");
        while ($data = $result->GetNext()){
            $ret[] = $data;
        }
        return $ret;
    }

    public function get_data_by_map_id($id)
    {
        $ret = array();
        $ret = $this->db->Query('SELECT data FROM angerro_yadelivery WHERE id='.$id)->GetNext();
        return $ret['~data'];
    }

}
?>