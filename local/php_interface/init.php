<?php

include_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/wsrubi.smtp/classes/general/wsrubismtp.php");
require_once $_SERVER['DOCUMENT_ROOT'].'/local/vendor/autoload.php';

use \Bitrix\Main\Service\GeoIp;
define('IBLOCK_CITIES', 5);
define('CITY_URL', 'vot-takie-pirogi.ru');

session_start();

CModule::IncludeModule("iblock");


if(!isset($_COOKIE['currCity'])) {
    $ipAddress = GeoIp\Manager::getRealIp();

    $city = GeoIp\Manager::getDataResult($ipAddress, "ru", array('cityName'));
    if($city)
	    $result = $city->getGeoData();
    else $result = null;
}



$parsedUrl = parse_url($_SERVER['SERVER_NAME']);
$host = explode('.', isset($parsedUrl['path']) ? $parsedUrl['path'] : $parsedUrl['host']);
$subdomain = $host[0];




if($_COOKIE['currCity'] == 'main' && $_SERVER['SERVER_NAME'] != CITY_URL) {
	header('Location: //'. CITY_URL . $_SERVER['REQUEST_URI']);
	exit;
}
else if($_COOKIE['currCity'] != $subdomain && $_COOKIE['currCity'] != 'main' && strlen($_COOKIE['currCity']) >= 3) {
	header('Location: //' . $_COOKIE['currCity'] . '.' . CITY_URL . $_SERVER['REQUEST_URI']);
	exit;
}
else if(isset($result) && !empty($result->cityName) && !isset($_COOKIE['currCity'])) {

	$arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_*");
	$arFilter = Array("IBLOCK_ID" => IBLOCK_CITIES, "=NAME" => $result->cityName, "ACTIVE" => "Y");
	$res = CIBlockElement::GetList(Array(), $arFilter, false, array(), $arSelect);
	if ($ob = $res->GetNextElement()) {
		$property = $ob->GetProperties();
		if($result->cityName == 'Новосибирск') {
			header('Location: //'. CITY_URL . $_SERVER['REQUEST_URI']);
			setcookie('currCity', 'main',  time()+60*60*24*30, '/', '.'.$_SERVER['HTTP_HOST']);
			exit;
		}
		else if ($subdomain != $property['DOMAIN_NAME']['VALUE']) {
			setcookie('currCity', $property['DOMAIN_NAME']['VALUE'],  time()+60*60*24*30, '/', '.'.CITY_URL);
			header('Location: //' . $property['DOMAIN_NAME']['VALUE'] . '.' . CITY_URL.$_SERVER['REQUEST_URI']);
			exit;
		}
	}
}

if(!isset($_COOKIE['currCity'])) {
	setcookie('currCity', $_SESSION['GET_CITY']['DOMAIN_NAME']['VALUE'],  time()+60*60*24*30, '/', '.'.CITY_URL);
}









// CLASS LABEL
AddEventHandler("iblock", 'OnIBlockPropertyBuildList', array('CIPropLabels', 'GetUserTypeDescription'));
AddEventHandler("iblock", 'OnIBlockPropertyBuildList', array('CIPropLabelsImage', 'GetUserTypeDescription'));

// INWIDGET INCLUDE

require_once $_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/lib/instagram/InstagramScraper.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/lib/instagram/Unirest.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/lib/instagram/InWidget.php';

// SUBDOMAIN MODULE (PRICE, HEADER, AND ANOTHER)

require_once $_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/lib/GetCity.php';

$city = new GetCity();
$result = $city->getData();
if(!$result) {
    //if(isset($_SESSION['GET_CITY'])) unset($_SESSION['GET_CITY']);
    
    $arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_*", "PROPERTY_SECTIONS.DETAIL_PAGE_URL");
    $arFilter = Array("IBLOCK_ID" => 5, "=CODE" => "MAIN", "ACTIVE" => "Y");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, array(), $arSelect);
    if ($ob = $res->GetNextElement()) {
        $property = $ob->GetProperties();
        $params = $ob->GetFields();
    }
	$_SESSION['GET_CITY'] = $property;
    $_SESSION['GET_CITY']['ID']['VALUE'] = $params['ID'];
    $_SESSION['GET_CITY']['NAME']['VALUE'] = $params['NAME'];
}
else
	$_SESSION['GET_CITY'] = $result;

if(!empty($_SESSION['GET_CITY']['TIMEZONE']['VALUE']) && isset($_SESSION['GET_CITY']['TIMEZONE']['VALUE'])) {
	date_default_timezone_set($_SESSION['GET_CITY']['TIMEZONE']['VALUE']);
}



/*
if($_SERVER['REMOTE_ADDR'] == '79.104.198.76') {
	echo debug($_SESSION['GET_CITY']);
	die;
}*/



// LOGIN EMAIL

AddEventHandler("main", "OnBeforeUserRegister", "OnBeforeUserUpdateHandler");
AddEventHandler("main", "OnBeforeUserUpdate", "OnBeforeUserUpdateHandler");
function OnBeforeUserUpdateHandler(&$arFields)
{
	$arFields["LOGIN"] = $arFields["EMAIL"];
	return $arFields;
}



class CIPropLabelsImage {

	public static function GetUserTypeDescription()
	{
		return array(
			"PROPERTY_TYPE" => "S",
			'USER_TYPE' => 'LabelPie',
			'DESCRIPTION' => '[CSOLUTIONS] Пироги (Лейблы)',
			'GetPropertyFieldHtml' => array(__CLASS__, 'GetPropertyFieldHtml'),
		);
	}

	public static function buildSelect($name, $value) {
		ob_start();

		$labels = [
			'empty' => 'Нет',
			'mark--sweet' => 'Красный',
			'mark--much-meat' => 'Синий',
			'mark--hit' => 'Желтый'
		];
	    ?>
            <select name = "<?=$name?>">
                <?foreach($labels as $key => $item):?>
                    <option value="<?=htmlspecialcharsbx($key)?>" <?=$key == $value ? 'selected' : ''?> ><?=$item?></option>
                <?endforeach;?>
            </select>
		<?
		$HTML = ob_get_clean();
		return $HTML;

    }

	public static function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
	{
		$HTML = self::buildSelect($strHTMLControlName['DESCRIPTION'], $value['DESCRIPTION']);
		$HTML .= '<input type = "text" placeholder="Название скидки" name = "'.$strHTMLControlName['VALUE'].'" value="' . htmlspecialcharsbx($value['VALUE']) . '">';
		return $HTML;
	}

}


class CIPropLabels
{
	public static function GetUserTypeDescription()
	{
		return array(
			"PROPERTY_TYPE" => "S",
			'USER_TYPE' => 'LabelWithColor',
			'DESCRIPTION' => '[CUSTOM] ColorPicker (Labels)',
			'GetPropertyFieldHtml' => array(__CLASS__, 'GetPropertyFieldHtml'),
		);
	}
	public static function getEditHTML($name, $value)
	{
		$pathToLib = "/local/modules/csolution.colorpicker/";
		$uid = 'x' . uniqid();
		$dom_id = $uid;
		CJSCore::Init(array("jquery"));
		ob_start();
		?>
        <link type="text/css" rel="stylesheet" href="<?= $pathToLib ?>css/jPicker-1.1.6.css"/>
        <script type='text/javascript' src='<?= $pathToLib ?>jpicker-1.1.6.js'></script>

        <input type="text" id = "element<?= $dom_id ?>" name="<?=$name?>" value="<?=htmlspecialcharsbx($value)?>"/>
        <script>
            $(document).ready(function () {
                $('#element<?= $dom_id ?>').jPicker({
                    images:
                        {
                            clientPath: '<?= $pathToLib ?>images/'
                        },
                    localization:
                        {
                            text:
                                {
                                    title: 'JSPicker',
                                    newColor: 'Новый цвет',
                                    currentColor: 'Текущий цвет',
                                    ok: 'Применить',
                                    cancel: 'Закрыть'
                                },
                        }
                });
            });
        </script>
		<?
		$HTML = ob_get_clean();
		return $HTML;
	}
	public static function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
	{
		$HTML = '<input type = "text" placeholder="Название скидки" name = "'.$strHTMLControlName['VALUE'].'" value="' . htmlspecialcharsbx($value['VALUE']) . '">';
		$HTML .= self::getEditHTML($strHTMLControlName['DESCRIPTION'], $value['DESCRIPTION']);
		return $HTML;
	}
}


class inWidget
{

	private $inWidget = null;

	public function __construct()
	{

		try {
			$config = array(
				'LOGIN' => $_SESSION['GET_CITY']['INSTAGRAM']['VALUE'] ?: 'vot_takie_pirogi_24',
				'HASHTAG' => '',
				'ACCESS_TOKEN' => '',
				'authLogin' => '',
				'authPassword' => '',
				'tagsBannedLogins' => '',
				'tagsFromAccountOnly' => false,
				'imgRandom' => false,
				'imgCount' => 10,
				'cacheExpiration' => 6,
				'cacheSkip' => false,
				'cachePath' => $_SERVER['DOCUMENT_ROOT'] . '/local/cache/',
				'skinDefault' => 'default',
				'skinPath' => 'skins/',
				'langDefault' => 'ru',
				'langAuto' => false,
				'langPath' => $_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/lib/instagram/langs/',
			);
			$this->inWidget = new \inWidget\Core($config);
			$this->inWidget->getData();
		} catch (\Exception $e) {
			echo $e->getMessage();
		}
	}

	public function getData()
	{
		return $this->inWidget;
	}
}

class MyBasketClass
{

	public function BasketInit()
	{
		$price = $_SESSION['GET_CITY']['CODE_PRICE']['VALUE'] ?: 'BASE_PRICE';
		$productCount = $totalCount = $totalSum = $totalSumDiscount = 0;
		if (CModule::IncludeModule("sale") && CModule::IncludeModule("currency")) {
			$dbBaket = CSaleBasket::GetList(
				array("NAME" => "ASC"),
				array("FUSER_ID" => CSaleBasket::GetBasketUserID(), "LID" => SITE_ID, "ORDER_ID" => "NULL")
			);
			while ($arBasket = $dbBaket->GetNext()) {

				if ($arBasket["DELAY"] == "N" && $arBasket["CAN_BUY"] == "Y") {
					$totalSum += (int)$arBasket['PRICE'] * $arBasket['QUANTITY'];
				}
			}
			return [
                "ELEMENTS" => $dbBaket->SelectedRowsCount(),
				"TOTAL_SUM_FORMATED" => $totalSum ? FormatCurrency($totalSum, "RUB") : false,
			];
		}

	}
}

function format_price($value)
{
	if ($value > 0) {
		$value = number_format($value, 2, ',', ' ');
		$value = str_replace(',00', '', $value);
	}
	return $value;
}

function debug($result)
{
	if (is_array($result) == false) return true;
	return '<pre>' . print_r($result, true) . '</pre>';
}


function getFinalPriceInCurrency($item_id = 0, $sale_currency = 'RUB') {
	CModule::IncludeModule("iblock");
	CModule::IncludeModule("catalog");
	CModule::IncludeModule("sale");
	global $USER;

	$currency_code = 'RUB';



	// Простой товар, без торговых предложений (для количества равному 1)
	$price = CCatalogProduct::GetOptimalPrice($item_id, 1, $USER->GetUserGroupArray(), 'N', array(), SITE_ID);

	//echo debug($price);


	// Получили цену?
	if(!$price || !isset($price['PRICE'])) {
		return false;
	}


	return 0;
	// Нашли скидку сразу
	if(!empty($price['PRICE'])) {

		if($price['DISCOUNT_PRICE'] != 0 && empty($price['DISCOUNT_PRICE']))
			return 0;
		else
			$final_price = $price['DISCOUNT_PRICE'];
	}
	else {
		// Меняем код валюты, если нашли
		if (isset($price['CURRENCY'])) {
			$currency_code = $price['CURRENCY'];
		}
		if (isset($price['PRICE']['CURRENCY'])) {
			$currency_code = $price['PRICE']['CURRENCY'];
		}

		// Получаем итоговую цену
		$final_price = $price['PRICE']['PRICE'];

		// Ищем скидки и пересчитываем цену товара с их учетом
		$arDiscounts = CCatalogDiscount::GetDiscountByProduct($item_id, $USER->GetUserGroupArray(), "N", 2);
		if (is_array($arDiscounts) && sizeof($arDiscounts) > 0) {
			$final_price = CCatalogProduct::CountPriceWithDiscount($final_price, $currency_code, $arDiscounts);
			//echo 'FINAL_PRICE=' . $final_price;
		}
	}

	//echo $final_price;

	return $final_price;

}

AddEventHandler("sale", "OnOrderNewSendEmail", "bxModifySaleMails");
function bxModifySaleMails($orderID, &$eventName, &$arFields)
{
	$arOrder = CSaleOrder::GetByID($orderID);
	$arPaySystem = CSalePaySystem::GetByID($arOrder["PAY_SYSTEM_ID"]);
	$pay_system_name = "";
	if ($arPaySystem) {
		$pay_system_name = $arPaySystem["NAME"];
	}
	$coupon_system_name = "";
	$couponList = \Bitrix\Sale\Internals\OrderCouponsTable::getList(array(
		'select' => array('COUPON'),
		'filter' => array('=ORDER_ID' => $orderID)
	));
	if ($coupon = $couponList->fetch())
    {
		$coupon_system_name = $coupon['COUPON'];
    }

	$arFields["PAY_SYSTEM_NAME"] = $pay_system_name;
	$arFields["COUPON"] = $coupon_system_name ?: '-';
	$arFields["HTTP_HOST"] = $_SERVER['HTTP_HOST'];
    if (!empty($_SESSION['GET_CITY']['EMAIL']['VALUE'])) {
        $arFields["BCC"] = $_SESSION['GET_CITY']['EMAIL']['VALUE'];
    }
}
AddEventHandler("main", "OnBeforeEventAdd", array("MyClass", "OnBeforeEventAddHandler"));
class MyClass
{
	function OnBeforeEventAddHandler(&$event, &$lid, &$arFields)
	{
		$arFields["HTTP_HOST"] = $_SERVER['HTTP_HOST'];
	}
}
