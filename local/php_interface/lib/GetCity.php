<?php

global $_GLOBAL;

class GetCity {

	private $fields = [];

	public function __construct()
	{
		CModule::IncludeModule("iblock");

		$arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_*");
		$arFilter = Array("IBLOCK_ID" => IBLOCK_CITIES, "ACTIVE"=>"Y", "=PROPERTY_ACTIVE_CODE_PRICE_VALUE" => 'Y');
		$res = CIBlockElement::GetList(Array(), $arFilter, false, array(), $arSelect);
		while($ob = $res->GetNextElement())
		{
			$property = $ob->GetProperties();
			$field = $ob->GetFields();
			$this->fields[$property['DOMAIN_NAME']['VALUE']] = $property;
			$this->fields[$property['DOMAIN_NAME']['VALUE']]['NAME']['VALUE'] = $field['NAME'];
			$this->fields[$property['DOMAIN_NAME']['VALUE']]['ID']['VALUE'] = $field['ID'];

		}
	}
	protected function prepareData() {

		$parsedUrl = parse_url($_SERVER['SERVER_NAME']);
		$host = explode('.', isset($parsedUrl['path']) ? $parsedUrl['path'] : $parsedUrl['host']);
		$subdomain = $host[0];
		return isset($this->fields[$subdomain]) ? $this->fields[$subdomain] : false;
	}

	public function getData() {

		$result = $this->prepareData();
		return $result;
		/*unset($_SESSION['GET_CITY']);
		$result = $this->prepareData();
		if(!$result)
			return false;
		else
			$_SESSION['GET_CITY'] = $result;

		echo '<pre>'.print_r($_SESSION, true).'</pre>';*/
	}


}