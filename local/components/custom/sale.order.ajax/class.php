<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}


/* Bitrix Order | PirogiSite */

use Bitrix\Main,
	Bitrix\Main\Loader,
	Bitrix\Main\Config\Option,
	Bitrix\Main\Web\Json,
	Bitrix\Main\Localization\Loc,
	Bitrix\Sale,
	Bitrix\Sale\Order,
	Bitrix\Sale\PersonType,
	Bitrix\Sale\Shipment,
	Bitrix\Sale\PaySystem,
	Bitrix\Sale\Payment,
	Bitrix\Sale\Delivery,
	Bitrix\Sale\Location\LocationTable,
	Bitrix\Sale\Result,
	Bitrix\Sale\DiscountCouponsManager,
	Bitrix\Sale\Services\Company,
	Bitrix\Sale\Location\GeoIp;

class customOrderComponent extends CBitrixComponent
{
	/**
	 * @var \Bitrix\Sale\Order
	 */
	public $order;

	private $userID = null;
	private $fields = array();

	protected $arElementId = array();
	protected $arSku2Parent = array();

	private $deliveryPrice = 0;
	private $minDeliveryPrice = 0;
	private $accessPrice = 0;


	function __construct($component = null)
	{
		parent::__construct($component);
		if (!Loader::includeModule('sale')) {
			$this->errors[] = 'No sale module';
		}
		if (!Loader::includeModule('catalog')) {
			$this->errors[] = 'No catalog module';
		}
	}
	protected function obtainPropertiesForIbElements()
	{
		if (empty($this->arElementId))
		{
			return;
		}
		$arResult =& $this->arResult;
		$arParents = CCatalogSku::getProductList($this->arElementId);
		if (!empty($arParents))
		{
			foreach ($arParents as $productId => $arParent)
			{
				$this->arElementId[] = $arParent["ID"];
				$this->arSku2Parent[$productId] = $arParent["ID"];
			}
		}

		$res = CIBlockElement::GetList(
			array(),
			array("=ID" => array_unique($this->arElementId)),
			false,
			false,
			array("ID", "IBLOCK_ID", "PREVIEW_PICTURE", "DETAIL_PICTURE", "PREVIEW_TEXT", "PROPERTY_*")
		);
		while ($arElement = $res->GetNextElement()) //Update
		{
			$element = $arElement->GetFields();
			$arElementData[$element["IBLOCK_ID"]][] = $element["ID"];
			$arProductData[$element["ID"]] = $arElement->GetFields();
			$arProductDataProperties[$element["ID"]] = $arElement->GetProperties();
			$elementIndex[$element["ID"]] = array();
		}
		foreach ($arElementData as $iBlockId => $arElemId)
		{
			$arCodes = array();
			if (!empty($this->arIblockProps[$iBlockId]))
				$arCodes = array_keys($this->arIblockProps[$iBlockId]);

			$imageCode = $this->arParams['ADDITIONAL_PICT_PROP'][$iBlockId];

			if (!empty($imageCode) && !in_array($imageCode, $arCodes))
				$arCodes[] = $imageCode;

			if (!empty($arCodes))
			{
				CIBlockElement::GetPropertyValuesArray($elementIndex, $iBlockId,
					array("ID" => $arElemId),
					array("CODE" => $arCodes)
				);
			}
		}
		unset($arElementData);

		$arAdditionalImages = array();
		$currentProductProperties = array();
		$basketOrderDay = null;


		foreach ($arResult["BASKET_ITEMS"] as &$arResultItem)
		{
			$productId = $arResultItem["PRODUCT_ID"];
			$arParent = $arParents[$productId];

			// PROPERTY FIELDS
			$arResultItem["PROPERTY"] = $arProductDataProperties[$productId];

			// CHECKING ONE DAY
			if($arProductDataProperties[$productId]['ORDER_DAY']['VALUE'] == 'Y')
				$basketOrderDay = true;


			$itemIblockId = intval($arProductData[$productId]['IBLOCK_ID']);
			$currentProductProperties[$productId] = isset($this->arIblockProps[$itemIblockId])
				? $this->arIblockProps[$itemIblockId]
				: array();

			if (
				(int)$arProductData[$productId]["PREVIEW_PICTURE"] <= 0
				&& (int)$arProductData[$productId]["DETAIL_PICTURE"] <= 0
				&& $arParent
			)
			{
				$productId = $arParent["ID"];
			}
			if ((int)$arProductData[$productId]["PREVIEW_PICTURE"] > 0)
			{
				$arResultItem["PREVIEW_PICTURE"] = $arProductData[$productId]["PREVIEW_PICTURE"];
			}
			if ((int)$arProductData[$productId]["DETAIL_PICTURE"] > 0)
			{
				$arResultItem["DETAIL_PICTURE"] = $arProductData[$productId]["DETAIL_PICTURE"];
			}
			if ($arProductData[$productId]["PREVIEW_TEXT"] != '')
			{
				$arResultItem["PREVIEW_TEXT"] = $arProductData[$productId]["PREVIEW_TEXT"];
				$arResultItem["PREVIEW_TEXT_TYPE"] = $arProductData[$productId]["PREVIEW_TEXT_TYPE"];
			}
			// replace PREVIEW_PICTURE with selected ADDITIONAL_PICT_PROP
			if (
				empty($arProductData[$arResultItem["PRODUCT_ID"]]["PREVIEW_PICTURE"])
				&& empty($arProductData[$arResultItem["PRODUCT_ID"]]["DETAIL_PICTURE"])
				&& $arAdditionalImages[$arResultItem["PRODUCT_ID"]]
			)
			{
				$arResultItem["PREVIEW_PICTURE"] = $arAdditionalImages[$arResultItem["PRODUCT_ID"]];
			}
			elseif (
				empty($arResultItem["PREVIEW_PICTURE"])
				&& empty($arResultItem["DETAIL_PICTURE"])
				&& $arAdditionalImages[$productId]
			)
			{
				$arResultItem["PREVIEW_PICTURE"] = $arAdditionalImages[$productId];
			}
			$arResultItem["PREVIEW_PICTURE_SRC"] = "";
			if (isset($arResultItem["PREVIEW_PICTURE"])
				&& (int)$arResultItem["PREVIEW_PICTURE"] > 0
			) {
				$arImage = CFile::GetFileArray($arResultItem["PREVIEW_PICTURE"]);
				if (!empty($arImage)) {
					self::resizeImage($arResultItem, 'PREVIEW_PICTURE', $arImage,
						array("width" => 320, "height" => 320),
						array("width" => 110, "height" => 110),
						$this->arParams['BASKET_IMAGES_SCALING']
					);
				}
			}
			$arResultItem["DETAIL_PICTURE_SRC"] = "";
			if (isset($arResultItem["DETAIL_PICTURE"])
				&& (int)$arResultItem["DETAIL_PICTURE"] > 0
			)
			{
				$arImage = CFile::GetFileArray($arResultItem["DETAIL_PICTURE"]);
				if (!empty($arImage))
				{
					self::resizeImage($arResultItem, 'DETAIL_PICTURE', $arImage,
						array("width" => 320, "height" => 320),
						array("width" => 110, "height" => 110),
						$this->arParams['BASKET_IMAGES_SCALING']
					);
				}
			}
		}

		$arResult['ORDER_DAY_CART'] = $basketOrderDay ?: false;
		//echo '<pre>'.print_r($this->arElementId, true).'</pre>';
	}







	// Инициализурем тип плательщика

	function onPrepareComponentParams($arParams)
	{
		if (isset($arParams['PERSON_TYPE_ID']) && intval($arParams['PERSON_TYPE_ID']) > 0) {
			$arParams['PERSON_TYPE_ID'] = intval($arParams['PERSON_TYPE_ID']);
		} else {
			if (intval($this->request['PERSON_TYPE_ID']) > 0) {
				$arParams['PERSON_TYPE_ID'] = intval($this->request['PERSON_TYPE_ID']);
			} else {
				$arParams['PERSON_TYPE_ID'] = 1;
			}
		}
		if(!isset($arParams['LANG'])) {
			$arParams['LANG'] = 'RU';
		}

		return $arParams;
	}

	protected function setOrderProps()
	{
		/** @var \Bitrix\Sale\PropertyValue $prop */
		foreach ($this->order->getPropertyCollection() as $propertyItem) {
			$value = '';
			if (empty($value)) {
				foreach ($this->fields as $key => $val) {
					if (strtolower($key) == strtolower($propertyItem->getField('CODE'))) {
						$value = $val;
					}
				}
			}
			if (!empty($value)) {
				$propertyItem->setValue($value);
			}
		}
	}

	protected function createVirtualOrder()
	{


		global $USER;

		try {

			$siteId = \Bitrix\Main\Context::getCurrent()->getSite();
			$basketItems = \Bitrix\Sale\Basket::loadItemsForFUser(
				\CSaleBasket::GetBasketUserID(),
				$siteId
			)
				->getOrderableItems();

			if (count($basketItems) == 0) {
				LocalRedirect('/cart');
			}


			$this->order = \Bitrix\Sale\Order::create($siteId, CUser::IsAuthorized() ? $USER->getID() : \CSaleUser::GetAnonymousUserID());
			$this->order->setPersonTypeId(1);
			$this->order->setBasket($basketItems);


			$delivery_id = $this->request['delivery_id_'.$this->request['delivery']];
			$payment_id = $this->request['payment_id_'.$this->request['delivery']];


			if(!empty($this->request['save']) && $this->request['save'] == 'Y') {



				$this->deliveryWithMap();
				$price = $this->order->getPrice();

				if($price > $this->deliveryPrice) {
					$this->minDeliveryPrice = 0;
				}
				//if($_SERVER['REMOTE_ADDR'] == '37.195.203.64') {
				if($price < $this->accessPrice) {
					$this->addError('Минимальная сумма заказа: '.$this->accessPrice);
				}
				//}

				if (intval($payment_id) > 0) {
					$paymentCollection = $this->order->getPaymentCollection();
					$payment = $paymentCollection->createItem(
						Bitrix\Sale\PaySystem\Manager::getObjectById(
							intval($payment_id)
						)
					);
					$payment->setField("SUM", $price);
					$payment->setField("CURRENCY", $this->order->getCurrency());
					$payment->setField('PAY_SYSTEM_ID', $payment_id);
				}
				$shipmentCollection = $this->order->getShipmentCollection();

				if($_SERVER['REMOTE_ADDR'] == '37.195.203.64') {
					//echo '<pre>'. print_r($this->request, true).'</pre>';
					//die;
				}

				if ($this->request['delivery'] == 1) {
					$shipment = $shipmentCollection->createItem(
						Bitrix\Sale\Delivery\Services\Manager::getObjectById(
							intval($delivery_id)
						)
					);
					$shipment->setField('CURRENCY', $this->order->getCurrency());
					$shipment->setFields(array(
						'DELIVERY_ID' => $delivery_id,
						'CUSTOM_PRICE_DELIVERY' => 'Y',
						'DELIVERY_NAME' => \Bitrix\Sale\Delivery\Services\Manager::getById($delivery_id)['NAME'],
						'CURRENCY' => $this->order->getCurrency(),
						'PRICE_DELIVERY' => !empty($this->minDeliveryPrice) ? $this->minDeliveryPrice : 0,
						'BASE_PRICE_DELIVERY' => !empty($this->minDeliveryPrice) ? $this->minDeliveryPrice : 0
					));
				} else {
					$shipment = $shipmentCollection->createItem();
					$service = Delivery\Services\Manager::getById(Delivery\Services\EmptyDeliveryService::getEmptyDeliveryServiceId());
					$shipment->setFields(array(
						'DELIVERY_ID' => $service['ID'],
						'DELIVERY_NAME' => $service['NAME'],
					));
				}
				$this->order->refreshData();
			}
			if($this->fields['USER_COMMENT']) {
				$this->order->setFieldNoDemand('USER_DESCRIPTION', $this->fields['USER_COMMENT']);
			}



		} catch (\Exception $e) {
			$this->errors[] = $e->getMessage();
		}
	}

	private function register() {
		global $USER;
		$user = new CUser;
		$filter = Array("EMAIL" => $this->request['USER_EMAIL']);
		$rsUsers = CUser::GetList($by="id", $order="asc", $filter);
		if($arUser = $rsUsers->fetch()) {
			$this->userID = $arUser['ID'];
		}
		else {
			$password = randString(8);
			$arFields = Array(
				"NAME"              => $this->request['USER_NAME'],
				"EMAIL"             => $this->request['USER_EMAIL'],
				"LOGIN"             => $this->request['USER_EMAIL'],
				"PHONE"				=> $this->request['USER_PHONE'],
				"LID"               => SITE_ID,
				"ACTIVE"            => "N",
				"GROUP_ID"          => array(2),
				"PASSWORD"          => $password,
				"CONFIRM_PASSWORD"  => $password,
				"CONFIRM_CODE" => randString(8)
			);
			$this->userID = $user->Add($arFields);
			if(intval($this->userID) <= 0) {
				echo $user->LAST_ERROR;
				die;
			}
			$arFields['USER_ID'] = $this->userID;
			$USER->Authorize((int)$this->userID);

			$arEventFields = $arFields;
			$event = new CEvent;
			$event->SendImmediate("NEW_USER_CONFIRM", SITE_ID, $arEventFields);
		}
		$this->order->setFieldNoDemand('USER_ID', CUser::IsAuthorized() ? $USER->getID() : (int)$this->userID);
	}

	private function validate() {
		$this->fields['USER_TIME'] = $this->request['USER_TIME_'.$this->request['delivery']];
		$this->fields['USER_NAME'] = $this->request['USER_NAME'];
		$this->fields['USER_PHONE'] = $this->request['USER_PHONE'];
		$this->fields['USER_EMAIL'] = $this->request['USER_EMAIL'];
		$this->fields['USER_DATE'] = $this->request['USER_DATE_'.$this->request['delivery']];
		$this->fields['USER_COMMENT'] = $this->request['USER_COMMENT_'.$this->request['delivery']];

		$comment = null;
		if($this->request['delivery'] == 2) {
			$comment = '[Самовывоз] '.\Bitrix\Sale\Delivery\Services\Manager::getById($this->request['delivery_id_'.$this->request['delivery']])['NAME'];
		} else {
			$comment = '[Доставка курьером] '.$this->request['USER_ADRESS_'.$this->request['delivery']];
		}
		$this->fields['USER_ADRESS'] = $comment;

		if(isset($this->request['MAP_ERROR']) && $this->request['MAP_ERROR'] == 'Y') {
			$this->addError('Укажите правильный адрес доставки');
		}
		if(isset($this->request['CORRECTADDRESS']) && $this->request['CORRECTADDRESS'] != 'Y' && $this->request['delivery'] != 2) {
			$this->addError('Город, улица, дом введен не корректно');
		}
		if($this->fields['USER_TIME'] == -1) {
			$this->addError('Укажите дату доставки');
		}
		if($this->request['payment_id_'.$this->request['delivery']] == -1) {
			$this->addError('Укажите способ оплаты');
		}

		/*if($_SERVER['REMOTE_ADDR'] == '37.195.203.64') {
			echo debug($this->fields);
			die;
		}*/

	}


	protected function deliveryWithMap() {
		global $DB;
		if($this->request['bitrix_delivery_map']) {

			$format_string = 'SELECT data FROM angerro_yadelivery WHERE id = '.$this->request['bitrix_delivery_map'];
			$query = $DB->query($format_string)->GetNext();
			$dataDelivery = json_decode(html_entity_decode($query['data']), true);

			foreach($dataDelivery['features'] as $feature) {
				if($feature['options']['id'] == $this->request['bitrix_inside_area']) {


					$this->deliveryPrice = $feature['properties']['deliveryPrice'];
					$this->minDeliveryPrice = $feature['properties']['minDelivery'];
					$this->accessPrice = $feature['properties']['accessPrice'];
				}
			}
		}
	}

	private function generateTimeDelivery() {


		if(date('G') < 9)
			$currentTime = '9:00';
		else
			$currentTime = '9:00';//date('H');


		$interval = $currentTime.'-21:00';
		$interval = explode('-', $interval);

		$firstDate = explode(':', $interval[0]);
		$firstDate = (new DateTime())->setTime($firstDate[0], $firstDate[1]);

		$secondDate = explode(':', $interval[1]);
		$secondDate = (new DateTime())->setTime($secondDate[0], $secondDate[1]);

		$this->arResult['TIME'] = [];

		$result = [];
		while ($firstDate < $secondDate) {

			$date = null;
			$time = $firstDate->format('Y-m-d H:i:sP');

			$result[] = $firstDate->format('H:i');
			$date = new DateTime($time);
			$date->modify('+2 hour');
			$result[] = $date->format('H:i');

			$firstDate->modify("+1 hour");
		}
		$this->arResult['TIME'] = $result;
		/*if(date('G') < 9)
			$currentTime = '9:00';
		else
			$currentTime = date('H');


		$interval = $currentTime.'-21:00';
		$interval = explode('-', $interval);

		$firstDate = explode(':', $interval[0]);
		$firstDate = (new DateTime())->setTime($firstDate[0], $firstDate[1]);

		$secondDate = explode(':', $interval[1]);
		$secondDate = (new DateTime())->setTime($secondDate[0], $secondDate[1]);

		$this->arResult['TIME'] = [];

		$result = [];
		while ($firstDate < $secondDate) {

			$date = null;
			$time = $firstDate->format('Y-m-d H:i:sP');

			$result[] = $firstDate->format('H:i');
			$date = new DateTime($time);
			$date->modify('+2 hour');
			$result[] = $date->format('H:i');

			$firstDate->modify("+1 hour");


			//$this->arResult['TIME'][] = $firstDate->format('H:i');
			//$firstDate->modify("+2 hour");
			if($parity >= 2) {
				$parity = 1;
				$firstDate->modify("+1 hour");
				$this->arResult['TIME'][] = $firstDate->format('H:i');
			}
			$firstDate->modify("+2 hour");
			$parity ++;

		}
		$this->arResult['TIME'] = $result;
		*/
	}

	/* Ошибки */
	protected function isHasError() {
		return count($this->arResult['ERRORS']) ? true : false;
	}
	protected function addError($error) {
		if(!empty($error))
			$this->arResult['ERRORS'][] = $error;
	}
	protected function getErrors() {
		return $this->arResult['ERRORS'];
	}

	/* Оплата и Доставка (вывод) */
	protected function getPaymentDelivery() {
		$arResult =& $this->arResult;


		$payment = Payment::create($this->order->getPaymentCollection());
		$paySystemsList = PaySystem\Manager::getListWithRestrictions($payment);

		$arResult['PAY_SYSTEM'] = $paySystemsList;

		
		$delivery = \Bitrix\Sale\Delivery\Services\Manager::getActiveList();
		foreach($delivery as $key => $arDelivery) {
			if($arDelivery['PARENT_ID']) {
				$arDelivery['MAIN_NAME'] = $delivery[$arDelivery['PARENT_ID']]['NAME'];
				$arResult['DELIVERY'][$arDelivery['PARENT_ID']][] = $arDelivery;
			}
		}
	}
	

	private function getCurrentCitySettings() {
		$arResult =& $this->arResult;
		$property = null;
		if($_COOKIE['currCity'] == 'main' || !isset($_COOKIE['currCity'])) {
			$arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_*");
			$arFilter = Array("IBLOCK_ID" => 5, "=CODE" => "MAIN", "ACTIVE" => "Y");
			$res = CIBlockElement::GetList(Array(), $arFilter, false, array(), $arSelect);
			if ($ob = $res->GetNextElement()) {
				$property = $ob->GetProperties();
			}
		}
		$arResult['CURRENT_MAP_ID'] = isset($_SESSION['GET_CITY']['DELIVERY_MAP_ID']['VALUE']) ? $_SESSION['GET_CITY']['DELIVERY_MAP_ID']['VALUE'] : $property['DELIVERY_MAP_ID']['VALUE'];
		$arResult['CURRENT_DELIVERY_ID'] = isset($_SESSION['GET_CITY']['DELIVERY_ID']['VALUE'])  ? $_SESSION['GET_CITY']['DELIVERY_ID']['VALUE'] : $property['DELIVERY_ID']['VALUE'];
	}


	function executeComponent()
	{

		$this->generateTimeDelivery();
		$this->createVirtualOrder();
		$this->obtainBasket();
		$this->obtainPropertiesForIbElements();
		$this->getPaymentDelivery();
		$this->getCurrentCitySettings();

		if(!empty($this->request['save']) && $this->request['save'] == 'Y') {

			$this->validate();
			$this->setOrderProps();


			if(!$this->isHasError()) {


				$this->register();
				$this->order->doFinalAction(true);
				$this->order->save();

				if($_SESSION['TOTAL_SUM_FORMATED']) {
					$_SESSION['TOTAL_SUM_FORMATED'] = null;
					unset($_SESSION['TOTAL_SUM_FORMATED']);
				}
				\Bitrix\Sale\DiscountCouponsManager::clear(true);
				LocalRedirect('/success?ORDERID='.$this->order->getId());
				exit;
			}
			
		}

		$this->includeComponentTemplate();
	}




	// Вспомогательные функции


	protected function obtainBasket() // Определение товаров в корзине
	{
		$arResult =& $this->arResult;

		$arResult["BASKET_ITEMS"] = array();

		$this->calculateBasket = $this->order->getBasket()->createClone();

		$discounts = $this->order->getDiscount();
		$showPrices = $discounts->getShowPrices();
		if (!empty($showPrices['BASKET']))
		{
			foreach ($showPrices['BASKET'] as $basketCode => $data)
			{
				$basketItem = $this->calculateBasket->getItemByBasketCode($basketCode);
				if ($basketItem instanceof Sale\BasketItemBase)
				{
					$basketItem->setFieldNoDemand('BASE_PRICE', $data['SHOW_BASE_PRICE']);
					$basketItem->setFieldNoDemand('PRICE', $data['SHOW_PRICE']);
					$basketItem->setFieldNoDemand('DISCOUNT_PRICE', $data['SHOW_DISCOUNT']);
				}
			}
		}
		unset($showPrices);

		/** @var Sale\BasketItem $basketItem */
		foreach ($this->calculateBasket as $basketItem)
		{
			$arBasketItem = $basketItem->getFieldValues();
			if ($basketItem->getVatRate() > 0)
			{
				$arResult["bUsingVat"] = "Y";
				$arBasketItem["VAT_VALUE"] = $basketItem->getVat();
			}
			$arBasketItem["QUANTITY"] = $basketItem->getQuantity();
			$arBasketItem["PRICE_FORMATED"] = SaleFormatCurrency($basketItem->getPrice(), $this->order->getCurrency());
			$arBasketItem["WEIGHT_FORMATED"] = roundEx(doubleval($basketItem->getWeight()/$arResult["WEIGHT_KOEF"]), SALE_WEIGHT_PRECISION)." ".$arResult["WEIGHT_UNIT"];
			$arBasketItem["DISCOUNT_PRICE"] = $basketItem->getDiscountPrice();

			$arBasketItem["DISCOUNT_PRICE_PERCENT"] = 0;
			if ($arBasketItem['CUSTOM_PRICE'] != 'Y')
			{
				$arBasketItem['DISCOUNT_PRICE_PERCENT'] = Sale\Discount::calculateDiscountPercent(
					$arBasketItem['BASE_PRICE'],
					$arBasketItem['DISCOUNT_PRICE']
				);
				if ($arBasketItem['DISCOUNT_PRICE_PERCENT'] === null)
					$arBasketItem['DISCOUNT_PRICE_PERCENT'] = 0;
			}
			$arBasketItem["DISCOUNT_PRICE_PERCENT_FORMATED"] = $arBasketItem['DISCOUNT_PRICE_PERCENT'].'%';

			$arBasketItem["BASE_PRICE_FORMATED"] = SaleFormatCurrency($basketItem->getBasePrice(), $this->order->getCurrency());


			$arBasketItem["PROPS"] = array();
			/** @var Sale\BasketPropertiesCollection $propertyCollection */
			$propertyCollection = $basketItem->getPropertyCollection();
			$propList = $propertyCollection->getPropertyValues();
			foreach ($propList as $key => &$prop)
			{
				if ($prop['CODE'] == 'CATALOG.XML_ID' || $prop['CODE'] == 'PRODUCT.XML_ID' || $prop['CODE'] == 'SUM_OF_CHARGE')
					continue;

				$prop = array_filter($prop, array("CSaleBasketHelper", "filterFields"));
				$arBasketItem["PROPS"][] = $prop;
			}

			$this->arElementId[] = $arBasketItem["PRODUCT_ID"];
			$arBasketItem["SUM_NUM"] = $basketItem->getPrice() * $basketItem->getQuantity();
			$arBasketItem["SUM"] = SaleFormatCurrency($basketItem->getPrice() * $basketItem->getQuantity(), $this->order->getCurrency());
			$arBasketItem["SUM_BASE"] = $basketItem->getBasePrice() * $basketItem->getQuantity();
			$arBasketItem["SUM_BASE_FORMATED"] = SaleFormatCurrency($basketItem->getBasePrice() * $basketItem->getQuantity(), $this->order->getCurrency());

			$arResult["BASKET_ITEMS"][] = $arBasketItem;
		}
	}

	public static function resizeImage(array &$item, $imageKey, array $arImage, array $sizeAdaptive, array $sizeStandard, $scale = '') // CropImage
	{
		if ($scale == '')
		{
			$scale = 'adaptive';
		}

		if ($scale === 'no_scale')
		{
			$item[$imageKey.'_SRC'] = $arImage['SRC'];
			$item[$imageKey.'_SRC_ORIGINAL'] = $arImage['SRC'];
		}
		elseif ($scale === 'adaptive')
		{
			$arFileTmp = CFile::ResizeImageGet(
				$arImage,
				array('width' => $sizeAdaptive['width'] / 2 , 'height' => $sizeAdaptive['height'] / 2),
				BX_RESIZE_IMAGE_PROPORTIONAL,
				true
			);
			$item[$imageKey.'_SRC'] = $arFileTmp['src'];

			$arFileTmp = CFile::ResizeImageGet(
				$arImage,
				$sizeAdaptive,
				BX_RESIZE_IMAGE_PROPORTIONAL,
				true
			);
			$item[$imageKey.'_SRC_2X'] = $arFileTmp['src'];

			$item[$imageKey.'_SRC_ORIGINAL'] = $arImage['SRC'];
		}
		else
		{
			$arFileTmp = CFile::ResizeImageGet($arImage, $sizeStandard, BX_RESIZE_IMAGE_PROPORTIONAL, true);
			$item[$imageKey.'_SRC'] = $arFileTmp['src'];

			$item[$imageKey.'_SRC_ORIGINAL'] = $arImage['SRC'];
		}
	}


}
