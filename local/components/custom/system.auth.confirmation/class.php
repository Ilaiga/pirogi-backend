<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}
use Bitrix\Main;
use Bitrix\Main\UserTable;

class customConfirmUser extends CBitrixComponent {


	private $user = null;
	public $output = null;

	private function validate() {
		if(!$this->request['confirm_user_id'])
			LocalRedirect('/');

		$stdObject = (object) [
			'code' => $this->user['CONFIRM_CODE'],
			'login' => $this->user['LOGIN'],
			'code_print' => $this->user['CONFIRM_CODE'] == $this->request['confirm_code'] ? true : false,
			'errors' => [
				'USER_ACTIVE' => $this->user['ACTIVE'] == 'Y' ? 'Y' : 'N',
			]
		];
		$this->arResult['objectResult'] = $stdObject;
	}

	private function password() {
		return $this->request['USER_PASSWORD'] == $this->request['USER_CONFIRM_PASSWORD'] ? true : false;
	}

	function executeComponent() {

		$this->user = (CUser::GetByID($this->request['confirm_user_id']))->Fetch();
		if(!$this->user) {
			global $APPLICATION;

			include($_SERVER['DOCUMENT_ROOT']."/404.php");

			return 1;
		}


		self::validate();

		if($this->request['Confirm']) {
			global $USER;
			$user = new CUser;

			if(!$this->arResult['objectResult']->code_print) { //Код подтверждения был ошибочный
				if($this->user['CONFIRM_CODE'] == trim($this->request['USER_CONFIRMCODE']) && $this->password()) {
					$user->Update($this->user['ID'], [
						'PASSWORD' => $this->request['USER_PASSWORD'],
						'CONFIRM_PASSWORD' => $this->request['USER_PASSWORD'],
						"ACTIVE" => "Y",
					]);
					$this->arResult['SUCCESS'] = 'Y';
					$USER->Authorize($this->user['ID']);
				}
				else {
					$this->arResult['ERRORS'][] = 'Не правильное подтверждение пароля/Не верный код подтверждения';
				}

			} else { // Код подтверждения сразу был правильный

				if($this->password()) {
					$user->Update($this->user['ID'], [
						'PASSWORD' => $this->request['USER_PASSWORD'],
						'CONFIRM_PASSWORD' => $this->request['USER_PASSWORD'],
						"ACTIVE" => "Y",
					]);
					$USER->Authorize($this->user['ID']);
					$this->arResult['SUCCESS'] = 'Y';
				}
				else {
					$this->arResult['ERRORS'][] = 'Не правильное подтверждение пароля';
				}
			}
		}
		$this->includeComponentTemplate();
	}

}

?>