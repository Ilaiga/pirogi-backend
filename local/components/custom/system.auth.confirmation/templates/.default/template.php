<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}
/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @var array $arParams
 * @var array $arResult
 */


//echo debug($arResult);

?>


<?if($arResult["objectResult"]->errors['USER_ACTIVE'] == 'Y'):?>
    <section class="login">
        <div class="container container--helper">
            <h1 class="title title__form">Аккаунт подтвержден (err)</h1>
        </div>
    </section>
<?else:?>

    <?=debug($arResult['ERRORS'])?>

    <?if($arResult['SUCCESS'] == 'Y'):?>
        <p style="color: green">Пользователь успешно активирован!</p>
    <?else:?>
        <section class="login">
            <div class="container container--helper">
                <h1 class="title title__form">Подтверждение аккаунта</h1>
                <form class="form" method="post" action="" name="bform" enctype="multipart/form-data">


                    <div class="input__inner input__inner--required">
                        <input class="input" name="USER_CONFIRMCODE" type="text" value="<?=$arResult["objectResult"]->code_print == 1 ? $arResult["objectResult"]->code : ''?>" placeholder="Код подтверждения" required="required"/>
                    </div>
                    <div class="input__inner input__inner--required">
                        <input class="input" name="USER_EMAIL" type="email" value="<?=$arResult["objectResult"]->login?>" placeholder="Email" required="required"/>
                    </div>
                    <div class="input__inner input__inner--required">
                        <input class="input" name="USER_PASSWORD" type="password" value="<?=$arResult["USER_PASSWORD"]?>" placeholder="Пароль" required="required"/>
                    </div>
                    <div class="input__inner input__inner--required">
                        <input class="input" name="USER_CONFIRM_PASSWORD" value="<?=$arResult["USER_CONFIRM_PASSWORD"]?>" type="password" placeholder="Повторите пароль" required="required"/>
                    </div>
                    <input type = "submit" class="btn sub-btn" name="Confirm" value="<?=GetMessage("CT_BSAC_CONFIRM")?>">
                </form>
            </div>
        </section>
    <?endif;?>
<?endif;?>


