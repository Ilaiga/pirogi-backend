<section class="subscribe">

	<div class="container" <?=$arResult['MESSAGE'] ? 'style = "display: none"' : ''?>>
        <h2 class="form__title h2">Узнавай первым!</h2>
		<p class="form__desc">Получай на почту информацию о новых акциях!</p>
		<form class="form" method="POST" action="<?=$arResult["FORM_ACTION"]?>">
			<?=bitrix_sessid_post()?>
            
			<input class="input" name="SENDER_SUBSCRIBE_EMAIL" type="email" placeholder="Ваша почта" required="required"/>
			<input type="hidden" name="Save" value="Y">

			<div class="form__wrapper">
				<button type="submit" class="btn btn--accept subscribe__btn">Подписаться!
					<svg class="icon-svg">
						<use xlink:href="#icon-mail"></use>
					</svg>
				</button>
			</div>
		</form>
	</div>
    <?if($arResult['MESSAGE']):?>
        <div class="subscribe--thanks js-form-helper" style="display: block">
            <div class="container"><h2 class="form__title"><?=$arResult['MESSAGE']?></h2>
                <p class="form__desc">Мы будем оповещать Вас о самых выгодных акциях и предложениях</p>
            </div>
        </div>
    <?endif;?>
</section>

