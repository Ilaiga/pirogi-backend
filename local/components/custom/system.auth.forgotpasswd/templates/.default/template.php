<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?

if($component->message)
    echo $component->message;
?>


<section class="login">
    <div class="container container--helper">
        <h1 class="title title__form">Забыли пароль?</h1>
        <form class="form" name="bform" method="post" target="_top" action="">
            <div class="input__inner input__inner--required">
                <input type="text" class="input" placeholder="Email" name="USER_EMAIL" maxlength="255" />
            </div>
            <button class="btn sub-btn m-auto" name="send_account_info" value="Y">Восстановить пароль</button>
        </form>
    </div>
</section>

