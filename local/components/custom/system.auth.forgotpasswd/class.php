<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?


use Bitrix\Main;

class customForgotPassword extends CBitrixComponent
{

	public $message = null;

	protected function forgotpassword($USER_EMAIL) {
		global $USER;


		if(filter_var($USER_EMAIL, FILTER_VALIDATE_EMAIL)) {
			$arResult = $USER->SendPassword($USER_EMAIL, $USER_EMAIL, SITE_ID);
			if($arResult["TYPE"] == "OK")
				$this->message = 'Контрольная строка для смены пароля выслана.';
			else
				$this->message = 'Введенные логин (email) не найдены.';
		}
	}



	function executeComponent()
	{
		if($this->request['send_account_info'] <> '') {
			$this->forgotpassword($this->request['USER_EMAIL']);
		}

		if(CUser::IsAuthorized()) {
			LocalRedirect('/personal/private/');
		}

		$this->IncludeComponentTemplate();
	}
}


?>
