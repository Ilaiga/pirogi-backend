<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$APPLICATION->SetTitle("История заказов");


foreach($arResult['INFO']['STATUS'] as $arKey => $arStatus) {
	$STATUS[$arKey]['NAME'] = $arStatus['NAME'];
}

//echo '<pre>'.print_r($arResult, true).'</pre>';






?>


<section class="settings-history">
    <div class="container">
        <h1 class="settings-history__title title">Личный кабинет</h1>
        <nav class="settings-nav">
            <ul class="settings-nav__list">
                <li class="settings-nav__item">
                    <a class="settings-nav__link settings-nav__link--active">История заказов</a>
                </li>
                <li class="settings-nav__item">
                    <a href="/personal/private/" class="settings-nav__link">Личные данные</a>
                </li>
            </ul>
        </nav>
        <div class="settings-history__active">
            <h2 class="settings-history__sub-title h2">Активные</h2>
            <ul class="settings-history__list">

                <?foreach ($arResult['ORDERS'] as $ORDER):
                    if($ORDER['ORDER']['STATUS_ID'] == 'F') continue;
                    ?>
                    <li class="settings-history__item row">
                        <p class="settings-history__item-code col-12 col-lg-3">Заказ № <?=$ORDER['ORDER']['ID']?></p>

                        <?if($ORDER['ORDER_PROPS']['USER_DATE']):?>
                            <p class="settings-history__item-date col-12 col-lg-2"><?=$ORDER['ORDER_PROPS']['USER_DATE']?> (<?=$ORDER['ORDER_PROPS']['USER_TIME']?>)</p>
                        <?else:?>
                            <p class="settings-history__item-date col-12 col-lg-2"><?=$ORDER['ORDER']['DATE_INSERT_FORMATED']?></p>
                        <?endif;?>
                        <p class="settings-history__item-address col-12 col-lg-3 col-vl-4"><?=$ORDER['ORDER_PROPS']['USER_ADRESS']?></p>
                        <p class="settings-history__item-price col-12 col-lg-3 col-vl-2">
                            <span class="js-item-price"><?=(int)$ORDER['ORDER']['PRICE']?> </span>
                            <span class="currency">₽</span>
                        </p>
                        <button class="js-accordeon-toggle settings-history__item-toggle settings-history__item-toggle--close"></button>
                        <ul class="settings-history__product-list container">
                            <?foreach($ORDER['BASKET_ITEMS'] as $BASKET_KEY => $BASKET_ITEM):

                                //echo '<pre>'.print_r($ORDER['BASKET_ITEMS_DLC'][$BASKET_KEY], true).'</pre>';
                                ?>
                                <li class="settings-history__product-item row">
                                    <div class="settings-history__product-img col-5 col-lg-3">
                                        <img src="<?=CFile::getPath($ORDER['BASKET_ITEMS_DLC'][$BASKET_KEY]['PREVIEW_PICTURE'])?>" alt=""/>
                                    </div>
                                    <div class="settings-history__product-inner col-6 col-lg-9 row">
                                        <p class="settings-history__product-count col-lg-2 col-vl-3"><?=$BASKET_ITEM['QUANTITY']?> шт</p>
                                        <p class="settings-history__product-title col-lg-3"><?=$BASKET_ITEM['NAME']?></p>
                                        <p class="settings-history__product-weight col-lg-2"><?=$ORDER['BASKET_ITEMS_DLC'][$BASKET_KEY]['PROPERTIES']['WEIGHT']['VALUE']?></p>
                                        <p class="settings-history__product-price col-lg-3">
                                            <span class="js-item-price"><?=(int)$BASKET_ITEM['PRICE']?> </span>
                                            <span class="currency">₽</span>
                                        </p>
                                    </div>
                                </li>
                            <?endforeach;?>
                        </ul>
                    </li>
                <?endforeach;?>
            </ul>
        </div>
        <div class="settings-history__close">
            <h2 class="settings-history__sub-title h2">Завершенные</h2>
            <ul class="settings-history__list">

				<?foreach ($arResult['ORDERS'] as $ORDER):
					if($ORDER['ORDER']['STATUS_ID'] == 'N') continue;
					?>
                    <li class="settings-history__item row">
                        <p class="settings-history__item-code col-12 col-lg-3">Заказ № <?=$ORDER['ORDER']['ID']?></p>

						<?if($ORDER['ORDER_PROPS']['USER_DATE']):?>
                            <p class="settings-history__item-date col-12 col-lg-2"><?=$ORDER['ORDER_PROPS']['USER_DATE']?> (<?=$ORDER['ORDER_PROPS']['USER_TIME']?>)</p>
						<?else:?>
                            <p class="settings-history__item-date col-12 col-lg-2"><?=$ORDER['ORDER']['DATE_INSERT_FORMATED']?></p>
						<?endif;?>
                        <p class="settings-history__item-address col-12 col-lg-3 col-vl-4"><?=$ORDER['ORDER_PROPS']['USER_ADRESS']?></p>
                        <p class="settings-history__item-price col-12 col-lg-3 col-vl-2">
                            <span class="js-item-price"><?=(int)$ORDER['ORDER']['PRICE']?> </span>
                            <span class="currency">₽</span>
                        </p>
                        <button class="js-accordeon-toggle settings-history__item-toggle settings-history__item-toggle--close"></button>
                        <ul class="settings-history__product-list container">
							<?foreach($ORDER['BASKET_ITEMS'] as $BASKET_KEY => $BASKET_ITEM):?>
                                <li class="settings-history__product-item row">
                                    <div class="settings-history__product-img col-5 col-lg-3">
                                        <img src="<?=CFile::getPath($ORDER['BASKET_ITEMS_DLC'][$BASKET_KEY]['PREVIEW_PICTURE'])?>" alt=""/>
                                    </div>
                                    <div class="settings-history__product-inner col-6 col-lg-9 row">
                                        <p class="settings-history__product-count col-lg-2 col-vl-3"><?=$BASKET_ITEM['QUANTITY']?> шт</p>
                                        <p class="settings-history__product-title col-lg-3"><?=$BASKET_ITEM['NAME']?></p>
                                        <p class="settings-history__product-weight col-lg-2"><?=$ORDER['BASKET_ITEMS_DLC'][$BASKET_KEY]['PROPERTIES']['WEIGHT']['VALUE']?></p>
                                        <p class="settings-history__product-price col-lg-3">
                                            <span class="js-item-price"><?=(int)$BASKET_ITEM['PRICE']?> </span>
                                            <span class="currency">₽</span>
                                        </p>
                                    </div>
                                </li>
							<?endforeach;?>
                        </ul>
                    </li>
				<?endforeach;?>
            </ul>
        </div>
    </div>
</section>