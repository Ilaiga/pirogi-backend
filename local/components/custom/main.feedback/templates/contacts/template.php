<section class="feedback">


	<?
	foreach($arResult["ERROR_MESSAGE"] as $v)
		ShowError($v);
	?>


	<div class="container" <?=$arResult["OK_MESSAGE"] ? 'style="display: none"' : ''?>>
        <h2 class="form__title">Обратная связь</h2>
		<p class="form__desc">Мы с радостью ответим на все Ваши вопросы и предложения!</p>
		<form action="<?=POST_FORM_ACTION_URI?>" method="POST">

			<?=bitrix_sessid_post()?>

			<input class="input" name="user_name" type="text" value="<?=$arResult["AUTHOR_NAME"]?>" placeholder="Как Вас зовут?" required="required"/>
			<input class="input" name="user_email" type="email" value="<?=$arResult["AUTHOR_EMAIL"]?>" placeholder="Ваша почта" equired="required"/>
			<textarea class="input input__textarea" name="MESSAGE" cols="30" rows="7" placeholder="Ваше сообщение" required="required"><?=$arResult["MESSAGE"]?></textarea>
			<div class="form__wrapper">
				<button name="submit" class="btn btn--accept" type="submit">Отправить</button>
			</div>
			<input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>">
			<input type="hidden" name="Save" value="1">
		</form>
	</div>
    <?php if($arResult["OK_MESSAGE"]):?>
        <div class="feedback--thanks js-form-helper">
            <div class="container"><h2 class="form__title">Спасибо за подписку!</h2>
                <p class="form__desc">Мы будем оповещать Вас о самых выгодных акциях и предложениях</p></div>
        </div>
    <?endif;?>
</section>