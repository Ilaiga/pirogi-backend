<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");

$includePath = null; $coord = [];
if(isset($_COOKIE['currCity']) && $_COOKIE['currCity'] != 'main') {
	$includePath = '/include/subdomains/'.$_SESSION['GET_CITY']['DOMAIN_NAME']['VALUE'];
}
if($map = $_SESSION['GET_CITY']['CONTACTS_MAP']['VALUE']) {
    foreach($map as $item) {
        $item = explode(',', $item);
        $coord['POSITION'][] = [
			'LON' => $item[1],// LON и LAT - координаты элемента
			'LAT' => $item[0],
		];
    }
}
if($_COOKIE['currCity'] == 'main') {

	$arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_*");
	$arFilter = Array("IBLOCK_ID" => 5, "=CODE" => "MAIN", "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array(), $arFilter, false, array(), $arSelect);
	if($ob = $res->GetNextElement()) {
		$property = $ob->GetProperties();
    }
    
	$coord = [];
	if(is_array($property))
	foreach($property['CONTACTS_MAP']['VALUE'] as $item) {
		$item = explode(',', $item);
		$coord['POSITION'][] = [
			'LON' => $item[1],// LON и LAT - координаты элемента
			'LAT' => $item[0],
		];
	}
}


//echo '<pre>'.print_r($coord, true).'</pre>';


?>



<section class="contacts">
        <div class="container container--helper">
            <h1 class="title">Контакты</h1>
			<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
				"AREA_FILE_SHOW" => "file",
				"PATH" => $includePath ? $includePath.'/contact.php' : "/include/contact.php",
				"EDIT_TEMPLATE" => ""
			),
				array(
					'HIDE_ICONS' => 'Y'
				)
			);?>
        </div>
    </section>


    <section class="contacts-map">
		<?$APPLICATION->IncludeComponent("bitrix:map.yandex.view",".default",Array(
				"INIT_MAP_TYPE" => "MAP",
				"MAP_DATA" => $coord,
                    "MAP_WIDTH" => "100%",
                    "MAP_HEIGHT" => "500",
                    "CONTROLS" => array(
                        "TOOLBAR",
                        "ZOOM",
                        "SMALLZOOM",
                        "MINIMAP",
                        "TYPECONTROL",
                        "SCALELINE"
                    ),
                    "OPTIONS" => array(
                        "ENABLE_SCROLL_ZOOM",
                        "ENABLE_DBLCLICK_ZOOM",
                        "ENABLE_DRAGGING"
                    ),
                    "MAP_ID" => "yam_1"
                )
            );?>
    </section>


    <?php
    $APPLICATION->IncludeComponent(
        "custom:main.feedback",
        "contacts",
        array(
            "COMPONENT_TEMPLATE" => "contacts",
            "USE_CAPTCHA" => "N",
            "OK_TEXT" => "Спасибо, ваше сообщение принято.",
            "EMAIL_TO" => "duknuken666@gmail.com",
            "REQUIRED_FIELDS" => array(
            ),
            "EVENT_MESSAGE_ID" => [7]
        ),
        false
    ); ?>


    <section class="about-block">
        <div class="container">
            <div class="row about-block__row">
				<? $APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
					"AREA_FILE_SHOW" => "file",
					"PATH" => $includePath ? $includePath.'/about_main.php' : "/include/about_main.php",
					"EDIT_TEMPLATE" => ""
				),
					false
				); ?>
            </div>
        </div>
    </section>


    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU"></script>
    <script src="/js/contacts-map.js"></script>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>