<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Доставка");


$property = null;
if ($_COOKIE['currCity'] == 'main') {
	$arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_*");
	$arFilter = Array("IBLOCK_ID" => 5, "=CODE" => "MAIN", "ACTIVE" => "Y");
	$res = CIBlockElement::GetList(Array(), $arFilter, false, array(), $arSelect);
	if ($ob = $res->GetNextElement()) {
		$property = $ob->GetProperties();
	}
}
$mapId = $property['DELIVERY_MAP_ID']['VALUE'] ?: $_SESSION['GET_CITY']['DELIVERY_MAP_ID']['VALUE'];


//echo $mapId;

$includePath = null;
if(isset($_COOKIE['currCity']) && $_COOKIE['currCity'] != 'main') {
	$includePath = '/include/subdomains/'.$_SESSION['GET_CITY']['DOMAIN_NAME']['VALUE'];
}

$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	"AREA_FILE_SHOW" => "file",
	"PATH" => $includePath ? $includePath . '/delivery.php' : "/include/delivery.php",
	"EDIT_TEMPLATE" => ""
),
	array(
		'HIDE_ICONS' => 'Y'
	)
);

?>

<?  $APPLICATION->IncludeComponent(
	"angerro:angerro.yadelivery",
	"contacts",
	Array(
		"WIDTH" => "100%",
		//"WIDTH" => "490px",
		"HEIGHT" => "263px",
		"MAP_ID" => $mapId
	)
); ?>




<? $APPLICATION->IncludeComponent(
        "custom:main.feedback",
        "contacts",
        Array(
            "COMPONENT_TEMPLATE" => "contacts",
            "EMAIL_TO" => "duknuken666@gmail.com",
            "EVENT_MESSAGE_ID" => [ 7 ],
            "OK_TEXT" => "Спасибо, ваше сообщение принято.",
            "REQUIRED_FIELDS" => array(),
            "USE_CAPTCHA" => "N",
            "EVENT_NAME" => 'FEEDBACK_FORM'
        )
    ); ?>
    <section class="about-block">
        <div class="container">
            <div class="row about-block__row">
				<? $APPLICATION->IncludeComponent(
					"bitrix:main.include",
					".default",
					Array(
						"AREA_FILE_SHOW" => "file",
						"EDIT_TEMPLATE" => "",
						"PATH" => "/include/about.php"
					)
				); ?>
            </div>
        </div>
    </section>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>