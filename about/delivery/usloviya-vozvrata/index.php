<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Условия возврата");


$property = null;
if ($_COOKIE['currCity'] == 'main') {
	$arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_*");
	$arFilter = Array("IBLOCK_ID" => 5, "=CODE" => "MAIN", "ACTIVE" => "Y");
	$res = CIBlockElement::GetList(Array(), $arFilter, false, array(), $arSelect);
	if ($ob = $res->GetNextElement()) {
		$property = $ob->GetProperties();
	}
}

$includePath = null;
if(isset($_COOKIE['currCity']) && $_COOKIE['currCity'] != 'main') {
	$includePath = '/include/subdomains/'.$_SESSION['GET_CITY']['DOMAIN_NAME']['VALUE'];
}

$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	"AREA_FILE_SHOW" => "file",
	"PATH" => $includePath ? $includePath . '/usloviya-vozvrata.php' : "/include/usloviya-vozvrata.php",
	"EDIT_TEMPLATE" => ""
),
	array(
		'HIDE_ICONS' => 'Y'
	)
);

?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>