<?php

//Отключаем статистику Bitrix
define("NO_KEEP_STATISTIC", true);
//Подключаем движок
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
//устанавливаем тип ответа как xml документ
header('Content-Type: application/xml; charset=utf-8');


$array_pages = array();

//Простые текстовые страницы: начало
$array_pages[] = array(
	'NAME' => 'Главная страница',
	'URL' => '/',
);
$array_pages[] = array(
	'NAME' => 'Каталог',
	'URL' => '/catalog/',
);
$array_pages[] = array(
	'NAME' => 'Доставка и оплата',
	'URL' => '/about/delivery/',
);
$array_pages[] = array(
	'NAME' => 'Акции',
	'URL' => '/actions/',
);
$array_pages[] = array(
	'NAME' => 'О нас',
	'URL' => '/about/',
);
$array_pages[] = array(
	'NAME' => 'Контакты',
	'URL' => '/contacts/',
);
//Простые текстовые страницы: конец


$SECTION = array_values($_SESSION['GET_CITY']['SECTIONS']['VALUE']);

$array_iblocks_id = array('1'); //ID инфоблоков, разделы и элементы которых попадут в карту сайта
if(CModule::IncludeModule("iblock"))
{
	foreach($array_iblocks_id as $iblock_id)
	{
		//Список разделов
		//Список элементов
		$res = CIBlockSection::GetList(
			array(),
			Array(
				"IBLOCK_ID" => $iblock_id,
				"ACTIVE" => "Y",
				"ID" => $SECTION ?: 999
			),
			false,
			array(
				"ID",
				"NAME",
				"SECTION_PAGE_URL",
			));
		$res->SetUrlTemplates([],"/catalog/#SECTION_CODE_PATH#");
		while($ob = $res->GetNext())
		{
			$array_pages[] = array(
				'NAME' => $ob['NAME'],
				'URL' => $ob['SECTION_PAGE_URL'],
			);
		}
		//Список элементов
		$res = CIBlockElement::GetList(
			array(),
			Array(
				"IBLOCK_ID" => $iblock_id,
				"ACTIVE_DATE" => "Y",
				"ACTIVE" => "Y",
				"SECTION_ID" => $SECTION ?: 999
			),
			false,
			false,
			array(
				"ID",
				"NAME",
				"DETAIL_PAGE_URL",
			));
		$res->SetUrlTemplates("/catalog/#SECTION_CODE_PATH#/#ELEMENT_CODE#/");
		while($ob = $res->GetNext())
		{
			$array_pages[] = array(
				'NAME' => $ob['NAME'],
				'URL' => $ob['DETAIL_PAGE_URL'],
			);
		}
	}
}

//Создаём XML документ: начало
$xml_content = '';
$site_url = '//'.$_SERVER['HTTP_HOST'];
$quantity_elements = 0;
foreach($array_pages as $v)
{
	$quantity_elements++;
	$xml_content.='
   	<url>
		<loc>'.$site_url.$v['URL'].'</loc>
		<lastmode>2019-04-20T12:00:44+07:00</lastmode>
	</url>
	';
}
//Создаём XML документ: конец

//Выводим документ
echo '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
	'.$xml_content.'
</urlset>
';
?>
