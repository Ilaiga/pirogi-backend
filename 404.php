<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");
define("HIDE_SIDEBAR", true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("Страница не найдена");?>

    <div class="container container--helper">
        <section class="not-found">
            <div class="not-found__inner"><h1 class="title">Страница недоступна</h1>
                <p class="text-404">Пока мы устраняем неполадки, вы можете выбрать другой пирог и вкусно поесть</p><a
                        class="btn order-btn" href="/catalog">Выбрать
                    <svg class="icon-svg">
                        <use xlink:href="#icon-pie"></use>
                    </svg>
                </a></div>
            <img class="not-found__img" src="/img/404.svg" width="400" height="500"/></section>
    </div>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>